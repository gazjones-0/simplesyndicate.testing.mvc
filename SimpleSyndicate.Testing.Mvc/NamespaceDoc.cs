﻿namespace SimpleSyndicate.Testing.Mvc
{
	/// <summary>
	/// The <see cref="Mvc"/> namespace contains classes for use in testing MVC applications.
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGeneratedAttribute]
	internal class NamespaceDoc
	{
	}
}
