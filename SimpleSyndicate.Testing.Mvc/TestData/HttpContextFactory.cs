﻿using System;
using System.Web;
using SimpleSyndicate.Testing.Mvc.Web;

namespace SimpleSyndicate.Testing.Mvc.TestData
{
	/// <summary>
	/// Factory class for creating <see cref="HttpContext"/>s and related items.
	/// </summary>
	public static class HttpContextFactory
	{
		/// <summary>
		/// Creates a <see cref="HttpContext"/>, suitable for use in unit tests; the context is for a <see cref="HttpRequest"/> for <c>tempuri.org</c>.
		/// </summary>
		/// <returns>A <see cref="HttpContext"/> suitable for use in unit tests.</returns>
		public static HttpContext CreateHttpContext()
		{
			return new HttpContext(new HttpRequest(String.Empty, "http://tempuri.org", String.Empty), new HttpResponse(null));
		}

		/// <summary>
		/// Creates a <see cref="HttpContext"/> simulating paging, suitable for use in unit tests; the context is for a <see cref="HttpRequest"/> for <c>tempuri.org</c>
		/// and the query string has appropriate values for paging.
		/// </summary>
		/// <returns>A <see cref="HttpContext"/> simulating paging, suitable for use in unit tests.</returns>
		public static HttpContext CreateHttpContextWithPaging()
		{
			return new HttpContext(new HttpRequest(String.Empty, "http://tempuri.org", "grid-page=3"), new HttpResponse(null));
		}

		/// <summary>
		/// Creates a <see cref="HttpContextWrapper"/>, suitable for use in unit tests; the context is for a <see cref="HttpRequest"/> for <c>tempuri.org</c>.
		/// </summary>
		/// <remarks>
		/// The actual class returned is a <see cref="TestHttpContextWrapper"/> that derives from <see cref="HttpContextWrapper"/>; see
		/// <see cref="TestHttpContextWrapper"/> for more details on why the derived class is used.
		/// </remarks>
		/// <returns>A <see cref="HttpContextWrapper"/> suitable for use in unit tests.</returns>
		public static HttpContextWrapper CreateHttpContextWrapper()
		{
			return new TestHttpContextWrapper(new HttpContext(new HttpRequest(String.Empty, "http://tempuri.org", String.Empty), new HttpResponse(null)));
		}

		/// <summary>
		/// Creates a <see cref="HttpContextWrapper"/> simulating paging, suitable for use in unit tests; the context is for a <see cref="HttpRequest"/> for <c>tempuri.org</c>
		/// and the query string has appropriate values for paging.
		/// </summary>
		/// <remarks>
		/// The actual class returned is a <see cref="TestHttpContextWrapper"/> that derives from <see cref="HttpContextWrapper"/>; see
		/// <see cref="TestHttpContextWrapper"/> for more details on why the derived class is used.
		/// </remarks>
		/// <returns>A <see cref="HttpContextWrapper"/> simulating paging, suitable for use in unit tests.</returns>
		public static HttpContextWrapper CreateHttpContextWrapperWithPaging()
		{
			return new TestHttpContextWrapper(new HttpContext(new HttpRequest(String.Empty, "http://tempuri.org", "grid-page=3"), new HttpResponse(null)));
		}
	}
}
