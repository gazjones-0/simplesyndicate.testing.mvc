﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace SimpleSyndicate.Testing.Mvc.TestData
{
	/// <summary>
	/// Factory class for creating <see cref="AspNetContainer{TIdentityUser}"/>s.
	/// </summary>
	public static class AspNetContainerFactory
	{
		/// <summary>
		/// Creates an <see cref="AspNetContainer{TIdentityUser}"/>, populated with test data.
		/// </summary>
		/// <typeparam name="TIdentityUser">The user type the user store and user manager hold.</typeparam>
		/// <returns>An <see cref="AspNetContainer{TIdentityUser}"/> populated with test data.</returns>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "userStore, userManager and roleStore will be disposed by the AspNetContainer that holds them.")]
		public static AspNetContainer<TIdentityUser> Create<TIdentityUser>()
			where TIdentityUser : IdentityUser, new()
		{
			AspNetContainer<TIdentityUser> container = new AspNetContainer<TIdentityUser>();
			IUserStore<TIdentityUser> userStore = null;
			UserManager<TIdentityUser> userManager = null;
			IRoleStore<IdentityRole> roleStore = null;
			try
			{
				userStore = AspNetIdentityFactory.CreateUserStore<TIdentityUser>();
				userManager = AspNetIdentityFactory.CreateUserManager(userStore);
				roleStore = AspNetIdentityFactory.CreateRoleStore();
				container.RoleStore = roleStore;
				roleStore = null;
				container.UserManager = userManager;
				userManager = null;
				container.UserStore = userStore;
				userStore = null;
			}
			finally
			{
				if (userStore != null)
				{
					userStore.Dispose();
				}
				if (userManager != null)
				{
					userManager.Dispose();
				}
				if (roleStore != null)
				{
					roleStore.Dispose();
				}
			}
			return container;
		}
	}
}
