﻿using System;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SimpleSyndicate.Testing.Mvc.AspNet.Identity;

namespace SimpleSyndicate.Testing.Mvc.TestData
{
	/// <summary>
	/// Factory class for creating <see cref="Microsoft.AspNet.Identity"/> related items.
	/// </summary>
	public static class AspNetIdentityFactory
	{
		/// <summary>
		/// Creates an <see cref="UserManager{TIdentityUser}"/>, populated with test data.
		/// </summary>
		/// <typeparam name="TIdentityUser">The user type the user manager holds.</typeparam>
		/// <param name="userStore">The user store the user manager uses to hold user information in.</param>
		/// <returns>An <see cref="UserManager{TIdentityUser}"/> populated with test data.</returns>
		/// <exception cref="ArgumentNullException">Thrown when <paramref name="userStore"/> is <c>null</c>.</exception>
		public static UserManager<TIdentityUser> CreateUserManager<TIdentityUser>(IUserStore<TIdentityUser> userStore)
			where TIdentityUser : IdentityUser, new()
		{
			if (userStore == null)
			{
				throw new ArgumentNullException("userStore");
			}
			var userManager = new UserManager<TIdentityUser>(userStore);
			return userManager;
		}

		/// <summary>
		/// Creates an <see cref="IRoleStore{IdentityRole}"/>, populated with test data.
		/// </summary>
		/// <returns>An <see cref="IRoleStore{IdentityRole}"/> populated with test data.</returns>
		public static IRoleStore<IdentityRole> CreateRoleStore()
		{
			IRoleStore<IdentityRole> roleStore = null;
			IRoleStore<IdentityRole> tempRoleStore = null;
			try
			{
				tempRoleStore = new TestRoleStore();
				tempRoleStore.CreateAsync(new IdentityRole() { Id = "1", Name = "Role 1" }).Wait();
				tempRoleStore.CreateAsync(new IdentityRole() { Id = "2", Name = "Role 2" }).Wait();
				tempRoleStore.CreateAsync(new IdentityRole() { Id = "3", Name = "Role 3" }).Wait();
				tempRoleStore.CreateAsync(new IdentityRole() { Id = "4", Name = "Role 4" }).Wait();
				tempRoleStore.CreateAsync(new IdentityRole() { Id = "5", Name = "Role 5" }).Wait();
				roleStore = tempRoleStore;
				tempRoleStore = null;
			}
			finally
			{
				if (tempRoleStore != null)
				{
					tempRoleStore.Dispose();
				}
			}
			return roleStore;
		}

		/// <summary>
		/// Creates an <see cref="IUserStore{TIdentityUser}"/>, populated with test data.
		/// </summary>
		/// <typeparam name="TIdentityUser">The user type the user store holds.</typeparam>
		/// <returns>An <see cref="IUserStore{TIdentityUser}"/> populated with test data.</returns>
		public static IUserStore<TIdentityUser> CreateUserStore<TIdentityUser>()
			where TIdentityUser : IdentityUser, new()
		{
			IUserStore<TIdentityUser> userStore = null;
			IUserStore<TIdentityUser> tempUserStore = null;
			try
			{
				tempUserStore = new TestUserStore<TIdentityUser>();
				tempUserStore.CreateAsync(new TIdentityUser() { Id = "1", UserName = "user1@tempuri.org" }).Wait();
				tempUserStore.CreateAsync(new TIdentityUser() { Id = "2", UserName = "user2@tempuri.org" }).Wait();
				tempUserStore.CreateAsync(new TIdentityUser() { Id = "3", UserName = "user3@tempuri.org" }).Wait();
				userStore = tempUserStore;
				tempUserStore = null;
			}
			finally
			{
				if (tempUserStore != null)
				{
					tempUserStore.Dispose();
				}
			}
			return userStore;
		}
	}
}
