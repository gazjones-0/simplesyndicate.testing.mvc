﻿namespace SimpleSyndicate.Testing.Mvc.TestData
{
	/// <summary>
	/// The <see cref="TestData"/> namespace contains classes for working with data used in unit testing.
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGeneratedAttribute]
	internal class NamespaceDoc
	{
	}
}
