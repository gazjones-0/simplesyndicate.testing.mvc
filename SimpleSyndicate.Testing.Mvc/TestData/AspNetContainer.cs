﻿using System;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace SimpleSyndicate.Testing.Mvc.TestData
{
	/// <summary>
	/// Contains <b>Microsoft.AspNet</b> related objects that are suitable for use in unit testing; use
	/// <see cref="AspNetContainerFactory"/> to create.
	/// </summary>
	/// <typeparam name="TIdentityUser">The user type the user store and user manager hold.</typeparam>
	public class AspNetContainer<TIdentityUser> : IDisposable
		where TIdentityUser : IdentityUser
	{
		/// <summary>
		/// Whether Dispose has been called or not.
		/// </summary>
		private bool disposed = false;

		/// <summary>
		/// Gets or sets the <see cref="IRoleStore{IdentityRole}"/> that is suitable for use in unit testing.
		/// </summary>
		/// <value>The <see cref="IRoleStore{IdentityRole}"/>.</value>
		public IRoleStore<IdentityRole> RoleStore { get; set; }

		/// <summary>
		/// Gets or sets the <see cref="Microsoft.AspNet.Identity.UserManager{TIdentityUser}"/> that is suitable for use in unit testing.
		/// </summary>
		/// <value>The <see cref="Microsoft.AspNet.Identity.UserManager{TIdentityUser}"/>.</value>
		public UserManager<TIdentityUser> UserManager { get; set; }

		/// <summary>
		/// Gets or sets the <see cref="IUserStore{TIdentityUser}"/> that is suitable for use in unit testing.
		/// </summary>
		/// <value>The <see cref="IUserStore{TIdentityUser}"/>.</value>
		public IUserStore<TIdentityUser> UserStore { get; set; }

		/// <overloads>
		/// <summary>
		/// Releases all resources that are used by the current instance of the <see cref="AspNetContainer{TIdentityUser}"/> class.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Releases all resources that are used by the current instance of the <see cref="AspNetContainer{TIdentityUser}"/> class;
		/// <see cref="RoleStore"/>, <see cref="UserManager"/> and <see cref="UserStore"/> will be released.
		/// </summary>
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>
		/// Releases unmanaged resources and optionally releases managed resources; if <paramref name="disposing"/> is <c>true</c>
		/// <see cref="RoleStore"/>, <see cref="UserManager"/> and <see cref="UserStore"/> will be released.
		/// </summary>
		/// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
		protected virtual void Dispose(bool disposing)
		{
			if (this.disposed)
			{
				return;
			}
			if (disposing)
			{
				if (this.UserManager != null)
				{
					this.UserManager.Dispose();
					this.UserManager = null;
				}
				if (this.UserStore != null)
				{
					this.UserStore.Dispose();
					this.UserStore = null;
				}
				if (this.RoleStore != null)
				{
					this.RoleStore.Dispose();
					this.RoleStore = null;
				}
			}
			this.disposed = true;
		}
	}
}
