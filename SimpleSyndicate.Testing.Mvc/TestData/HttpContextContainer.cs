﻿using System.Web;

namespace SimpleSyndicate.Testing.Mvc.TestData
{
	/// <summary>
	/// Contains <see cref="HttpContext"/>, or <see cref="HttpContext"/>-related objects that are suitable for use in unit testing; use
	/// <see cref="HttpContextContainerFactory"/> to create.
	/// </summary>
	/// <remarks>
	/// When unit testing, it is often necessary to set up, or provide, a <see cref="HttpContext"/> (or related object), and this class
	/// is designed to provide these, to save having to create or mock them.
	/// </remarks>
	/// <seealso cref="HttpContextContainerFactory"/>
	public class HttpContextContainer
	{
		/// <summary>
		/// Gets or sets a <see cref="HttpContext"/> object that is suitable for use in unit testing.
		/// </summary>
		/// <value>The <see cref="HttpContext"/>.</value>
		public HttpContext HttpContext { get; set; }

		/// <summary>
		/// Gets or sets a <see cref="HttpContext"/> object that is suitable for use in unit testing, and is set up to support unit testing paging.
		/// </summary>
		/// <value>The <see cref="HttpContext"/>.</value>
		public HttpContext HttpContextWithPaging { get; set; }

		/// <summary>
		/// Gets or sets a <see cref="HttpContextWrapper"/> object that is suitable for use in unit testing.
		/// </summary>
		/// <value>The <see cref="HttpContextWrapper"/>.</value>
		public HttpContextWrapper HttpContextWrapper { get; set; }

		/// <summary>
		/// Gets or sets a <see cref="HttpContextWrapper"/> object that is suitable for use in unit testing, and is set up to support unit testing paging.
		/// </summary>
		/// <value>The <see cref="HttpContextWrapper"/>.</value>
		public HttpContextWrapper HttpContextWrapperWithPaging { get; set; }
	}
}
