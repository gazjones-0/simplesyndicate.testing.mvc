﻿namespace SimpleSyndicate.Testing.Mvc.TestData
{
	/// <summary>
	/// Factory class for creating <see cref="HttpContextContainer"/>s.
	/// </summary>
	public static class HttpContextContainerFactory
	{
		/// <summary>
		/// Creates a <see cref="HttpContextContainer"/>, populated with test data.
		/// </summary>
		/// <returns>A <see cref="HttpContextContainer"/> populated with test data.</returns>
		public static HttpContextContainer Create()
		{
			var container = new HttpContextContainer();
			container.HttpContext = HttpContextFactory.CreateHttpContext();
			container.HttpContextWithPaging = HttpContextFactory.CreateHttpContextWithPaging();
			container.HttpContextWrapper = HttpContextFactory.CreateHttpContextWrapper();
			container.HttpContextWrapperWithPaging = HttpContextFactory.CreateHttpContextWrapperWithPaging();
			return container;
		}
	}
}
