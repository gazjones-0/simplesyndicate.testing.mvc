﻿using SimpleSyndicate.Mvc.Controllers;
using SimpleSyndicate.Mvc.Models;
using SimpleSyndicate.Repositories;
using SimpleSyndicate.Testing.Repositories;

namespace SimpleSyndicate.Testing.Mvc.TestData
{
	/// <summary>
	/// Factory class for creating repositories for <see cref="MvcRepositoriesContainer"/>.
	/// </summary>
	public static class MvcRepositoriesContainerFactory
	{
		/// <summary>
		/// Creates a <see cref="AspNetUserDefaultRole"/> <see cref="TestRepository{TEntity}"/>, populated with test data.
		/// </summary>
		/// <returns>A <see cref="AspNetUserDefaultRole"/> repository.</returns>
		public static IRepository<AspNetUserDefaultRole> CreateAspNetUserDefaultRoleRepository()
		{
			IRepository<AspNetUserDefaultRole> repository = null;
			IRepository<AspNetUserDefaultRole> tempRepository = null;
			try
			{
				tempRepository = new TestRepository<AspNetUserDefaultRole>();
				tempRepository.Add(new AspNetUserDefaultRole() { RoleId = "2" });
				tempRepository.Add(new AspNetUserDefaultRole() { RoleId = "4" });
				repository = tempRepository;
				tempRepository = null;
			}
			finally
			{
				if (tempRepository != null)
				{
					tempRepository.Dispose();
				}
			}
			return repository;
		}

		/// <summary>
		/// Creates a <see cref="AspNetUserLoginDetail"/> <see cref="TestRepository{TEntity}"/>, populated with test data.
		/// </summary>
		/// <returns>A <see cref="AspNetUserLoginDetail"/> repository.</returns>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1726:UsePreferredTerms", MessageId = "Login", Justification = "Class relates to existing Asp.Net class where Login is already used; name kept for consistency.")]
		public static IRepository<AspNetUserLoginDetail> CreateAspNetUserLoginDetailRepository()
		{
			IRepository<AspNetUserLoginDetail> repository = null;
			IRepository<AspNetUserLoginDetail> tempRepository = null;
			try
			{
				tempRepository = new TestRepository<AspNetUserLoginDetail>();
				tempRepository.Add(new AspNetUserLoginDetail() { UserId = "userId1", Email = "userId1@tempuri.org" });
				tempRepository.Add(new AspNetUserLoginDetail() { UserId = "userId2", Email = "userId2@tempuri.org" });
				repository = tempRepository;
				tempRepository = null;
			}
			finally
			{
				if (tempRepository != null)
				{
					tempRepository.Dispose();
				}
			}
			return repository;
		}

		/// <summary>
		/// Creates a <see cref="SelectListItemViewModel"/> <see cref="TestRepository{TEntity}"/>, populated with test data.
		/// </summary>
		/// <returns>A <see cref="SelectListItemViewModel"/> repository.</returns>
		public static IRepository<SelectListItemViewModel> CreateSelectListItemViewModelRepository()
		{
			IRepository<SelectListItemViewModel> repository = null;
			IRepository<SelectListItemViewModel> tempRepository = null;
			try
			{
				tempRepository = new TestRepository<SelectListItemViewModel>();
				tempRepository.Add(new SelectListItemViewModel() { Id = 1, Name = "Test 1" });
				tempRepository.Add(new SelectListItemViewModel() { Id = 2, Name = "Test 2" });
				tempRepository.Add(new SelectListItemViewModel() { Id = 3, Name = "Test 3" });
				repository = tempRepository;
				tempRepository = null;
			}
			finally
			{
				if (tempRepository != null)
				{
					tempRepository.Dispose();
				}
			}
			return repository;
		}
	}
}
