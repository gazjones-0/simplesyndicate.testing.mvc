﻿using SimpleSyndicate.Mvc.Controllers;
using SimpleSyndicate.Mvc.Models;
using SimpleSyndicate.Repositories;
using SimpleSyndicate.Testing.TestData;

namespace SimpleSyndicate.Testing.Mvc.TestData
{
	/// <summary>
	/// Contains a set of repositories that have been populated with a suite of test data, suitable for use in unit testing.
	/// </summary>
	/// <remarks>
	/// This class is designed to act as a base class that is then extended to hold other, application-specific, repositories.
	/// </remarks>
	public abstract class MvcRepositoriesContainer : RepositoriesContainer
	{
		/// <summary>
		/// Whether Dispose has been called or not.
		/// </summary>
		private bool disposed = false;

		/// <summary>
		/// Gets or sets the <see cref="IRepository{AspNetUserDefaultRole}"/> repository.
		/// </summary>
		/// <value>The <see cref="IRepository{AspNetUserDefaultRole}"/> repository.</value>
		public IRepository<AspNetUserDefaultRole> AspNetUserDefaultRoleRepository { get; set; }

		/// <summary>
		/// Gets or sets the <see cref="IRepository{AspNetUserLoginDetail}"/> repository.
		/// </summary>
		/// <value>The <see cref="IRepository{AspNetUserLoginDetail}"/> repository.</value>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1726:UsePreferredTerms", MessageId = "Login", Justification = "Class relates to existing Asp.Net class where Login is already used; name kept for consistency.")]
		public IRepository<AspNetUserLoginDetail> AspNetUserLoginDetailRepository { get; set; }

		/// <summary>
		/// Gets or sets the <see cref="IRepository{SelectListItemViewModel}"/> repository.
		/// </summary>
		/// <value>The <see cref="IRepository{SelectListItemViewModel}"/> repository.</value>
		public IRepository<SelectListItemViewModel> SelectListItemViewModelRepository { get; set; }

		/// <overloads>
		/// <summary>
		/// Releases all resources that are used by the current instance of the <see cref="MvcRepositoriesContainer"/> class.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Releases unmanaged resources and optionally releases managed resources; if <paramref name="disposing"/> is <c>true</c>
		/// <see cref="AspNetUserDefaultRoleRepository"/>, <see cref="AspNetUserLoginDetailRepository"/> and
		/// <see cref="SelectListItemViewModelRepository"/> will be released.
		/// </summary>
		/// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
		protected override void Dispose(bool disposing)
		{
			if (this.disposed)
			{
				return;
			}
			if (disposing)
			{
				if (this.AspNetUserDefaultRoleRepository != null)
				{
					this.AspNetUserDefaultRoleRepository.Dispose();
					this.AspNetUserDefaultRoleRepository = null;
				}
				if (this.AspNetUserLoginDetailRepository != null)
				{
					this.AspNetUserLoginDetailRepository.Dispose();
					this.AspNetUserLoginDetailRepository = null;
				}
				if (this.SelectListItemViewModelRepository != null)
				{
					this.SelectListItemViewModelRepository.Dispose();
					this.SelectListItemViewModelRepository = null;
				}
			}
			this.disposed = true;
			base.Dispose(disposing);
		}
	}
}
