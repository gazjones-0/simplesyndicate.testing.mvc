﻿using System;
using System.Collections.Generic;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Facebook;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.MicrosoftAccount;
using Microsoft.Owin.Security.Twitter;
using Microsoft.Owin.Testing;
using Owin;
using SimpleSyndicate.Mvc.App_Start;

namespace SimpleSyndicate.Testing.Mvc.App_Start
{
	/// <summary>
	/// Helper methods for unit testing MVC authentication configuration.
	/// </summary>
	public static class AuthenticationConfigurationHelpers
	{
		/// <overloads>
		/// <summary>
		/// Returns whether cookie authentication has been enabled.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Returns whether cookie authentication has been enabled for the specified configuration and authentication type.
		/// </summary>
		/// <remarks>
		/// To check whether authentication has been enabled, a test Owin server is set up, configured using the specified
		/// configuration, and then the Owin environment examined.
		/// </remarks>
		/// <param name="authenticationConfiguration">The authentication configuration.</param>
		/// <param name="authenticationType">The authentication type.</param>
		/// <returns>A boolean representing whether cookie authentication has been enabled for the specified authentication type or not.</returns>
		/// <seealso cref="SimpleSyndicate.Testing.Mvc.TestTools.UnitTesting.MvcAssert.HasCookieAuthentication"/>
		public static bool HasCookieAuthentication(IAuthenticationConfiguration authenticationConfiguration, string authenticationType)
		{
			return HasAuthentication(authenticationConfiguration, authenticationType, String.Empty);
		}

		/// <summary>
		/// Returns whether cookie authentication has been enabled for the specified configuration, authentication type and path to the login page.
		/// </summary>
		/// <remarks>
		/// To check whether authentication has been enabled, a test Owin server is set up, configured using the specified
		/// configuration, and then the Owin environment examined.
		/// </remarks>
		/// <param name="authenticationConfiguration">The authentication configuration.</param>
		/// <param name="authenticationType">The authentication type.</param>
		/// <param name="loginPath">Path to the login page.</param>
		/// <returns>A boolean representing whether cookie authentication has been enabled for the specified authentication type and login path or not.</returns>
		/// <seealso cref="SimpleSyndicate.Testing.Mvc.TestTools.UnitTesting.MvcAssert.HasCookieAuthentication"/>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1726:UsePreferredTerms", MessageId = "login", Justification = "Login is used in existing Asp.Net class name")]
		public static bool HasCookieAuthentication(IAuthenticationConfiguration authenticationConfiguration, string authenticationType, string loginPath)
		{
			return HasAuthentication(authenticationConfiguration, authenticationType, loginPath);
		}

		/// <summary>
		/// Returns whether Facebook authentication has been enabled for the specified configuration.
		/// </summary>
		/// <remarks>
		/// To check whether authentication has been enabled, a test Owin server is set up, configured using the specified
		/// configuration, and then the Owin environment examined.
		/// </remarks>
		/// <param name="authenticationConfiguration">The authentication configuration.</param>
		/// <returns>A boolean representing whether Facebook authentication has been enabled or not.</returns>
		/// <seealso cref="SimpleSyndicate.Testing.Mvc.TestTools.UnitTesting.MvcAssert.HasCookieAuthentication"/>
		public static bool HasFacebookAuthentication(IAuthenticationConfiguration authenticationConfiguration)
		{
			return HasAuthentication(authenticationConfiguration, typeof(FacebookAuthenticationOptions));
		}

		/// <summary>
		/// Returns whether Google authentication has been enabled for the specified configuration.
		/// </summary>
		/// <remarks>
		/// To check whether authentication has been enabled, a test Owin server is set up, configured using the specified
		/// configuration, and then the Owin environment examined.
		/// </remarks>
		/// <param name="authenticationConfiguration">The authentication configuration.</param>
		/// <returns>A boolean representing whether Google authentication has been enabled or not.</returns>
		/// <seealso cref="SimpleSyndicate.Testing.Mvc.TestTools.UnitTesting.MvcAssert.HasCookieAuthentication"/>
		public static bool HasGoogleAuthentication(IAuthenticationConfiguration authenticationConfiguration)
		{
			return HasAuthentication(authenticationConfiguration, typeof(GoogleAuthenticationOptions));
		}

		/// <summary>
		/// Returns whether Microsoft authentication has been enabled for the specified configuration.
		/// </summary>
		/// <remarks>
		/// To check whether authentication has been enabled, a test Owin server is set up, configured using the specified
		/// configuration, and then the Owin environment examined.
		/// </remarks>
		/// <param name="authenticationConfiguration">The authentication configuration.</param>
		/// <returns>A boolean representing whether Microsoft authentication has been enabled or not.</returns>
		/// <seealso cref="SimpleSyndicate.Testing.Mvc.TestTools.UnitTesting.MvcAssert.HasCookieAuthentication"/>
		public static bool HasMicrosoftAuthentication(IAuthenticationConfiguration authenticationConfiguration)
		{
			return HasAuthentication(authenticationConfiguration, typeof(MicrosoftAccountAuthenticationOptions));
		}

		/// <summary>
		/// Returns whether Twitter authentication has been enabled for the specified configuration.
		/// </summary>
		/// <remarks>
		/// To check whether authentication has been enabled, a test Owin server is set up, configured using the specified
		/// configuration, and then the Owin environment examined.
		/// </remarks>
		/// <param name="authenticationConfiguration">The authentication configuration.</param>
		/// <returns>A boolean representing whether Twitter authentication has been enabled or not.</returns>
		/// <seealso cref="SimpleSyndicate.Testing.Mvc.TestTools.UnitTesting.MvcAssert.HasCookieAuthentication"/>
		public static bool HasTwitterAuthentication(IAuthenticationConfiguration authenticationConfiguration)
		{
			return HasAuthentication(authenticationConfiguration, typeof(TwitterAuthenticationOptions));
		}

		/// <summary>
		/// Returns whether cookie authentication has been enabled for the specified configuration, authentication type and path to the login page.
		/// </summary>
		/// <remarks>
		/// To check whether authentication has been enabled, a test Owin server is set up, configured using the specified
		/// configuration, and then the Owin environment examined.
		/// </remarks>
		/// <param name="authenticationConfiguration">The authentication configuration.</param>
		/// <param name="authenticationType">The authentication type.</param>
		/// <param name="loginPath">Path to the login page; if this is <c>null</c> or an empty string the login path is not checked in the configuration.</param>
		/// <returns>A boolean representing whether cookie authentication has been enabled for the specified authentication type and login path or not.</returns>
		/// <seealso cref="SimpleSyndicate.Testing.Mvc.TestTools.UnitTesting.MvcAssert.HasCookieAuthentication"/>
		private static bool HasAuthentication(IAuthenticationConfiguration authenticationConfiguration, string authenticationType, string loginPath)
		{
			var middlewareList = GetMiddleware(authenticationConfiguration);
			if (middlewareList != null)
			{
				// check each piece of middleware
				foreach (var middleware in middlewareList)
				{
					// check each component used by the middleware
					var components = middleware.Item3;
					if (components != null)
					{
						foreach (var component in components)
						{
							if (component.GetType() == typeof(CookieAuthenticationOptions))
							{
								var options = (CookieAuthenticationOptions)component;
								if (String.IsNullOrEmpty(loginPath))
								{
									if (options.AuthenticationType == authenticationType)
									{
										return true;
									}
								}
								else
								{
									if (options.AuthenticationType == authenticationType && options.LoginPath == new PathString(loginPath))
									{
										return true;
									}
								}
							}
						}
					}
				}
			}
			return false;
		}

		/// <summary>
		/// Returns whether authentication of the specified type has been enabled for the specified configuration.
		/// </summary>
		/// <remarks>
		/// To check whether authentication has been enabled, a test Owin server is set up, configured using the specified
		/// configuration, and then the Owin environment examined.
		/// </remarks>
		/// <param name="authenticationConfiguration">The authentication configuration.</param>
		/// <param name="expectedType">Type of authentication to check for.</param>
		/// <returns>A boolean representing whether authentication of the specified type has been enabled or not.</returns>
		private static bool HasAuthentication(IAuthenticationConfiguration authenticationConfiguration, Type expectedType)
		{
			var middlewareList = GetMiddleware(authenticationConfiguration);
			if (middlewareList != null)
			{
				// check each piece of middleware
				foreach (var middleware in middlewareList)
				{
					// check each component used by the middleware
					var components = middleware.Item3;
					if (components != null)
					{
						foreach (var component in components)
						{
							if (component.GetType() == expectedType)
							{
								return true;
							}
						}
					}
				}
			}
			return false;
		}

		/// <summary>
		/// Internal method to get a list of the Owin middleware for the specified configuration.
		/// </summary>
		/// <param name="authenticationConfiguration">The authentication configuration.</param>
		/// <returns>A collection of Owin middleware for the specified configuration.</returns>
		private static IList<Tuple<Type, Delegate, object[]>> GetMiddleware(IAuthenticationConfiguration authenticationConfiguration)
		{
			// create test Owin server, and configure it using the provided configuration
			IAppBuilder appBuilder = null;
			TestServer testServer = null;
			try
			{
				testServer = TestServer.Create(app =>
				{
					appBuilder = app;
					authenticationConfiguration.Setup(app);
				});

				// the middleware that has been configured isn't directly exposed anywhere so we resort to reflection...
				var middleware = typeof(Microsoft.Owin.Builder.AppBuilder)
					.GetField("_middleware", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance)
					.GetValue(appBuilder);

				return (IList<Tuple<Type, Delegate, object[]>>)middleware;
			}
			finally
			{
				if (testServer != null)
				{
					testServer.Dispose();
				}
			}
		}
	}
}
