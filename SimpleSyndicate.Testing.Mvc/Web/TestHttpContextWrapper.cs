﻿using System;
using System.Collections.Generic;
using System.Web;

namespace SimpleSyndicate.Testing.Mvc.Web
{
	/// <summary>
	/// Customized <see cref="HttpContextWrapper"/>, suitable for use in unit testing.
	/// </summary>
	/// <remarks>
	/// A <see cref="HttpContextWrapper"/> that doesn't wrap a real <see cref="HttpContext"/> can fail in unexpected ways; this custom
	/// version handles these failures so that it can be used for unit testing.
	/// </remarks>
	public class TestHttpContextWrapper : HttpContextWrapper
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="TestHttpContextWrapper"/> class using the specified context.
		/// </summary>
		/// <param name="httpContext">The HTTP context to wrap.</param>
		public TestHttpContextWrapper(HttpContext httpContext)
			: base(httpContext)
		{
		}

		/// <summary>
		/// Gets a <see cref="RequestNotification"/> value that indicates the current <see cref="HttpApplication"/> event that is processing.
		/// </summary>
		/// <remarks>
		/// If the wrapped <see cref="HttpContextWrapper"/> throws an exception, <see cref="RequestNotification.SendResponse"/> is returned.
		/// </remarks>
		/// <value>One of the <see cref="RequestNotification"/> values.</value>
		public override RequestNotification CurrentNotification
		{
			get
			{
				try
				{
					return base.CurrentNotification;
				}
				catch (PlatformNotSupportedException)
				{
					return RequestNotification.SendResponse;
				}
			}
		}

		/// <summary>
		/// Gets a value that indicates whether the request is an <see cref="System.Web.WebSockets.AspNetWebSocket"/> request.
		/// </summary>
		/// <remarks>
		/// If the wrapped <see cref="HttpContextWrapper"/> throws an exception, <c>false</c> is returned.
		/// </remarks>
		/// <value><c>true</c> if the request is an <see cref="System.Web.WebSockets.AspNetWebSocket"/> request; otherwise, <c>false</c>.</value>
		public override bool IsWebSocketRequest
		{
			get
			{
				try
				{
					return base.IsWebSocketRequest;
				}
				catch (PlatformNotSupportedException)
				{
					return false;
				}
			}
		}

		/// <summary>
		/// Gets the ordered list of protocols requested by the client.
		/// </summary>
		/// <remarks>
		/// If the wrapped <see cref="HttpContextWrapper"/> throws an exception, <c>null</c> is returned.
		/// </remarks>
		/// <value>The requested protocols, or <c>null</c> if this is not an <see cref="System.Web.WebSockets.AspNetWebSocket"/> request or if no list is present.</value>
		public override IList<string> WebSocketRequestedProtocols
		{
			get
			{
				try
				{
					return base.WebSocketRequestedProtocols;
				}
				catch (PlatformNotSupportedException)
				{
					return null;
				}
			}
		}
	}
}
