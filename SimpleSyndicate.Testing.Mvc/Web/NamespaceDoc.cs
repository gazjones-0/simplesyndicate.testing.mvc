﻿namespace SimpleSyndicate.Testing.Mvc.Web
{
	/// <summary>
	/// The <see cref="Web"/> namespace contains classes for use in testing <see cref="System.Web"/>.
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGeneratedAttribute]
	internal class NamespaceDoc
	{
	}
}
