﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSyndicate.Mvc.Controllers;

namespace SimpleSyndicate.Testing.Mvc.Web.Mvc
{
	/// <content>
	/// Contains extensions for GenericController{TBuilder, TCommand}.
	/// </content>
	public static partial class GenericControllerExtensions
	{
		/// <summary>
		/// Verifies that the <see cref="GenericController{TBuilder, TCommand}"/> has set a confirmation message for <see cref="ConfirmationMessageActionPerformed.EntityAdded"/>
		/// (via <see cref="GenericController.SetConfirmationMessage"/>). The assertion fails if the confirmation message hasn't been set.
		/// </summary>
		/// <typeparam name="TBuilder">The <see cref="Builder"/> to use for providing functionality for creating view models and related items.</typeparam>
		/// <typeparam name="TCommand">The <see cref="Command"/> to use for storing entities.</typeparam>
		/// <param name="controller">The <see cref="GenericController{TBuilder, TCommand}"/> instance that was verified.</param>
		/// <returns>The <see cref="GenericController{TBuilder, TCommand}"/> instance that this method extends.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// 
		/// // act
		/// someController.SomeMethod();
		/// 
		/// // assert
		/// someController.HasConfirmationMessageForEntityAdded();
		/// </code>
		/// </example>
		public static GenericController<TBuilder, TCommand> HasConfirmationMessageForEntityAdded<TBuilder, TCommand>(this GenericController<TBuilder, TCommand> controller)
			where TBuilder : Builder
			where TCommand : Command
		{
			return HasConfirmationMessage(controller, ConfirmationMessageActionPerformed.EntityAdded);
		}

		/// <summary>
		/// Verifies that the <see cref="GenericController{TBuilder, TCommand}"/> has set a confirmation message for <see cref="ConfirmationMessageActionPerformed.EntityUpdated"/>
		/// (via <see cref="GenericController.SetConfirmationMessage"/>). The assertion fails if the confirmation message hasn't been set.
		/// </summary>
		/// <typeparam name="TBuilder">The <see cref="Builder"/> to use for providing functionality for creating view models and related items.</typeparam>
		/// <typeparam name="TCommand">The <see cref="Command"/> to use for storing entities.</typeparam>
		/// <param name="controller">The <see cref="GenericController{TBuilder, TCommand}"/> instance that was verified.</param>
		/// <returns>The <see cref="GenericController{TBuilder, TCommand}"/> instance that this method extends.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// 
		/// // act
		/// someController.SomeMethod();
		/// 
		/// // assert
		/// someController.HasConfirmationMessageForEntityUpdated();
		/// </code>
		/// </example>
		public static GenericController<TBuilder, TCommand> HasConfirmationMessageForEntityUpdated<TBuilder, TCommand>(this GenericController<TBuilder, TCommand> controller)
			where TBuilder : Builder
			where TCommand : Command
		{
			return HasConfirmationMessage(controller, ConfirmationMessageActionPerformed.EntityUpdated);
		}

		/// <summary>
		/// Verifies that the <see cref="GenericController{TBuilder, TCommand}"/> has set a confirmation message for <see cref="ConfirmationMessageActionPerformed.EntityDeleted"/>
		/// (via <see cref="GenericController.SetConfirmationMessage"/>). The assertion fails if the confirmation message hasn't been set.
		/// </summary>
		/// <typeparam name="TBuilder">The <see cref="Builder"/> to use for providing functionality for creating view models and related items.</typeparam>
		/// <typeparam name="TCommand">The <see cref="Command"/> to use for storing entities.</typeparam>
		/// <param name="controller">The <see cref="GenericController{TBuilder, TCommand}"/> instance that was verified.</param>
		/// <returns>The <see cref="GenericController{TBuilder}"/> instance that this method extends.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// 
		/// // act
		/// someController.SomeMethod();
		/// 
		/// // assert
		/// someController.HasConfirmationMessageForEntityDeleted();
		/// </code>
		/// </example>
		public static GenericController<TBuilder, TCommand> HasConfirmationMessageForEntityDeleted<TBuilder, TCommand>(this GenericController<TBuilder, TCommand> controller)
			where TBuilder : Builder
			where TCommand : Command
		{
			return HasConfirmationMessage(controller, ConfirmationMessageActionPerformed.EntityDeleted);
		}

		/// <summary>
		/// Verifies that the <see cref="GenericController{TBuilder, TCommand}"/> has set a confirmation message of the specified type (via
		/// <see cref="GenericController.SetConfirmationMessage"/>). The assertion fails if the confirmation message hasn't been set.
		/// </summary>
		/// <typeparam name="TBuilder">The <see cref="Builder"/> to use for providing functionality for creating view models and related items.</typeparam>
		/// <typeparam name="TCommand">The <see cref="Command"/> to use for storing entities.</typeparam>
		/// <param name="controller">The <see cref="GenericController{TBuilder, TCommand}"/> instance that was verified.</param>
		/// <param name="actionPerformed">The <see cref="ConfirmationMessageActionPerformed"/> to check for.</param>
		/// <returns>The <see cref="GenericController{TBuilder, TCommand}"/> instance that this method extends.</returns>
		private static GenericController<TBuilder, TCommand> HasConfirmationMessage<TBuilder, TCommand>(GenericController<TBuilder, TCommand> controller, ConfirmationMessageActionPerformed actionPerformed)
			where TBuilder : Builder
			where TCommand : Command
		{
			Assert.IsNotNull(controller, "Failed check for confirmation message; Controller is null");
			Assert.IsNotNull(controller.TempData, "Failed check for confirmation message; TempData in Controller is null");
			ConfirmationMessageViewModel confirmationMessage = null;
			try
			{
				confirmationMessage = controller.ConfirmationMessage();
			}
			catch (InvalidOperationException)
			{
				Assert.Fail("Failed check for confirmation message; caught NullReferenceException when calling GenericController<TBuilder, TCommand>.ConfirmationMessage(); have you called SetConfirmationMessage?");
			}
			Assert.IsInstanceOfType(confirmationMessage, typeof(ConfirmationMessageViewModel), "Failed check for confirmation message, confirmation message was returned but is not of type ConfirmationMessageViewModel!");
			Assert.AreEqual(actionPerformed, ((ConfirmationMessageViewModel)confirmationMessage).ActionPerformed, "Failed check for confirmation message, ConfirmationMessageViewModel.Type is not expected value; did you set the right confirmation message?");
			return controller;
		}
	}
}
