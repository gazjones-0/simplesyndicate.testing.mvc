﻿namespace SimpleSyndicate.Testing.Mvc.Web.Mvc
{
	/// <summary>
	/// The <see cref="Mvc"/> namespace contains classes for use in testing <see cref="System.Web.Mvc"/>.
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGeneratedAttribute]
	internal class NamespaceDoc
	{
	}
}
