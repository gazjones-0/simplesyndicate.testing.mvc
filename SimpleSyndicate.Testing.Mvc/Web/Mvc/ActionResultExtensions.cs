﻿using System.Net;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SimpleSyndicate.Testing.Mvc.Web.Mvc
{
	/// <summary>
	/// Extends <see cref="ActionResult"/> providing various assertions in a fluent syntax.
	/// </summary>
	/// <remarks>
	/// The <see cref="ActionResultExtensions"/> class contains methods that extend the <see cref="ActionResult"/> class. Each method returns the
	/// <see cref="ActionResult"/> instance the method extends so that they can be chained together.
	/// </remarks>
	/// <example>
	/// <code language="cs">
	/// using SimpleSyndicate.Testing.Mvc.Web.Mvc;
	/// 
	/// namespace MyMvcApp.Tests.Controllers
	/// {
	/// 	[TestClass]
	/// 	public class SomeControllerTests
	/// 	{
	/// 		[TestMethod]
	/// 		public void SomeMethodReturnsNotFound()
	/// 		{
	/// 			// arrange
	/// 			var controller = new SomeController();
	/// 
	/// 			// act
	/// 			ActionResult actionResult = controller.SomeMethod();
	/// 
	/// 			// assert
	/// 			actionResult
	/// 				.IsNotNull()
	/// 				.IsNotFound();
	/// 		}
	/// 	}
	/// </code>
	/// </example>
	public static class ActionResultExtensions
	{
		/// <summary>
		/// Verifies that the <see cref="ActionResult"/> is a bad request response (i.e. is a <see cref="HttpStatusCodeResult"/> with a status code of
		/// <see cref="HttpStatusCode.BadRequest"/>). The assertion fails if the <see cref="ActionResult"/> is not a bad request response.
		/// </summary>
		/// <param name="actionResult">The <see cref="ActionResult"/> instance that this method extends.</param>
		/// <returns>The <see cref="ActionResult"/> instance that was verified.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// 
		/// // act
		/// var actionResult = someController.SomeMethod();
		/// 
		/// // assert
		/// actionResult.IsBadRequest();
		/// </code>
		/// </example>
		public static ActionResult IsBadRequest(this ActionResult actionResult)
		{
			Assert.IsInstanceOfType(actionResult, typeof(HttpStatusCodeResult));
			Assert.AreEqual((int)HttpStatusCode.BadRequest, ((HttpStatusCodeResult)actionResult).StatusCode, "StatusCode did not match expected value");
			return actionResult;
		}

		/// <summary>
		/// Verifies that the <see cref="ActionResult"/> is JSON (i.e. is a <see cref="JsonResult"/>). The assertion fails if the <see cref="ActionResult"/> is not JSON.
		/// </summary>
		/// <param name="actionResult">The <see cref="ActionResult"/> instance that this method extends.</param>
		/// <returns>The <see cref="ActionResult"/> instance that was verified.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// 
		/// // act
		/// var actionResult = someController.SomeMethod();
		/// 
		/// // assert
		/// actionResult.IsJsonResult();
		/// </code>
		/// </example>
		public static ActionResult IsJsonResult(this ActionResult actionResult)
		{
			Assert.IsInstanceOfType(actionResult, typeof(JsonResult), "ActionResult is not of expected type {0}", typeof(JsonResult).Name);
			return actionResult;
		}

		/// <summary>
		/// Verifies that the <see cref="ActionResult"/> is a not found HTTP response (i.e. is a <see cref="HttpNotFoundResult"/>). The assertion fails
		/// if the <see cref="ActionResult"/> is not a not found HTTP response.
		/// </summary>
		/// <param name="actionResult">The <see cref="ActionResult"/> instance that this method extends.</param>
		/// <returns>The <see cref="ActionResult"/> instance that was verified.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// 
		/// // act
		/// var actionResult = someController.SomeMethod();
		/// 
		/// // assert
		/// actionResult.IsNotFound();
		/// </code>
		/// </example>
		public static ActionResult IsNotFound(this ActionResult actionResult)
		{
			Assert.IsInstanceOfType(actionResult, typeof(HttpNotFoundResult), "ActionResult is not of expected type {0}", typeof(HttpNotFoundResult).Name);
			return actionResult;
		}

		/// <summary>
		/// Verifies that the <see cref="ActionResult"/> is not <c>null</c>. The assertion fails if the <see cref="ActionResult"/> is <c>null</c>.
		/// </summary>
		/// <param name="actionResult">The <see cref="ActionResult"/> instance that this method extends.</param>
		/// <returns>The <see cref="ActionResult"/> instance that was verified.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// 
		/// // act
		/// var actionResult = someController.SomeMethod();
		/// 
		/// // assert
		/// actionResult.IsNotNull();
		/// </code>
		/// </example>
		public static ActionResult IsNotNull(this ActionResult actionResult)
		{
			Assert.IsNotNull(actionResult, "ActionResult is null");
			return actionResult;
		}

		/// <summary>
		/// Verifies that the <see cref="ActionResult"/> is a redirect (i.e. is a <see cref="RedirectResult"/>). The assertion fails if the
		/// <see cref="ActionResult"/> is not a redirect.
		/// </summary>
		/// <param name="actionResult">The <see cref="ActionResult"/> instance that this method extends.</param>
		/// <returns>The <see cref="ActionResult"/> instance that was verified.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// 
		/// // act
		/// var actionResult = someController.SomeMethod();
		/// 
		/// // assert
		/// actionResult.IsRedirectResult();
		/// </code>
		/// </example>
		public static ActionResult IsRedirectResult(this ActionResult actionResult)
		{
			Assert.IsInstanceOfType(actionResult, typeof(RedirectResult), "ActionResult is not of expected type {0}", typeof(RedirectResult).Name);
			return actionResult;
		}

		/// <overloads>
		/// <summary>
		/// Verifies that the <see cref="ActionResult"/> is a redirect to route (i.e. is a <see cref="RedirectToRouteResult"/>).
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Verifies that the <see cref="ActionResult"/> is a redirect to route (i.e. is a <see cref="RedirectToRouteResult"/>). The assertion
		/// fails if the <see cref="ActionResult"/> is not a redirect to route.
		/// </summary>
		/// <param name="actionResult">The <see cref="ActionResult"/> instance that this method extends.</param>
		/// <returns>The <see cref="ActionResult"/> instance that was verified.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// 
		/// // act
		/// var actionResult = someController.SomeMethod();
		/// 
		/// // assert
		/// actionResult.IsRedirectToRouteResult();
		/// </code>
		/// </example>
		public static ActionResult IsRedirectToRouteResult(this ActionResult actionResult)
		{
			Assert.IsInstanceOfType(actionResult, typeof(RedirectToRouteResult), "ActionResult is not of expected type {0}", typeof(RedirectToRouteResult).Name);
			return actionResult;
		}

		/// <summary>
		/// Verifies that the <see cref="ActionResult"/> is a redirect to the specified action (i.e. is a <see cref="RedirectToRouteResult"/>).
		/// The assertion fails if the <see cref="ActionResult"/> is not a redirect to the specified action.
		/// </summary>
		/// <param name="actionResult">The <see cref="ActionResult"/> instance that this method extends.</param>
		/// <param name="expectedAction">The name of the action the redirect is expected to be to.</param>
		/// <returns>The <see cref="ActionResult"/> instance that was verified.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// 
		/// // act
		/// var actionResult = someController.SomeMethod();
		/// 
		/// // assert
		/// actionResult.IsRedirectToRouteResult("Index");
		/// </code>
		/// </example>
		public static ActionResult IsRedirectToRouteResult(this ActionResult actionResult, string expectedAction)
		{
			Assert.IsInstanceOfType(actionResult, typeof(RedirectToRouteResult), "ActionResult is not of expected type {0}", typeof(RedirectToRouteResult).Name);
			actionResult.RedirectToRouteResult().ActionIs(expectedAction);
			return actionResult;
		}

		/// <summary>
		/// Verifies that the <see cref="ActionResult"/> is a redirect to the specified action and controller (i.e. is a <see cref="RedirectToRouteResult"/>).
		/// The assertion fails if the <see cref="ActionResult"/> is not a redirect to the specified action and controller..
		/// </summary>
		/// <param name="actionResult">The <see cref="ActionResult"/> instance that this method extends.</param>
		/// <param name="expectedAction">The name of the action the redirect is expected to be to.</param>
		/// <param name="expectedController">The name of the controller the redirect is expected to be to.</param>
		/// <returns>The <see cref="ActionResult"/> instance that was verified.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// 
		/// // act
		/// var actionResult = someController.SomeMethod();
		/// 
		/// // assert
		/// actionResult.IsRedirectToRouteResult("Index", "SomeController");
		/// </code>
		/// </example>
		public static ActionResult IsRedirectToRouteResult(this ActionResult actionResult, string expectedAction, string expectedController)
		{
			Assert.IsInstanceOfType(actionResult, typeof(RedirectToRouteResult), "ActionResult is not of expected type {0}", typeof(RedirectToRouteResult).Name);
			actionResult.RedirectToRouteResult().ActionIs(expectedAction);
			actionResult.RedirectToRouteResult().ControllerIs(expectedController);
			return actionResult;
		}

		/// <summary>
		/// Verifies that the <see cref="ActionResult"/> is a view (i.e. is a <see cref="ViewResult"/>).
		/// The assertion fails if the <see cref="ActionResult"/> is not a view.
		/// </summary>
		/// <param name="actionResult">The <see cref="ActionResult"/> instance that this method extends.</param>
		/// <returns>The <see cref="ActionResult"/> instance that was verified.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// 
		/// // act
		/// var actionResult = someController.SomeMethod();
		/// 
		/// // assert
		/// actionResult.IsViewResult();
		/// </code>
		/// </example>
		public static ActionResult IsViewResult(this ActionResult actionResult)
		{
			Assert.IsInstanceOfType(actionResult, typeof(ViewResult), "ActionResult is not of expected type {0}", typeof(ViewResult).Name);
			return actionResult;
		}

		/// <summary>
		/// Verifies that the <see cref="ActionResult"/> is JSON (i.e. is a <see cref="JsonResult"/>) and then
		/// returns the <see cref="ActionResult"/> being extended as a <see cref="JsonResult"/>.
		/// </summary>
		/// <param name="actionResult">The <see cref="ActionResult"/> instance that this method extends.</param>
		/// <returns>The <see cref="ActionResult"/> instance that this method extends as a <see cref="JsonResult"/>.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// 
		/// // act
		/// var actionResult = someController.SomeMethod();
		/// 
		/// // assert
		/// actionResult
		///		.JsonResult()
		///		.DataIs&lt;SomeType&gt;();
		/// </code>
		/// </example>
		public static JsonResult JsonResult(this ActionResult actionResult)
		{
			IsJsonResult(actionResult);
			return (JsonResult)actionResult;
		}

		/// <summary>
		/// Verifies that the <see cref="ActionResult"/> is a redirect (i.e. is a <see cref="RedirectResult"/>) and then
		/// returns the <see cref="ActionResult"/> being extended as a <see cref="RedirectResult"/>.
		/// </summary>
		/// <param name="actionResult">The <see cref="ActionResult"/> instance that this method extends.</param>
		/// <returns>The <see cref="ActionResult"/> instance that this method extends as a <see cref="RedirectResult"/>.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// 
		/// // act
		/// var actionResult = someController.SomeMethod();
		/// 
		/// // assert
		/// actionResult
		///		.RedirectResult()
		///		.IsPermanent();
		/// </code>
		/// </example>
		public static RedirectResult RedirectResult(this ActionResult actionResult)
		{
			IsRedirectResult(actionResult);
			return (RedirectResult)actionResult;
		}

		/// <summary>
		/// Verifies that the <see cref="ActionResult"/> is a redirect to route (i.e. is a <see cref="RedirectToRouteResult"/>) and then
		/// returns the <see cref="ActionResult"/> being extended as a <see cref="RedirectToRouteResult"/>.
		/// </summary>
		/// <param name="actionResult">The <see cref="ActionResult"/> instance that this method extends.</param>
		/// <returns>The <see cref="ActionResult"/> instance that this method extends as a <see cref="RedirectToRouteResult"/>.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// 
		/// // act
		/// var actionResult = someController.SomeMethod();
		/// 
		/// // assert
		/// actionResult
		///		.RedirectToRouteResult()
		///		.ActionIs("Index");
		/// </code>
		/// </example>
		public static RedirectToRouteResult RedirectToRouteResult(this ActionResult actionResult)
		{
			IsRedirectToRouteResult(actionResult);
			return (RedirectToRouteResult)actionResult;
		}

		/// <summary>
		/// Verifies that the <see cref="ActionResult"/> is a view (i.e. is a <see cref="ViewResult"/>) and then
		/// returns the <see cref="ActionResult"/> being extended as a <see cref="ViewResult"/>.
		/// </summary>
		/// <param name="actionResult">The <see cref="ActionResult"/> instance that this method extends.</param>
		/// <returns>The <see cref="ActionResult"/> instance that this method extends as a <see cref="ViewResult"/>.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// 
		/// // act
		/// var actionResult = someController.SomeMethod();
		/// 
		/// // assert
		/// actionResult
		///		.ViewResult()
		///		.IsDefaultView();
		/// </code>
		/// </example>
		public static ViewResult ViewResult(this ActionResult actionResult)
		{
			IsViewResult(actionResult);
			return (ViewResult)actionResult;
		}
	}
}
