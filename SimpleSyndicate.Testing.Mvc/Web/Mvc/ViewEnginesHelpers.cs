﻿using System;
using System.Web.Mvc;

namespace SimpleSyndicate.Testing.Mvc.Web.Mvc
{
	/// <summary>
	/// Helper methods for unit testing view engines.
	/// </summary>
	public static class ViewEnginesHelpers
	{
		/// <summary>
		/// Returns whether the specified view engine only supports C#.
		/// </summary>
		/// <param name="viewEngine">The view engine.</param>
		/// <returns><c>true</c> if the view engine only supports C#; otherwise, <c>false</c>.</returns>
		/// <exception cref="ArgumentNullException">Thrown when <paramref name="viewEngine"/> is <c>null</c>.</exception>
		public static bool OnlyHandlesCSharp(VirtualPathProviderViewEngine viewEngine)
		{
			if (viewEngine == null)
			{
				throw new ArgumentNullException("viewEngine");
			}
			if (!OnlyContainsCSharpItems(viewEngine.AreaMasterLocationFormats))
			{
				return false;
			}
			if (!OnlyContainsCSharpItems(viewEngine.AreaPartialViewLocationFormats))
			{
				return false;
			}
			if (!OnlyContainsCSharpItems(viewEngine.AreaViewLocationFormats))
			{
				return false;
			}
			if (!OnlyContainsCSharpItems(viewEngine.FileExtensions))
			{
				return false;
			}
			if (!OnlyContainsCSharpItems(viewEngine.MasterLocationFormats))
			{
				return false;
			}
			if (!OnlyContainsCSharpItems(viewEngine.PartialViewLocationFormats))
			{
				return false;
			}
			if (!OnlyContainsCSharpItems(viewEngine.ViewLocationFormats))
			{
				return false;
			}
			return true;
		}

		/// <summary>
		/// Returns whether the specified array only has C# items in it.
		/// </summary>
		/// <param name="array">The array.</param>
		/// <returns><c>true</c> if the array only contains C# items; otherwise, <c>false</c>.</returns>
		private static bool OnlyContainsCSharpItems(string[] array)
		{
			foreach (var item in array)
			{
				if (!item.EndsWith("cshtml", System.StringComparison.OrdinalIgnoreCase))
				{
					return false;
				}
			}
			return true;
		}
	}
}
