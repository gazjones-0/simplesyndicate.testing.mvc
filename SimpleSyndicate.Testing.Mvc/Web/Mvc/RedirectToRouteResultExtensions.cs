﻿using System;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SimpleSyndicate.Testing.Mvc.Web.Mvc
{
	/// <summary>
	/// Extends <see cref="RedirectToRouteResult"/> providing various functionality and assertions in a fluent syntax.
	/// </summary>
	/// <remarks>
	/// The <see cref="RedirectToRouteResultExtensions"/> class contains methods that extend the <see cref="RedirectToRouteResult"/> class. Where applicable,
	/// each method returns the <see cref="RedirectToRouteResult"/> instance the method extends so that they can be chained together.
	/// </remarks>
	/// <example>
	/// <code language="cs">
	/// using SimpleSyndicate.Testing.Mvc.Web.Mvc;
	/// 
	/// namespace MyMvcApp.Tests.Controllers
	/// {
	/// 	[TestClass]
	/// 	public class SomeControllerTests
	/// 	{
	/// 		[TestMethod]
	/// 		public void SomeMethodRedirectsToOtherControllerMethod()
	/// 		{
	/// 			// arrange
	/// 			var controller = new SomeController();
	/// 
	/// 			// act
	/// 			RedirectToRouteResult redirectToRouteResult = controller.SomeMethod();
	/// 
	/// 			// assert
	/// 			redirectToRouteResult
	/// 				.ControllerIs("OtherController")
	/// 				.ActionIs("SomeAction");
	/// 		}
	/// 	}
	/// </code>
	/// </example>
	public static class RedirectToRouteResultExtensions
	{
		/// <summary>
		/// Returns the name of the action the <see cref="RedirectToRouteResult"/> is going to.
		/// </summary>
		/// <param name="redirectToRouteResult">The <see cref="RedirectToRouteResult"/> instance that this method extends.</param>
		/// <returns>The name of the action, or <c>null</c> if there's no action route.</returns>
		/// <example>
		/// <code language="cs">
		/// var someController = new SomeController();
		/// var redirectToRouteResult = someController.SomeMethod();
		/// var action = redirectToRouteResult.Action();
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Justification = "Argument is validated by an assertion.")]
		public static string Action(this RedirectToRouteResult redirectToRouteResult)
		{
			Assert.IsNotNull(redirectToRouteResult, "Failed to get Action from RedirectToRouteResult; RedirectToRouteResult is null");
			Assert.IsNotNull(redirectToRouteResult.RouteValues, "Failed to get Action from RedirectToRouteResult; RedirectToRouteResult.RouteValues is null");
			var action = redirectToRouteResult.RouteValues["action"];
			if (action != null)
			{
				return action.ToString();
			}
			return null;
		}

		/// <summary>
		/// Verifies that the name of the action the <see cref="RedirectToRouteResult"/> is going to is the expected action name.
		/// The assertion fails if the name of the action isn't the expected action name.
		/// </summary>
		/// <param name="redirectToRouteResult">The <see cref="RedirectToRouteResult"/> instance that this method extends.</param>
		/// <param name="expectedAction">The name of the action the redirect is expected to be going to.</param>
		/// <returns>The <see cref="RedirectToRouteResult"/> instance that was verified.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// 
		/// // act
		/// var redirectToRouteResult = someController.SomeMethod();
		/// 
		/// // assert
		/// redirectToRouteResult.ActionIs("Index");
		/// </code>
		/// </example>
		public static RedirectToRouteResult ActionIs(this RedirectToRouteResult redirectToRouteResult, string expectedAction)
		{
			var actualAction = Action(redirectToRouteResult);
			Assert.AreEqual(expectedAction, actualAction, "Action in RedirectToRouteResult is not expected value");
			return redirectToRouteResult;
		}

		/// <summary>
		/// Returns the name of the controller the <see cref="RedirectToRouteResult"/> is going to.
		/// </summary>
		/// <param name="redirectToRouteResult">The <see cref="RedirectToRouteResult"/> instance that this method extends.</param>
		/// <returns>The name of the controller, or <c>null</c> if there's no controller route.</returns>
		/// <example>
		/// <code language="cs">
		/// var someController = new SomeController();
		/// var redirectToRouteResult = someController.SomeMethod();
		/// var controller = redirectToRouteResult.Controller();
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Justification = "Argument is validated by an assertion.")]
		public static string Controller(this RedirectToRouteResult redirectToRouteResult)
		{
			Assert.IsNotNull(redirectToRouteResult, "Failed to get Controller from RedirectToRouteResult; RedirectToRouteResult is null");
			Assert.IsNotNull(redirectToRouteResult.RouteValues, "Failed to get Controller from RedirectToRouteResult; RedirectToRouteResult.RouteValues is null");
			var controller = redirectToRouteResult.RouteValues["controller"];
			if (controller != null)
			{
				return controller.ToString();
			}
			return null;
		}

		/// <summary>
		/// Verifies that the name of the controller the <see cref="RedirectToRouteResult"/> is going to is the expected controller name.
		/// The assertion fails if the name of the controller isn't the expected controller name.
		/// </summary>
		/// <param name="redirectToRouteResult">The <see cref="RedirectToRouteResult"/> instance that this method extends.</param>
		/// <param name="expectedController">The name of the controller the redirect is expected to be going to.</param>
		/// <returns>The <see cref="RedirectToRouteResult"/> instance that was verified.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// 
		/// // act
		/// var redirectToRouteResult = someController.SomeMethod();
		/// 
		/// // assert
		/// redirectToRouteResult.ControllerIs("SomeController");
		/// </code>
		/// </example>
		public static RedirectToRouteResult ControllerIs(this RedirectToRouteResult redirectToRouteResult, string expectedController)
		{
			var actualController = Controller(redirectToRouteResult);
			Assert.AreEqual(expectedController, actualController, "Controller in RedirectToRouteResult is not expected value");
			return redirectToRouteResult;
		}

		/// <summary>
		/// Verifies that the <see cref="RedirectToRouteResult"/> has the necessary routing for paging.
		/// The assertion fails if the routing necessary for paging isn't present.
		/// </summary>
		/// <param name="redirectToRouteResult">The <see cref="RedirectToRouteResult"/> instance that this method extends.</param>
		/// <returns>The <see cref="RedirectToRouteResult"/> instance that was verified.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// 
		/// // act
		/// var redirectToRouteResult = someController.SomeMethod();
		/// 
		/// // assert
		/// redirectToRouteResult.HasPaging();
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Justification = "Argument is validated by an assertion.")]
		public static RedirectToRouteResult HasPaging(this RedirectToRouteResult redirectToRouteResult)
		{
			Assert.IsNotNull(redirectToRouteResult, "Failed to check for paging; RedirectToRouteResult is null");
			Assert.IsNotNull(redirectToRouteResult.RouteValues, "Failed to check for paging; RedirectToRouteResult.RouteValues is null");
			Assert.AreEqual(true, redirectToRouteResult.RouteValues.ContainsKey("grid-page"), "Failed to check for paging; RouteValues doesn't contain \"grid-page\" key");
			Assert.AreNotEqual(String.Empty, redirectToRouteResult.RouteValues["grid-page"].ToString(), "Failed to check for paging; \"grid-page\" is blank");
			return redirectToRouteResult;
		}

		/// <summary>
		/// Verifies that the <see cref="RedirectToRouteResult"/> has a route key with the expected name.
		/// The assertion fails if there's no route key with the specified name.
		/// </summary>
		/// <param name="redirectToRouteResult">The <see cref="RedirectToRouteResult"/> instance that this method extends.</param>
		/// <param name="expectedKey">The name of the key expected to be in the route.</param>
		/// <returns>The <see cref="RedirectToRouteResult"/> instance that was verified.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// 
		/// // act
		/// var redirectToRouteResult = someController.SomeMethod();
		/// 
		/// // assert
		/// redirectToRouteResult.HasRouteValue("someKey");
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Justification = "Argument is validated by an assertion.")]
		public static RedirectToRouteResult HasRouteValue(this RedirectToRouteResult redirectToRouteResult, string expectedKey)
		{
			Assert.IsNotNull(redirectToRouteResult, "Failed to check for route value; RedirectToRouteResult is null (expected to find \"{0}\" key)", expectedKey);
			Assert.IsNotNull(redirectToRouteResult.RouteValues, "Failed to check for route value; RedirectToRouteResult.RouteValues is null (expected to find \"{0}\" key)", expectedKey);
			Assert.AreEqual(true, redirectToRouteResult.RouteValues.ContainsKey(expectedKey), "RedirectToRouteResult.RouteValues does not contain \"{0}\" key", expectedKey);
			return redirectToRouteResult;
		}

		/// <summary>
		/// Verifies that the <see cref="RedirectToRouteResult"/> is not <c>null</c>. The assertion fails if the <see cref="RedirectToRouteResult"/> is <c>null</c>.
		/// </summary>
		/// <param name="redirectToRouteResult">The <see cref="RedirectToRouteResult"/> instance that this method extends.</param>
		/// <returns>The <see cref="RedirectToRouteResult"/> instance that was verified.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// 
		/// // act
		/// var redirectToRouteResult = someController.SomeMethod();
		/// 
		/// // assert
		/// redirectToRouteResult.IsNotNull();
		/// </code>
		/// </example>
		public static RedirectToRouteResult IsNotNull(this RedirectToRouteResult redirectToRouteResult)
		{
			Assert.IsNotNull(redirectToRouteResult, "RedirectToRouteResult is null");
			return redirectToRouteResult;
		}
	}
}
