﻿using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SimpleSyndicate.Testing.Mvc.Web.Mvc
{
	/// <summary>
	/// Extends <see cref="RedirectResult"/> providing various functionality and assertions in a fluent syntax.
	/// </summary>
	/// <remarks>
	/// The <see cref="RedirectResultExtensions"/> class contains methods that extend the <see cref="RedirectResult"/> class. Where applicable,
	/// each method returns the <see cref="RedirectResult"/> instance the method extends so that they can be chained together.
	/// </remarks>
	/// <example>
	/// <code language="cs">
	/// using SimpleSyndicate.Testing.Mvc.Web.Mvc;
	/// 
	/// namespace MyMvcApp.Tests.Controllers
	/// {
	/// 	[TestClass]
	/// 	public class SomeControllerTests
	/// 	{
	/// 		[TestMethod]
	/// 		public void SomeMethodPermanentlyRedirects()
	/// 		{
	/// 			// arrange
	/// 			var controller = new SomeController();
	/// 
	/// 			// act
	/// 			RedirectResult redirectResult = controller.SomeMethod();
	/// 
	/// 			// assert
	/// 			redirectResult.IsPermanent();
	/// 		}
	/// 	}
	/// </code>
	/// </example>
	public static class RedirectResultExtensions
	{
		/// <summary>
		/// Verifies that the <see cref="RedirectResult"/> is not <c>null</c>. The assertion fails if the <see cref="RedirectResult"/> is <c>null</c>.
		/// </summary>
		/// <param name="redirectResult">The <see cref="RedirectResult"/> instance that this method extends.</param>
		/// <returns>The <see cref="RedirectResult"/> instance that was verified.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// 
		/// // act
		/// var redirectResult = someController.SomeMethod();
		/// 
		/// // assert
		/// redirectResult.IsNotNull();
		/// </code>
		/// </example>
		public static RedirectResult IsNotNull(this RedirectResult redirectResult)
		{
			Assert.IsNotNull(redirectResult, "RedirectResult is null");
			return redirectResult;
		}

		/// <summary>
		/// Verifies that the <see cref="RedirectResult"/> is not a permanent redirection.
		/// The assertion fails if the redirection is permanent.
		/// </summary>
		/// <param name="redirectResult">The <see cref="RedirectResult"/> instance that this method extends.</param>
		/// <returns>The <see cref="RedirectResult"/> instance that was verified.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// 
		/// // act
		/// var redirectResult = someController.SomeMethod();
		/// 
		/// // assert
		/// redirectResult.IsNotPermanent();
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Justification = "Argument is validated by an assertion.")]
		public static RedirectResult IsNotPermanent(this RedirectResult redirectResult)
		{
			Assert.IsNotNull(redirectResult, "Failed to get Permanent from RedirectResult; RedirectResult is null");
			Assert.AreEqual(false, redirectResult.Permanent, "Redirection is permanent; expected it to not be permanent.");
			return redirectResult;
		}

		/// <summary>
		/// Verifies that the <see cref="RedirectResult"/> is a permanent redirection.
		/// The assertion fails if the redirection is not permanent.
		/// </summary>
		/// <param name="redirectResult">The <see cref="RedirectResult"/> instance that this method extends.</param>
		/// <returns>The <see cref="RedirectResult"/> instance that was verified.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// 
		/// // act
		/// var redirectResult = someController.SomeMethod();
		/// 
		/// // assert
		/// redirectResult.IsPermanent();
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Justification = "Argument is validated by an assertion.")]
		public static RedirectResult IsPermanent(this RedirectResult redirectResult)
		{
			Assert.IsNotNull(redirectResult, "Failed to get Permanent from RedirectResult; RedirectResult is null");
			Assert.AreEqual(true, redirectResult.Permanent, "Redirection is not permanent; expected it to be permanent.");
			return redirectResult;
		}

		/// <summary>
		/// Verifies that the target of the <see cref="RedirectResult"/> is the specified value.
		/// The assertion fails if the target Url is not the expected value.
		/// </summary>
		/// <param name="redirectResult">The <see cref="RedirectResult"/> instance that this method extends.</param>
		/// <param name="expectedUrl">The target Url that the redirect is expected to be to.</param>
		/// <returns>The <see cref="RedirectResult"/> instance that was verified.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// 
		/// // act
		/// var redirectResult = someController.SomeMethod();
		/// 
		/// // assert
		/// redirectResult.UrlIs("http://tempuri.org");
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1054:UriParametersShouldNotBeStrings", MessageId = "1#", Justification = "RedirectResult.Url, which is being verified, is itself a string")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Justification = "Argument is validated by an assertion.")]
		public static RedirectResult UrlIs(this RedirectResult redirectResult, string expectedUrl)
		{
			Assert.IsNotNull(redirectResult, "Failed to get Url from RedirectResult; RedirectResult is null");
			var actualUrl = redirectResult.Url;
			Assert.AreEqual(expectedUrl, actualUrl, "Url in RedirectResult is not expected value");
			return redirectResult;
		}
	}
}
