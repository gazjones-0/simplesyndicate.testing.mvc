﻿using System;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SimpleSyndicate.Testing.Mvc.Web.Mvc
{
	/// <summary>
	/// Extends <see cref="JsonResult"/> providing various assertions in a fluent syntax.
	/// </summary>
	/// <remarks>
	/// The <see cref="JsonResultExtensions"/> class contains methods that extend the <see cref="JsonResult"/> class. Each method returns the
	/// <see cref="JsonResult"/> instance the method extends so that they can be chained together.
	/// </remarks>
	/// <example>
	/// <code language="cs">
	/// using SimpleSyndicate.Testing.Mvc.Web.Mvc;
	/// 
	/// namespace MyMvcApp.Tests.Controllers
	/// {
	/// 	[TestClass]
	/// 	public class SomeControllerTests
	/// 	{
	/// 		[TestMethod]
	/// 		public void SomeMethodReturnsJson()
	/// 		{
	/// 			// arrange
	/// 			var controller = new SomeController();
	/// 
	/// 			// act
	/// 			JsonResult jsonResult = controller.SomeMethod();
	/// 
	/// 			// assert
	/// 			jsonResult
	/// 				.IsNotNull()
	/// 				.DataIs&lt;SomeType&gt;();
	/// 		}
	/// 	}
	/// </code>
	/// </example>
	public static class JsonResultExtensions
	{
		/// <overloads>
		/// <summary>
		/// Verifies that the <see cref="JsonResult"/> <see cref="JsonResult.Data"/> is of the specified type.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Verifies that the <see cref="JsonResult"/> <see cref="JsonResult.Data"/> is of the specified type. The assertion fails if
		/// the <see cref="JsonResult"/> is not of the specified type.
		/// </summary>
		/// <typeparam name="TType">Type the <see cref="JsonResult.Data"/> is expected to be.</typeparam>
		/// <param name="jsonResult">The <see cref="JsonResult"/> instance that this method extends.</param>
		/// <returns>The <see cref="JsonResult"/> instance that was verified.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// 
		/// // act
		/// var jsonResult = someController.SomeMethod();
		/// 
		/// // assert
		/// jsonResult.DataIs&lt;SomeType&gt;();
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Justification = "Clients shouldn't need to instantiate a filter just to check if a filter of that type is present.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Justification = "Argument is validated by an assertion.")]
		public static JsonResult DataIs<TType>(this JsonResult jsonResult)
		{
			Assert.IsNotNull(jsonResult, "JsonResult is null");
			Assert.IsNotNull(jsonResult.Data, "JsonResult.Data is null");
			Assert.IsInstanceOfType(jsonResult.Data, typeof(TType), "JsonResult.Data is not of expected type {0}", typeof(TType).Name);
			return jsonResult;
		}

		/// <summary>
		/// Verifies that the <see cref="JsonResult"/> <see cref="JsonResult.Data"/> is of the specified type. The assertion fails if
		/// the <see cref="JsonResult"/> is not of the specified type.
		/// </summary>
		/// <typeparam name="TType">Type the <see cref="JsonResult.Data"/> is expected to be.</typeparam>
		/// <param name="jsonResult">The <see cref="JsonResult"/> instance that this method extends.</param>
		/// <param name="objectOfTypeDataIsExpectedToBe">An object that is of the type the <see cref="JsonResult.Data"/> is expected to be.</param>
		/// <returns>The <see cref="JsonResult"/> instance that was verified.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// var someObject = new SomeObject();
		/// 
		/// // act
		/// var jsonResult = someController.SomeMethod();
		/// 
		/// // assert
		/// jsonResult.DataIs(someObject);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Justification = "Arguments are validated by assertions.")]
		public static JsonResult DataIs<TType>(this JsonResult jsonResult, TType objectOfTypeDataIsExpectedToBe)
		{
			Assert.IsNotNull(jsonResult, "JsonResult is null");
			Assert.IsNotNull(jsonResult.Data, "JsonResult.Data is null");
			Assert.IsNotNull(objectOfTypeDataIsExpectedToBe, "objectOfExpectedType is null");
			Assert.IsInstanceOfType(jsonResult.Data, objectOfTypeDataIsExpectedToBe.GetType(), "JsonResult.Data is not of expected type {0}", objectOfTypeDataIsExpectedToBe.GetType().Name);
			return jsonResult;
		}

		/// <summary>
		/// Verifies that the <see cref="JsonResult"/> <see cref="JsonResult.Data"/> is of the specified type. The assertion fails if
		/// the <see cref="JsonResult"/> is not of the specified type.
		/// </summary>
		/// <param name="jsonResult">The <see cref="JsonResult"/> instance that this method extends.</param>
		/// <param name="typeDataIsExpectedToBe">Type the <see cref="JsonResult.Data"/> is expected to be..</param>
		/// <returns>The <see cref="JsonResult"/> instance that was verified.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// var someObject = new SomeObject();
		/// 
		/// // act
		/// var jsonResult = someController.SomeMethod();
		/// 
		/// // assert
		/// jsonResult.DataIs(typeof(someObject));
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters", Justification = "More derived type required to pass to assertion.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Justification = "Arguments are validated by assertions.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "1", Justification = "Arguments are validated by assertions.")]
		public static JsonResult DataIs(this JsonResult jsonResult, Type typeDataIsExpectedToBe)
		{
			Assert.IsNotNull(jsonResult, "JsonResult is null");
			Assert.IsNotNull(jsonResult.Data, "JsonResult.Data is null");
			Assert.IsNotNull(typeDataIsExpectedToBe, "objectOfExpectedType is null");
			Assert.IsInstanceOfType(jsonResult.Data, typeDataIsExpectedToBe, "JsonResult.Data is not of expected type {0}", typeDataIsExpectedToBe.Name);
			return jsonResult;
		}

		/// <summary>
		/// Verifies that the <see cref="JsonResult"/> is not <c>null</c>. The assertion fails if the <see cref="JsonResult"/> is <c>null</c>.
		/// </summary>
		/// <param name="jsonResult">The <see cref="JsonResult"/> instance that this method extends.</param>
		/// <returns>The <see cref="JsonResult"/> instance that was verified.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// 
		/// // act
		/// var jsonResult = someController.SomeMethod();
		/// 
		/// // assert
		/// jsonResult.IsNotNull();
		/// </code>
		/// </example>
		public static JsonResult IsNotNull(this JsonResult jsonResult)
		{
			Assert.IsNotNull(jsonResult, "JsonResult is null");
			return jsonResult;
		}
	}
}
