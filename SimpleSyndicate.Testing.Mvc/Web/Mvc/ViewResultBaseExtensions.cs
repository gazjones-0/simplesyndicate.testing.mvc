﻿using System;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SimpleSyndicate.Testing.Mvc.Web.Mvc
{
	/// <summary>
	/// Extends <see cref="ViewResultBase"/> providing various functionality and assertions in a fluent syntax.
	/// </summary>
	/// <remarks>
	/// The <see cref="ViewResultBaseExtensions"/> class contains methods that extend the <see cref="ViewResultBase"/> class. Where applicable,
	/// each method returns the <see cref="ViewResultBase"/> instance the method extends so that they can be chained together.
	/// </remarks>
	/// <example>
	/// <code language="cs">
	/// using SimpleSyndicate.Testing.Mvc.Web.Mvc;
	/// 
	/// namespace MyMvcApp.Tests.Controllers
	/// {
	/// 	[TestClass]
	/// 	public class SomeControllerTests
	/// 	{
	/// 		[TestMethod]
	/// 		public void SomeMethodIsDefaultView()
	/// 		{
	/// 			// arrange
	/// 			var controller = new SomeController();
	/// 
	/// 			// act
	/// 			ViewResult viewResult = controller.SomeMethod();
	/// 
	/// 			// assert
	/// 			viewResult
	/// 				.IsNotNull()
	/// 				.ModelIs&lt;SomeType&gt;();
	/// 		}
	/// 	}
	/// </code>
	/// </example>
	public static class ViewResultBaseExtensions
	{
		/// <summary>
		/// Verifies that the view uses the default view (i.e. the <see cref="ViewResultBase.ViewName"/> is <c>null</c> or empty).
		/// The assertion fails if the view isn't using the default view.
		/// </summary>
		/// <param name="viewResult">The <see cref="ViewResultBase"/> instance that this method extends.</param>
		/// <returns>The <see cref="ViewResultBase"/> instance that was verified.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// 
		/// // act
		/// var viewResult = someController.SomeMethod();
		/// 
		/// // assert
		/// viewResult.IsDefaultView();
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Justification = "Argument is validated by an assertion.")]
		public static ViewResultBase IsDefaultView(this ViewResultBase viewResult)
		{
			Assert.IsNotNull(viewResult, "Failed to check if default view; ViewResult is null");
			if (!String.IsNullOrEmpty(viewResult.ViewName))
			{
				Assert.Fail("View is not default view (view is named \"{0}\")", viewResult.ViewName);
			}
			return viewResult;
		}

		/// <summary>
		/// Verifies that the view is not <c>null</c>. The assertion fails if the view is <c>null</c>.
		/// </summary>
		/// <param name="viewResult">The <see cref="ViewResultBase"/> instance that this method extends.</param>
		/// <returns>The <see cref="ViewResultBase"/> instance that was verified.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// 
		/// // act
		/// var viewResult = someController.SomeMethod();
		/// 
		/// // assert
		/// viewResult.IsNotNull();
		/// </code>
		/// </example>
		public static ViewResultBase IsNotNull(this ViewResultBase viewResult)
		{
			Assert.IsNotNull(viewResult, "ViewResult is null");
			return viewResult;
		}

		/// <summary>
		/// Verifies that the model the view holds if of the specified type and then returns the model the view holds as the specified type.
		/// </summary>
		/// <typeparam name="TModel">The type the view's model is expected to be.</typeparam>
		/// <param name="viewResult">The <see cref="ViewResultBase"/> instance that this method extends.</param>
		/// <returns>The <see cref="ViewResultBase"/> instance that was verified.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// 
		/// // act
		/// var viewResult = someController.SomeMethod();
		/// 
		/// // assert
		/// Assert.AreEqual("expectedValue", viewResult.ModelAs&lt;SomeType&gt;().SomePropertyOnTheModel);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Justification = "Argument is validated by an assertion.")]
		public static TModel ModelAs<TModel>(this ViewResultBase viewResult)
		{
			Assert.IsNotNull(viewResult, "Failed to cast Model; ViewResult is null");
			Assert.IsNotNull(viewResult.ViewData, "Failed to cast Model; ViewResult.ViewData is null");
			Assert.IsInstanceOfType(viewResult.ViewData.Model, typeof(TModel), "Failed to cast Model; Model is not of expected type {0}", typeof(TModel).Name);
			return (TModel)viewResult.Model;
		}

		/// <overloads>
		/// <summary>
		/// Verifies that the model the view holds is the specified type.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Verifies that the model the view holds is the specified type. The assertion fails if the model the view holds isn't of the expected type.
		/// </summary>
		/// <typeparam name="TModel">The type the view's model is expected to be</typeparam>
		/// <param name="viewResult">The <see cref="ViewResultBase"/> instance that this method extends.</param>
		/// <returns>The <see cref="ViewResultBase"/> instance that was verified.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// 
		/// // act
		/// var viewResult = someController.SomeMethod();
		/// 
		/// // assert
		/// viewResult.ModelIs&lt;SomeModel&gt;();
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Justification = "Clients shouldn't need to instantiate a model just to check if the view's model is of the same type.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Justification = "Argument is validated by an assertion.")]
		public static ViewResultBase ModelIs<TModel>(this ViewResultBase viewResult)
		{
			Assert.IsNotNull(viewResult, "Failed to check Model; ViewResult is null");
			Assert.IsNotNull(viewResult.ViewData, "Failed to check Model; ViewResult.ViewData is null");
			Assert.IsInstanceOfType(viewResult.ViewData.Model, typeof(TModel), "Failed to check Model; Model is not of expected type {0}", typeof(TModel).Name);
			return viewResult;
		}

		/// <summary>
		/// Verifies that the model the view holds is the specified type. The assertion fails if the model the view holds isn't of the expected type.
		/// </summary>
		/// <typeparam name="TModel">The type the view's model is expected to be</typeparam>
		/// <param name="viewResult">The <see cref="ViewResultBase"/> instance that this method extends.</param>
		/// <param name="objectOfTypeModelIsExpectedToBe">An object that is of the type the model the view holds is expected to be.</param>
		/// <returns>The <see cref="ViewResultBase"/> instance that was verified.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// var someModel = new SomeModel();
		/// 
		/// // act
		/// var viewResult = someController.SomeMethod();
		/// 
		/// // assert
		/// viewResult.ModelIs(someModel);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Justification = "Arguments are validated by assertions.")]
		public static ViewResultBase ModelIs<TModel>(this ViewResultBase viewResult, TModel objectOfTypeModelIsExpectedToBe)
		{
			Assert.IsNotNull(viewResult, "Failed to check Model; ViewResult is null");
			Assert.IsNotNull(viewResult.ViewData, "Failed to check Model; ViewResult.ViewData is null");
			Assert.IsNotNull(objectOfTypeModelIsExpectedToBe, "Failed to check Model; objectOfTypeModelIsExpectedToBe is null");
			Assert.IsInstanceOfType(viewResult.ViewData.Model, objectOfTypeModelIsExpectedToBe.GetType(), "Failed to check Model; Model is not of expected type {0}", objectOfTypeModelIsExpectedToBe.GetType().Name);
			return viewResult;
		}

		/// <summary>
		/// Verifies that the model the view holds is the specified type. The assertion fails if the model the view holds isn't of the expected type.
		/// </summary>
		/// <param name="viewResult">The <see cref="ViewResultBase"/> instance that this method extends.</param>
		/// <param name="typeModelIsExpectedToBe">Type the model the view holds is expected to be.</param>
		/// <returns>The <see cref="ViewResultBase"/> instance that was verified.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// var someModel = new SomeModel();
		/// 
		/// // act
		/// var viewResult = someController.SomeMethod();
		/// 
		/// // assert
		/// viewResult.ModelIs(typeof(someModel));
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters", Justification = "More derived type required to pass to assertion.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Justification = "Arguments are validated by assertions.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "1", Justification = "Arguments are validated by assertions.")]
		public static ViewResultBase ModelIs(this ViewResultBase viewResult, Type typeModelIsExpectedToBe)
		{
			Assert.IsNotNull(viewResult, "Failed to check Model; ViewResult is null");
			Assert.IsNotNull(viewResult.ViewData, "Failed to check Model; ViewResult.ViewData is null");
			Assert.IsNotNull(typeModelIsExpectedToBe, "Failed to check Model; objectOfTypeModelIsExpectedToBe is null");
			Assert.IsInstanceOfType(viewResult.ViewData.Model, typeModelIsExpectedToBe, "Failed to check Model; Model is not of expected type {0}", typeModelIsExpectedToBe.Name);
			return viewResult;
		}

		/// <summary>
		/// Verifies that the model the view holds is in an invalid state. The assertion fails if the model the view holds is in a valid state.
		/// </summary>
		/// <param name="viewResult">The <see cref="ViewResultBase"/> instance that this method extends.</param>
		/// <returns>The <see cref="ViewResultBase"/> instance that was verified.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// 
		/// // act
		/// var viewResult = someController.SomeMethod();
		/// 
		/// // assert
		/// viewResult.ModelIsInvalid();
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Justification = "Argument is validated by an assertion.")]
		public static ViewResultBase ModelIsInvalid(this ViewResultBase viewResult)
		{
			Assert.IsNotNull(viewResult, "Failed to check if Model is invalid; ViewResult is null");
			Assert.IsNotNull(viewResult.ViewData, "Failed to check if Model is invalid; ViewResult.ViewData is null");
			Assert.IsNotNull(viewResult.ViewData.ModelState, "Failed to check if Model is invalid; ViewResult.ViewData.ModelState is null");
			Assert.AreEqual(false, viewResult.ViewData.ModelState.IsValid, "ModelState is valid");
			return viewResult;
		}

		/// <summary>
		/// Verifies that the model the view holds is in a valid state. The assertion fails if the model the view holds is in an invalid state.
		/// </summary>
		/// <param name="viewResult">The <see cref="ViewResultBase"/> instance that this method extends.</param>
		/// <returns>The <see cref="ViewResultBase"/> instance that was verified.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// 
		/// // act
		/// var viewResult = someController.SomeMethod();
		/// 
		/// // assert
		/// viewResult.ModelIsValid();
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Justification = "Argument is validated by an assertion.")]
		public static ViewResultBase ModelIsValid(this ViewResultBase viewResult)
		{
			Assert.IsNotNull(viewResult, "Failed to check if Model is valid; ViewResult is null");
			Assert.IsNotNull(viewResult.ViewData, "Failed to check if Model is valid; ViewResult.ViewData is null");
			Assert.IsNotNull(viewResult.ViewData.ModelState, "Failed to check if Model is valid; ViewResult.ViewData.ModelState is null");
			Assert.AreEqual(true, viewResult.ViewData.ModelState.IsValid, "ModelState is invalid");
			return viewResult;
		}

		/// <overloads>
		/// <summary>
		/// Verifies that the view's view bag contains the specified item.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Verifies that the view's view bag contains the specified key. The assertion fails if the view's view bag doesn't contain the specified key.
		/// </summary>
		/// <param name="viewResult">The <see cref="ViewResultBase"/> instance that this method extends.</param>
		/// <param name="expectedKey">The name of the key the view's view bag is expected to contain.</param>
		/// <returns>The <see cref="ViewResultBase"/> instance that was verified.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// 
		/// // act
		/// var viewResult = someController.SomeMethod();
		/// 
		/// // assert
		/// viewResult.ViewBagContains("expectedKey");
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Justification = "Argument is validated by an assertion.")]
		public static ViewResultBase ViewBagContains(this ViewResultBase viewResult, string expectedKey)
		{
			Assert.IsNotNull(viewResult, "Failed to check ViewBag; ViewResult is null (was looking for \"{0}\" key)", expectedKey);
			Assert.IsNotNull(viewResult.ViewData, "Failed to check ViewBag; ViewResult.ViewData is null (was looking for \"{0}\" key)", expectedKey);
			Assert.IsNotNull(viewResult.ViewData[expectedKey], "ViewBag does not contain \"{0}\" key", expectedKey);
			return viewResult;
		}

		/// <summary>
		/// Verifies that the view's view bag contains the specified key, and that the key holds the specified value.
		/// The assertion fails if the view's view bag doesn't contain the specified key holding the specified value.
		/// </summary>
		/// <param name="viewResult">The <see cref="ViewResultBase"/> instance that this method extends.</param>
		/// <param name="expectedKey">The name of the key the view's view bag is expected to contain.</param>
		/// <param name="expectedValue">The value the key is expected to hold.</param>
		/// <returns>The <see cref="ViewResultBase"/> instance that was verified.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// 
		/// // act
		/// var viewResult = someController.SomeMethod();
		/// 
		/// // assert
		/// viewResult.ViewBagContains("expectedKey", "expectedValue");
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Justification = "Argument is validated by an assertion.")]
		public static ViewResultBase ViewBagContains(this ViewResultBase viewResult, string expectedKey, string expectedValue)
		{
			Assert.IsNotNull(viewResult, "Failed to check ViewBag; ViewResult is null (was looking for \"{0}\" key, expecting it to be \"{1}\")", expectedKey, expectedValue);
			Assert.IsNotNull(viewResult.ViewData, "Failed to check ViewBag; ViewResult.ViewData is null (was looking for \"{0}\" key, expecting it to be \"{1}\")", expectedKey, expectedValue);
			Assert.IsNotNull(viewResult.ViewData[expectedKey], "ViewBag does not contain \"{0}\" key (expected it to be \"{1}\")", expectedKey, expectedValue);
			Assert.AreEqual(expectedValue, viewResult.ViewData[expectedKey].ToString(), "ViewBag value for \"{0}\" key was not expected value", expectedKey);
			return viewResult;
		}

		/// <summary>
		/// Verifies that the view's name is as specified. The assertion fails if the view's name isn't the specified name.
		/// </summary>
		/// <param name="viewResult">The <see cref="ViewResultBase"/> instance that this method extends.</param>
		/// <param name="expectedViewName">The name of the view the view result is expected to hold.</param>
		/// <returns>The <see cref="ViewResultBase"/> instance that was verified.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// 
		/// // act
		/// var viewResult = someController.SomeMethod();
		/// 
		/// // assert
		/// viewResult.ViewNameIs("expectedViewName");
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Justification = "Argument is validated by an assertion.")]
		public static ViewResultBase ViewNameIs(this ViewResultBase viewResult, string expectedViewName)
		{
			Assert.IsNotNull(viewResult, "Failed to check ViewName; ViewResult is null");
			Assert.AreEqual(expectedViewName, viewResult.ViewName);
			return viewResult;
		}
	}
}
