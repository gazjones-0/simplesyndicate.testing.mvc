﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSyndicate.Mvc.Controllers;

namespace SimpleSyndicate.Testing.Mvc.Web.Mvc
{
	/// <summary>
	/// Extends <see cref="GenericController"/> providing various assertions in a fluent syntax.
	/// </summary>
	/// <remarks>
	/// The <see cref="GenericControllerExtensions"/> class contains methods that extend the <see cref="GenericController"/> class. Each method returns the
	/// <see cref="GenericController"/> instance the method extends so that they can be chained together.
	/// </remarks>
	/// <example>
	/// <code language="cs">
	/// using SimpleSyndicate.Testing.Mvc.Web.Mvc;
	/// 
	/// namespace MyMvcApp.Tests.Controllers
	/// {
	/// 	[TestClass]
	/// 	public class SomeControllerTests
	/// 	{
	/// 		[TestMethod]
	/// 		public void SomeMethodSetsConfirmationMessage()
	/// 		{
	/// 			// arrange
	/// 			var controller = new SomeController();
	/// 
	/// 			// act
	/// 			controller.SomeMethod();
	/// 
	/// 			// assert
	/// 			controller.HasAddedConfirmationMessage();
	/// 		}
	/// 	}
	/// </code>
	/// </example>
	public static partial class GenericControllerExtensions
	{
		/// <overloads>
		/// <summary>
		/// Verifies that the controller has set a confirmation message for <see cref="ConfirmationMessageActionPerformed.EntityAdded"/>.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Verifies that the <see cref="GenericController"/> has set a confirmation message for <see cref="ConfirmationMessageActionPerformed.EntityAdded"/>
		/// (via <see cref="GenericController.SetConfirmationMessage"/>). The assertion fails if the confirmation message hasn't been set.
		/// </summary>
		/// <param name="controller">The <see cref="GenericController"/> instance that this method extends.</param>
		/// <returns>The <see cref="GenericController"/> instance that was verified.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// 
		/// // act
		/// someController.SomeMethod();
		/// 
		/// // assert
		/// someController.HasConfirmationMessageForEntityAdded();
		/// </code>
		/// </example>
		public static GenericController HasConfirmationMessageForEntityAdded(this GenericController controller)
		{
			return HasConfirmationMessage(controller, ConfirmationMessageActionPerformed.EntityAdded);
		}

		/// <overloads>
		/// <summary>
		/// Verifies that the controller has set a confirmation message for <see cref="ConfirmationMessageActionPerformed.EntityUpdated"/>.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Verifies that the <see cref="GenericController"/> has set a confirmation message for <see cref="ConfirmationMessageActionPerformed.EntityUpdated"/>
		/// (via <see cref="GenericController.SetConfirmationMessage"/>). The assertion fails if the confirmation message hasn't been set.
		/// </summary>
		/// <param name="controller">The <see cref="GenericController"/> instance that this method extends.</param>
		/// <returns>The <see cref="GenericController"/> instance that was verified.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// 
		/// // act
		/// someController.SomeMethod();
		/// 
		/// // assert
		/// someController.HasConfirmationMessageForEntityUpdated();
		/// </code>
		/// </example>
		public static GenericController HasConfirmationMessageForEntityUpdated(this GenericController controller)
		{
			return HasConfirmationMessage(controller, ConfirmationMessageActionPerformed.EntityUpdated);
		}

		/// <overloads>
		/// <summary>
		/// Verifies that the controller has set a confirmation message for <see cref="ConfirmationMessageActionPerformed.EntityDeleted"/>.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Verifies that the <see cref="GenericController"/> has set a confirmation message for <see cref="ConfirmationMessageActionPerformed.EntityDeleted"/>
		/// (via <see cref="GenericController.SetConfirmationMessage"/>). The assertion fails if the confirmation message hasn't been set.
		/// </summary>
		/// <param name="controller">The <see cref="GenericController"/> instance that this method extends.</param>
		/// <returns>The <see cref="GenericController"/> instance that was verified.</returns>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// 
		/// // act
		/// someController.SomeMethod();
		/// 
		/// // assert
		/// someController.HasConfirmationMessageForEntityDeleted();
		/// </code>
		/// </example>
		public static GenericController HasConfirmationMessageForEntityDeleted(this GenericController controller)
		{
			return HasConfirmationMessage(controller, ConfirmationMessageActionPerformed.EntityDeleted);
		}

		/// <summary>
		/// Verifies that the <see cref="GenericController"/> has set a confirmation message of the specified type (via
		/// <see cref="GenericController.SetConfirmationMessage"/>). The assertion fails if the confirmation message hasn't been set.
		/// </summary>
		/// <param name="controller">The <see cref="GenericController"/> instance that this method extends.</param>
		/// <param name="actionPerformed">The <see cref="ConfirmationMessageActionPerformed"/> to check for.</param>
		/// <returns>The <see cref="GenericController"/> instance that was verified.</returns>
		private static GenericController HasConfirmationMessage(GenericController controller, ConfirmationMessageActionPerformed actionPerformed)
		{
			Assert.IsNotNull(controller, "Failed check for confirmation message; Controller is null");
			Assert.IsNotNull(controller.TempData, "Failed check for confirmation message; TempData in Controller is null");
			ConfirmationMessageViewModel confirmationMessage = null;
			try
			{
				confirmationMessage = controller.ConfirmationMessage();
			}
			catch (InvalidOperationException)
			{
				Assert.Fail("Failed check for confirmation message; caught NullReferenceException when calling GenericController.ConfirmationMessage(); have you called SetConfirmationMessage?");
			}
			Assert.IsInstanceOfType(confirmationMessage, typeof(ConfirmationMessageViewModel), "Failed check for confirmation message, confirmation message was returned but is not of type ConfirmationMessageViewModel!");
			Assert.AreEqual(actionPerformed, ((ConfirmationMessageViewModel)confirmationMessage).ActionPerformed, "Failed check for confirmation message, ConfirmationMessageViewModel.Type is not expected value; did you set the right confirmation message?");
			return controller;
		}
	}
}
