﻿using System;
using System.Web;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SimpleSyndicate.Testing.Mvc.Web.Mvc
{
	/// <summary>
	/// Extends <see cref="Controller"/> providing various unit testing functionality in a fluent syntax.
	/// </summary>
	/// <remarks>
	/// The <see cref="ControllerExtensions"/> class contains methods that extend the <see cref="Controller"/> class. Each method returns the
	/// <see cref="Controller"/> instance the method extends so that they can be chained together.
	/// </remarks>
	/// <example>
	/// <code language="cs">
	/// using SimpleSyndicate.Testing.Mvc.Web.Mvc;
	/// 
	/// namespace MyMvcApp.Tests.Controllers
	/// {
	/// 	[TestClass]
	/// 	public class SomeControllerTests
	/// 	{
	/// 		[TestMethod]
	/// 		public void SomeMethodSetsConfirmationMessage()
	/// 		{
	/// 			// arrange
	/// 			var controller = new SomeController();
	/// 			controller.WithInvalidModel();
	/// 			controller.WithMockedContext();
	/// 
	/// 			// act
	/// 			controller.SomeMethod();
	/// 
	/// 			// assert
	/// 			Assert.IsFalse(controller.ModelState.IsValid);
	/// 		}
	/// 	}
	/// </code>
	/// </example>
	public static class ControllerExtensions
	{
		/// <summary>
		/// Configures the <see cref="Controller"/> so that the model is invalid (i.e. the model state is not valid).
		/// </summary>
		/// <param name="controller">The <see cref="Controller"/> instance that this method extends.</param>
		/// <returns>The <see cref="Controller"/> instance configured with an invalid model.</returns>
		/// <exception cref="ArgumentNullException">Thrown when <paramref name="controller"/> is <c>null</c>.</exception>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// someController.WithInvalidModel();
		/// 
		/// // act
		/// var actionResult = someController.SomeMethod();
		/// 
		/// // assert
		/// actionResult.IsViewResult();
		/// </code>
		/// </example>
		public static Controller WithInvalidModel(this Controller controller)
		{
			if (controller == null)
			{
				throw new ArgumentNullException("controller");
			}
			Assert.IsNotNull(controller, "Controller is null");
			Assert.IsNotNull(controller.ModelState, "Controller.ModelState is null");
			controller.ModelState.AddModelError("key", "value");
			return controller;
		}

		/// <overloads>
		/// <summary>
		/// Configures the <see cref="Controller"/> to have a mocked <see cref="ControllerContext"/>.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Configures the <see cref="Controller"/> to have a mocked <see cref="ControllerContext"/>.
		/// </summary>
		/// <param name="controller">The <see cref="Controller"/> instance that this method extends.</param>
		/// <returns>The <see cref="Controller"/> instance with a mocked <see cref="ControllerContext"/>.</returns>
		/// <exception cref="ArgumentNullException">Thrown when <paramref name="controller"/> is <c>null</c>.</exception>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// someController.WithMockedContext();
		/// 
		/// // act
		/// var actionResult = someController.SomeMethod();
		/// 
		/// // assert
		/// actionResult.IsRedirectToRouteResult();
		/// </code>
		/// </example>
		public static Controller WithMockedContext(this Controller controller)
		{
			return WithMockedContext(controller, null, null);
		}

		/// <summary>
		/// Configures the <see cref="Controller"/> to have a mocked <see cref="ControllerContext"/>, sets the current <see cref="HttpContext"/> to the one
		/// specified and sets the <see cref="Controller"/>'s HTTP context to be the one specified.
		/// </summary>
		/// <param name="controller">The <see cref="Controller"/> instance that this method extends.</param>
		/// <param name="httpContext">A <see cref="HttpContext"/> instance to set as the current <see cref="HttpContext"/>; if this is <c>null</c>, the current <see cref="HttpContext"/> is not changed.</param>
		/// <param name="httpContextBase">A <see cref="HttpContextBase"/> instance to set as the <see cref="Controller"/>s HTTP context; if this is <c>null</c>, the <see cref="Controller"/>'s HTTP context is not changed.</param>
		/// <returns>The <see cref="Controller"/> instance with a mocked <see cref="ControllerContext"/>.</returns>
		/// <exception cref="ArgumentNullException">Thrown when <paramref name="controller"/> is <c>null</c>.</exception>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// var testData = TestData.HttpContextContainerFactory.Create();
		/// someController.WithMockedContext(testData.HttpContext, testData.HttpContextWrapper);
		/// 
		/// // act
		/// var actionResult = someController.SomeMethod();
		/// 
		/// // assert
		/// actionResult.IsRedirectToRouteResult();
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters", Justification = "The base class is abstract and rarely used, and for a fluent syntax we want to return the derived type as it will be more useful.")]
		public static Controller WithMockedContext(this Controller controller, HttpContext httpContext, HttpContextBase httpContextBase)
		{
			if (controller == null)
			{
				throw new ArgumentNullException("controller");
			}
			controller.ControllerContext = new ControllerContext();
			if (httpContext != null)
			{
				HttpContext.Current = httpContext;
			}
			if (httpContextBase != null)
			{
				controller.ControllerContext.HttpContext = httpContextBase;
			}
			return controller;
		}

		/// <overloads>
		/// <summary>
		/// Configures the <see cref="Controller"/> to have a mocked <see cref="ControllerBase.ValueProvider"/>.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Configures the <see cref="Controller"/> to have a mocked <see cref="ControllerBase.ValueProvider"/>; the value provider has no values.
		/// </summary>
		/// <param name="controller">The <see cref="Controller"/> instance that this method extends.</param>
		/// <returns>The <see cref="Controller"/> instance with a mocked <see cref="ControllerBase.ValueProvider"/>.</returns>
		/// <exception cref="ArgumentNullException">Thrown when <paramref name="controller"/> is <c>null</c>.</exception>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// someController.WithMockedValueProvider();
		/// 
		/// // act
		/// var actionResult = someController.SomeMethod();
		/// 
		/// // assert
		/// actionResult.IsRedirectToRouteResult();
		/// </code>
		/// </example>
		public static Controller WithMockedValueProvider(this Controller controller)
		{
			return WithMockedValueProvider(controller, new FormCollection());
		}

		/// <summary>
		/// Configures the <see cref="Controller"/> to have a mocked <see cref="ControllerBase.ValueProvider"/> populated with the specified form collection.
		/// </summary>
		/// <param name="controller">The <see cref="Controller"/> instance that this method extends.</param>
		/// <param name="formCollection">The <see cref="FormCollection"/> to populate the mocked <see cref="ControllerBase.ValueProvider"/> with.</param>
		/// <returns>The <see cref="Controller"/> instance with a mocked <see cref="ControllerBase.ValueProvider"/>.</returns>
		/// <exception cref="ArgumentNullException">Thrown when <paramref name="controller"/> is <c>null</c>.</exception>
		/// <exception cref="ArgumentNullException">Thrown when <paramref name="formCollection"/> is <c>null</c>.</exception>
		/// <example>
		/// <code language="cs">
		/// // arrange
		/// var someController = new SomeController();
		/// var formCollection = new FormCollection();
		/// formCollection.Add("key", "value");
		/// someController.WithMockedValueProvider(formCollection);
		/// 
		/// // act
		/// var actionResult = someController.SomeMethod();
		/// 
		/// // assert
		/// actionResult.IsRedirectToRouteResult();
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters", Justification = "The base class is abstract and rarely used, and for a fluent syntax we want to return the derived type as it will be more useful.")]
		public static Controller WithMockedValueProvider(this Controller controller, FormCollection formCollection)
		{
			if (controller == null)
			{
				throw new ArgumentNullException("controller");
			}
			if (formCollection == null)
			{
				throw new ArgumentNullException("formCollection");
			}
			controller.ValueProvider = formCollection.ToValueProvider();
			return controller;
		}
	}
}
