﻿using System.Web.Mvc;

namespace SimpleSyndicate.Testing.Mvc.Web.Mvc
{
	/// <summary>
	/// Concrete implementation of <see cref="IViewDataContainer"/>, suitable for use in unit testing.
	/// </summary>
	public class TestViewDataContainer : IViewDataContainer
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="TestViewDataContainer"/> class.
		/// </summary>
		public TestViewDataContainer()
		{
			this.ViewData = new ViewDataDictionary();
		}

		/// <summary>
		/// Gets or sets the view data dictionary.
		/// </summary>
		/// <value>The view data dictionary.</value>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "Interface requires collection be read/write.")]
		public ViewDataDictionary ViewData { get; set; }
	}
}