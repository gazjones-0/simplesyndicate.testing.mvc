﻿using System;
using System.Web.Mvc;

namespace SimpleSyndicate.Testing.Mvc.Web.Mvc
{
	/// <summary>
	/// Helper methods for unit testing <see cref="GlobalFilters"/>.
	/// </summary>
	public static class GlobalFiltersHelpers
	{
		/// <overloads>
		/// <summary>
		/// Returns whether a filter of the specified type is in the global filters list.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Returns whether a filter of type <typeparamref name="TType"/> is in the global filters list.
		/// </summary>
		/// <typeparam name="TType">Type of filter to check for.</typeparam>
		/// <returns><c>true</c> if there's a filter of type <typeparamref name="TType"/>; otherwise, <c>false</c>.</returns>
		/// <example>
		/// <code language="cs">
		/// var hasFilter = GlobalFiltersHelpers.HasFilter&lt;SomeType&gt;();
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Justification = "Clients shouldn't need to instantiate a filter just to check if a filter of that type is present.")]
		public static bool HasFilter<TType>()
		{
			return Contains(typeof(TType));
		}

		/// <summary>
		/// Returns whether a filter of type <typeparamref name="TType"/> is in the global filters list.
		/// </summary>
		/// <typeparam name="TType">Type of filter to check for.</typeparam>
		/// <param name="filter">An object that is the type of filter to check for.</param>
		/// <returns><c>true</c> if there's a filter of type <typeparamref name="TType"/>; otherwise, <c>false</c>.</returns>
		/// <exception cref="ArgumentNullException">Thrown when <paramref name="filter"/> is <c>null</c>.</exception>
		/// <example>
		/// <code language="cs">
		/// var someFilter = new HandleErrorAttribute();
		/// var hasFilter = GlobalFiltersHelpers.HasFilter(someFilter);
		/// </code>
		/// </example>
		public static bool HasFilter<TType>(TType filter)
		{
			if (filter == null)
			{
				throw new ArgumentNullException("filter");
			}
			return Contains(filter.GetType());
		}

		/// <summary>
		/// Returns whether a filter of type <paramref name="filterType"/> is in the global filters list.
		/// </summary>
		/// <param name="filterType">Type of filter to check for.</param>
		/// <returns><c>true</c> if there's a filter of type <paramref name="filterType"/>; otherwise, <c>false</c>.</returns>
		/// <exception cref="ArgumentNullException">Thrown when <paramref name="filterType"/> is <c>null</c>.</exception>
		/// <example>
		/// <code language="cs">
		/// var hasFilter = GlobalFiltersHelpers.HasFilter(typeof(HandleErrorAttribute));
		/// </code>
		/// </example>
		public static bool HasFilter(Type filterType)
		{
			if (filterType == null)
			{
				throw new ArgumentNullException("filterType");
			}
			return Contains(filterType);
		}

		/// <summary>
		/// Returns whether a filter of type <paramref name="type"/> is in the global filters list.
		/// </summary>
		/// <param name="type">Type of filter to check for.</param>
		/// <returns><c>true</c> if there's a filter of type <paramref name="type"/>; otherwise, <c>false</c>.</returns>
		private static bool Contains(Type type)
		{
			foreach (var filter in GlobalFilters.Filters)
			{
				if (filter.Instance.GetType() == type)
				{
					return true;
				}
			}
			return false;
		}
	}
}
