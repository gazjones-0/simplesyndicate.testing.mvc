﻿using System.Web.Mvc;

namespace SimpleSyndicate.Testing.Mvc.Web.Mvc
{
	/// <summary>
	/// Factory class for creating <see cref="HtmlHelper"/> objects suitable for use in unit testing.
	/// </summary>
	public static class HtmlHelperFactory
	{
		/// <summary>
		/// Returns a new instance of <see cref="HtmlHelper{TEntity}"/>
		/// </summary>
		/// <typeparam name="TEntity">The type.</typeparam>
		/// <returns>A new <see cref="HtmlHelper{TEntity}"/> instance.</returns>
		/// <example>
		/// <code language="cs">
		/// var htmlHelper = HtmlHelperFactory.Create&lt;SomeModel&gt;();
		/// </code>
		/// </example>
		public static HtmlHelper<TEntity> Create<TEntity>()
		{
			return new HtmlHelper<TEntity>(new ViewContext(), new TestViewDataContainer());
		}
	}
}
