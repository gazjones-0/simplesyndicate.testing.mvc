﻿namespace SimpleSyndicate.Testing.Mvc.AspNet
{
	/// <summary>
	/// The <see cref="AspNet"/> namespace contains classes for use in testing <b>Microsoft.AspNet</b>.
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGeneratedAttribute]
	internal class NamespaceDoc
	{
	}
}
