﻿namespace SimpleSyndicate.Testing.Mvc.AspNet.Identity
{
	/// <summary>
	/// The <see cref="Identity"/> namespace contains classes for use in testing <see cref="Microsoft.AspNet.Identity"/>.
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGeneratedAttribute]
	internal class NamespaceDoc
	{
	}
}
