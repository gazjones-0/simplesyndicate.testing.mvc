﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SimpleSyndicate.Repositories;
using SimpleSyndicate.Testing.Repositories;

namespace SimpleSyndicate.Testing.Mvc.AspNet.Identity
{
	/// <summary>
	/// Test user store.
	/// </summary>
	/// <typeparam name="TUser">The user type.</typeparam>
	public class TestUserStore<TUser> : IUserStore<TUser>
		where TUser : IdentityUser
	{
		/// <summary>
		/// Whether Dispose has been called or not.
		/// </summary>
		private bool disposed = false;
		
		/// <summary>
		/// Repository the users are stored in.
		/// </summary>
		private IRepository<TUser> userRepository;

		/// <summary>
		/// Initializes a new instance of the <see cref="TestUserStore{TUser}"/> class; the store is empty.
		/// </summary>
		public TestUserStore()
		{
			this.userRepository = new TestRepository<TUser>();
		}

		/// <summary>
		/// Asynchronously inserts a new user.
		/// </summary>
		/// <param name="user">The user.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		/// <exception cref="ArgumentNullException">Thrown when <paramref name="user"/> is <c>null</c>.</exception>
		public Task CreateAsync(TUser user)
		{
			if (user == null)
			{
				throw new ArgumentNullException("user");
			}
			return Task.Run(() => this.Create(user));
		}

		/// <summary>
		/// Asynchronously deletes a user.
		/// </summary>
		/// <param name="user">The user.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		/// <exception cref="ArgumentNullException">Thrown when <paramref name="user"/> is <c>null</c>.</exception>
		public Task DeleteAsync(TUser user)
		{
			if (user == null)
			{
				throw new ArgumentNullException("user");
			}
			return Task.Run(() => this.Delete(user));
		}

		/// <summary>
		/// Asynchronously finds a user using the specified identifier.
		/// </summary>
		/// <param name="userId">The user identifier.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		public Task<TUser> FindByIdAsync(string userId)
		{
			return Task.Run(() => this.FindById(userId));
		}

		/// <summary>
		/// Synchronously finds a user by name.
		/// </summary>
		/// <param name="userName">The user name.</param>
		/// <returns>The user, or <c>null</c> if the user cannot be found.</returns>
		public TUser FindByName(string userName)
		{
			var users = this.userRepository.All().Where(x => x.UserName.ToUpper() == userName.ToUpper());
			if (users.Count() > 0)
			{
				return users.First();
			}
			return null;
		}

		/// <summary>
		/// Asynchronously finds a user by name.
		/// </summary>
		/// <param name="userName">The user name.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		public Task<TUser> FindByNameAsync(string userName)
		{
			return Task.Run(() => this.FindByName(userName));
		}

		/// <summary>
		/// Asynchronously updates a user.
		/// </summary>
		/// <param name="user">The user.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		/// <exception cref="ArgumentNullException">Thrown when <paramref name="user"/> is <c>null</c>.</exception>
		public Task UpdateAsync(TUser user)
		{
			if (user == null)
			{
				throw new ArgumentNullException("user");
			}
			return Task.Run(() => this.Update(user));
		}

		/// <overloads>
		/// <summary>
		/// Releases all resources that are used by the current instance of the <see cref="TestUserStore{TUser}"/> class.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Releases all resources that are used by the current instance of the <see cref="TestUserStore{TUser}"/> class.
		/// </summary>
		/// <remarks>
		/// The Dispose method leaves the <see cref="TestUserStore{TUser}"/> in an unusable state.
		/// </remarks>
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>
		/// Releases unmanaged resources and optionally releases managed resources.
		/// </summary>
		/// <remarks>
		/// The Dispose method leaves the <see cref="TestRoleStore"/> in an unusable state.
		/// </remarks>
		/// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
		protected virtual void Dispose(bool disposing)
		{
			if (this.disposed)
			{
				return;
			}
			if (disposing)
			{
				if (this.userRepository != null)
				{
					this.userRepository.Dispose();
					this.userRepository = null;
				}
			}
			this.disposed = true;
		}

		/// <summary>
		/// Synchronously inserts a new user.
		/// </summary>
		/// <param name="user">The user.</param>
		private void Create(TUser user)
		{
			this.userRepository.Add(user);
			this.userRepository.SaveChanges();
		}

		/// <summary>
		/// Synchronously deletes a user.
		/// </summary>
		/// <param name="user">The user.</param>
		private void Delete(TUser user)
		{
			this.userRepository.Delete(user);
			this.userRepository.SaveChanges();
		}

		/// <summary>
		/// Synchronously finds a user using the specified identifier.
		/// </summary>
		/// <param name="userId">The user identifier.</param>
		/// <returns>The user, or <c>null</c> if the user cannot be found.</returns>
		private TUser FindById(string userId)
		{
			var users = this.userRepository.All().Where(x => x.Id == userId);
			if (users.Count() > 0)
			{
				return users.First();
			}
			return null;
		}

		/// <summary>
		/// Synchronously updates a user.
		/// </summary>
		/// <param name="user">The user.</param>
		private void Update(TUser user)
		{
			this.userRepository.Update(user);
			this.userRepository.SaveChanges();
		}
	}
}
