﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SimpleSyndicate.Repositories;
using SimpleSyndicate.Testing.Repositories;

namespace SimpleSyndicate.Testing.Mvc.AspNet.Identity
{
	/// <summary>
	/// Test role store.
	/// </summary>
	public class TestRoleStore : IRoleStore<IdentityRole>
	{
		/// <summary>
		/// Whether Dispose has been called or not.
		/// </summary>
		private bool disposed = false;

		/// <summary>
		/// Repository used to store the roles.
		/// </summary>
		private IRepository<IdentityRole> roleRepository;

		/// <summary>
		/// Initializes a new instance of the <see cref="TestRoleStore"/> class; the store is empty.
		/// </summary>
		public TestRoleStore()
		{
			this.roleRepository = new TestRepository<IdentityRole>();
		}

		/// <summary>
		/// Asynchronously creates a new role.
		/// </summary>
		/// <param name="role">The role to create.</param>
		/// <returns>The task representing the asynchronous operation.</returns>
		/// <exception cref="ArgumentNullException">Thrown when <paramref name="role"/> is <c>null</c>.</exception>
		public Task CreateAsync(IdentityRole role)
		{
			if (role == null)
			{
				throw new ArgumentNullException("role");
			}
			return Task.Run(() => this.Create(role));
		}

		/// <summary>
		/// Asynchronously deletes a role.
		/// </summary>
		/// <param name="role">The role to delete.</param>
		/// <returns>The task representing the asynchronous operation.</returns>
		/// <exception cref="ArgumentNullException">Thrown when <paramref name="role"/> is <c>null</c>.</exception>
		public Task DeleteAsync(IdentityRole role)
		{
			if (role == null)
			{
				throw new ArgumentNullException("role");
			}
			return Task.Run(() => this.Delete(role));
		}

		/// <summary>
		/// Asynchronously finds a role by ID.
		/// </summary>
		/// <param name="roleId">The role ID.</param>
		/// <returns>The task representing the asynchronous operation.</returns>
		public Task<IdentityRole> FindByIdAsync(string roleId)
		{
			return Task.Run(() => this.FindById(roleId));
		}

		/// <summary>
		/// Synchronously finds a role by name.
		/// </summary>
		/// <param name="roleName">The name of the role.</param>
		/// <returns>The role, or <c>null</c> if the role cannot be found.</returns>
		public IdentityRole FindByName(string roleName)
		{
			var roles = this.roleRepository.All().Where(x => x.Name.ToUpper() == roleName.ToUpper());
			if (roles.Count() > 0)
			{
				return roles.First();
			}
			return null;
		}

		/// <summary>
		/// Asynchronously finds a role by name.
		/// </summary>
		/// <param name="roleName">The name of the role.</param>
		/// <returns>The task representing the asynchronous operation.</returns>
		public Task<IdentityRole> FindByNameAsync(string roleName)
		{
			return Task.Run(() => this.FindByName(roleName));
		}

		/// <summary>
		/// Asynchronously updates a role.
		/// </summary>
		/// <param name="role">The role to update.</param>
		/// <returns>The task representing the asynchronous operation.</returns>
		/// <exception cref="ArgumentNullException">Thrown when <paramref name="role"/> is <c>null</c>.</exception>
		public Task UpdateAsync(IdentityRole role)
		{
			if (role == null)
			{
				throw new ArgumentNullException("role");
			}
			return Task.Run(() => this.Update(role));
		}

		/// <overloads>
		/// <summary>
		/// Releases all resources that are used by the current instance of the <see cref="TestRoleStore"/> class.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Releases all resources that are used by the current instance of the <see cref="TestRoleStore"/> class.
		/// </summary>
		/// <remarks>
		/// The Dispose method leaves the <see cref="TestRoleStore"/> in an unusable state.
		/// </remarks>
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>
		/// Releases unmanaged resources and optionally releases managed resources.
		/// </summary>
		/// <remarks>
		/// The Dispose method leaves the <see cref="TestRoleStore"/> in an unusable state.
		/// </remarks>
		/// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
		protected virtual void Dispose(bool disposing)
		{
			if (this.disposed)
			{
				return;
			}
			if (disposing)
			{
				if (this.roleRepository != null)
				{
					this.roleRepository.Dispose();
					this.roleRepository = null;
				}
			}
			this.disposed = true;
		}

		/// <summary>
		/// Synchronously creates a new role.
		/// </summary>
		/// <param name="role">The role to create.</param>
		private void Create(IdentityRole role)
		{
			this.roleRepository.Add(role);
			this.roleRepository.SaveChanges();
		}

		/// <summary>
		/// Synchronously deletes a role.
		/// </summary>
		/// <param name="role">The role to delete.</param>
		private void Delete(IdentityRole role)
		{
			this.roleRepository.Delete(role);
			this.roleRepository.SaveChanges();
		}

		/// <summary>
		/// Synchronously finds a role by ID.
		/// </summary>
		/// <param name="roleId">The role ID.</param>
		/// <returns>The role, or <c>null</c> if the role cannot be found.</returns>
		private IdentityRole FindById(string roleId)
		{
			var roles = this.roleRepository.All().Where(x => x.Id == roleId);
			if (roles.Count() > 0)
			{
				return roles.First();
			}
			return null;
		}

		/// <summary>
		/// Synchronously updates a role.
		/// </summary>
		/// <param name="role">The role to update.</param>
		private void Update(IdentityRole role)
		{
			this.roleRepository.Update(role);
			this.roleRepository.SaveChanges();
		}
	}
}
