﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SimpleSyndicate.Testing.Mvc.TestTools.UnitTesting
{
	/// <summary>
	/// Verifies conditions in unit tests using true/false propositions; this class provides all the functionality of <see cref="Assert"/>
	/// but with extra functionality specifically designed for MVC applications.
	/// </summary>
	public static partial class MvcAssert
	{
		/// <overloads>
		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RegularExpressionAttribute"/> and
		/// has an error message.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RegularExpressionAttribute"/>,
		/// and it has an error message that is not <c>null</c>, empty or consists only of white-space characters. The assertion
		/// fails if the specified model property does not have the attribute, or the error message is <c>null</c>, empty or
		/// consists only of white-space characters.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [RegularExpression]
		/// MvcAssert.HasRegularExpressionErrorMessage&lt;SomeInputModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasRegularExpressionErrorMessage<TModel>(Expression<Func<TModel, string>> expression)
		{
			var attribute = AttributeHelpers.GetAttribute(expression, typeof(RegularExpressionAttribute));
			if (attribute == null)
			{
				Assert.Fail("No RegularExpression attribute found");
			}
			IsNotNullOrWhiteSpace(((RegularExpressionAttribute)attribute).ErrorMessage);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RegularExpressionAttribute"/>,
		/// and it has <paramref name="expectedErrorMessage"/> as the error message. The assertion fails if the specified model
		/// property does not have the attribute, or the error message is not <paramref name="expectedErrorMessage"/>.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedErrorMessage">The expected error message.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [RegularExpression]
		/// MvcAssert.HasRegularExpressionErrorMessage&lt;SomeInputModel&gt;("Some message", x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasRegularExpressionErrorMessage<TModel>(string expectedErrorMessage, Expression<Func<TModel, string>> expression)
		{
			var attribute = AttributeHelpers.GetAttribute(expression, typeof(RegularExpressionAttribute));
			if (attribute == null)
			{
				Assert.Fail("No RegularExpression attribute found");
			}
			var actualErrorMessage = ((RegularExpressionAttribute)attribute).ErrorMessage;
			Assert.AreEqual(expectedErrorMessage, actualErrorMessage);
		}

		/// <overloads>
		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RequiredAttribute"/> and has an error message.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RequiredAttribute"/>,
		/// and it has an error message that is not <c>null</c>, empty or consists only of white-space characters. The assertion
		/// fails if the specified model property does not have the attribute, or the error message is <c>null</c>, empty or
		/// consists only of white-space characters.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [RegularExpression]
		/// MvcAssert.HasRequiredErrorMessage&lt;SomeInputModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasRequiredErrorMessage<TModel>(Expression<Func<TModel, string>> expression)
		{
			var attribute = AttributeHelpers.GetAttribute(expression, typeof(RegularExpressionAttribute));
			if (attribute == null)
			{
				Assert.Fail("No RegularExpression attribute found");
			}
			IsNotNullOrWhiteSpace(((RegularExpressionAttribute)attribute).ErrorMessage);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RequiredAttribute"/>,
		/// and it has <paramref name="expectedErrorMessage"/> as the error message. The assertion fails if the specified model
		/// property does not have the attribute, or the error message is not <paramref name="expectedErrorMessage"/>.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedErrorMessage">The expected error message.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [RegularExpression]
		/// MvcAssert.HasRequiredErrorMessage&lt;SomeInputModel&gt;("Some message", x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasRequiredErrorMessage<TModel>(string expectedErrorMessage, Expression<Func<TModel, string>> expression)
		{
			var attribute = AttributeHelpers.GetAttribute(expression, typeof(RegularExpressionAttribute));
			if (attribute == null)
			{
				Assert.Fail("No RegularExpression attribute found");
			}
			var actualErrorMessage = ((RegularExpressionAttribute)attribute).ErrorMessage;
			Assert.AreEqual(expectedErrorMessage, actualErrorMessage);
		}
	}
}
