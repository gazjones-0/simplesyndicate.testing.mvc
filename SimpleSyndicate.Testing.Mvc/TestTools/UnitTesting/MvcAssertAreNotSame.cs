﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SimpleSyndicate.Testing.Mvc.TestTools.UnitTesting
{
	/// <summary>
	/// Verifies conditions in unit tests using true/false propositions; this class provides all the functionality of <see cref="Assert"/>
	/// but with extra functionality specifically designed for MVC applications.
	/// </summary>
	public static partial class MvcAssert
	{
		/// <overloads>
		/// <summary>
		/// Verifies that specified object variables refer to different objects.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Verifies that two specified object variables refer to different objects. The assertion fails if they refer to the same object.
		/// </summary>
		/// <param name="notExpected">The first object to compare. This is the object the unit test expects not to match <paramref name="actual"/>.</param>
		/// <param name="actual">The second object to compare. This is the object the unit test produced.</param>
		public static void AreNotSame(object notExpected, object actual)
		{
			Assert.AreNotSame(notExpected, actual);
		}

		/// <summary>
		/// Verifies that two specified object variables refer to different objects. The assertion fails if they refer to the same object. Displays a message if the assertion fails. 
		/// </summary>
		/// <param name="notExpected">The first object to compare. This is the object the unit test expects not to match <paramref name="actual"/>.</param>
		/// <param name="actual">The second object to compare. This is the object the unit test produced.</param>
		/// <param name="message">A message to display if the assertion fails. This message can be seen in the unit test results.</param>
		public static void AreNotSame(object notExpected, object actual, string message)
		{
			Assert.AreNotSame(notExpected, actual, message);
		}

		/// <summary>
		/// Verifies that two specified object variables refer to different objects. The assertion fails if they refer to the same object. Displays a message if the assertion fails, and applies the specified formatting to it.
		/// </summary>
		/// <param name="notExpected">The first object to compare. This is the object the unit test expects not to match <paramref name="actual"/>.</param>
		/// <param name="actual">The second object to compare. This is the object the unit test produced.</param>
		/// <param name="message">A message to display if the assertion fails. This message can be seen in the unit test results.</param>
		/// <param name="parameters">An array of parameters to use when formatting <paramref name="message"/>.</param>
		public static void AreNotSame(object notExpected, object actual, string message, params object[] parameters)
		{
			Assert.AreNotSame(notExpected, actual, message, parameters);
		}
	}
}
