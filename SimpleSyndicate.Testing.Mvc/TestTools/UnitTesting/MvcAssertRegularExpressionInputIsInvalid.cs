﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SimpleSyndicate.Testing.Mvc.TestTools.UnitTesting
{
	/// <summary>
	/// Verifies conditions in unit tests using true/false propositions; this class provides all the functionality of <see cref="Assert"/>
	/// but with extra functionality specifically designed for MVC applications.
	/// </summary>
	public static partial class MvcAssert
	{
		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RegularExpressionAttribute"/> and that
		/// the specified input does not match the regular expression. The assertion fails if the specified model property does not
		/// have the attribute, or the model property has the attribute but the specified input passes validation.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="wrongInput">The input that is expected not to validate against the regular expression.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [RegularExpression]
		/// MvcAssert.RegularExpressionInputIsValid&lt;SomeInputModel&gt;("wrong input", x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void RegularExpressionInputIsInvalid<TModel>(string wrongInput, Expression<Func<TModel, string>> expression)
		{
			RegularExpressionInputIsInvalidInternal<TModel, string>(wrongInput, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RegularExpressionAttribute"/> and that
		/// the specified input does not match the regular expression. The assertion fails if the specified model property does not
		/// have the attribute, or the model property has the attribute but the specified input passes validation.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <typeparam name="TType">The property type.</typeparam>
		/// <param name="wrongInput">The input that is expected not to validate against the regular expression.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		private static void RegularExpressionInputIsInvalidInternal<TModel, TType>(string wrongInput, Expression<Func<TModel, TType>> expression)
		{
			var attribute = (RegularExpressionAttribute)AttributeHelpers.GetAttribute(expression, typeof(RegularExpressionAttribute));
			if (attribute == null)
			{
				Assert.Fail("No RegularExpression attribute found");
			}
			Assert.IsFalse(attribute.IsValid(wrongInput), "Regular expression was not invalid for input \"{0}\" (input passed validation)", wrongInput);
		}
	}
}
