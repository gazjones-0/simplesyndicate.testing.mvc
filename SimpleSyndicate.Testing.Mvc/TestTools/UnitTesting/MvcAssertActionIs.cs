﻿using System;
using System.Linq.Expressions;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SimpleSyndicate.Testing.Mvc.TestTools.UnitTesting
{
	/// <summary>
	/// Verifies conditions in unit tests using true/false propositions; this class provides all the functionality of <see cref="Assert"/>
	/// but with extra functionality specifically designed for MVC applications.
	/// </summary>
	public static partial class MvcAssert
	{
		/// <overloads>
		/// <summary>
		/// Verifies that the specified controller method has been decorated with an <see cref="ActionNameAttribute"/> with the specified action name.
		/// The assertion fails if the specified method does not have the attribute, or the attribute is present but with a different name.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Verifies that the specified controller method has been decorated with an <see cref="ActionNameAttribute"/> with the specified action name.
		/// The assertion fails if the specified method does not have the attribute, or the attribute is present but with a different name.
		/// </summary>
		/// <typeparam name="TController">The controller type.</typeparam>
		/// <param name="expectedActionName">The name of the action the attribute is expected to specify.</param>
		/// <param name="expression">The method that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the method isn't called, it's just examined to see if it has [ActionName("Delete")]
		/// MvcAssert.ActionNameIs&lt;SomeController&gt;("Delete", x => x.DeleteConfirmed(0));
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void ActionNameIs<TController>(string expectedActionName, Expression<Func<TController, ActionResult>> expression)
		{
			ActionNameIsInternal<TController, ActionResult>(expectedActionName, expression);
		}

		/// <summary>
		/// Verifies that the specified controller method has been decorated with an <see cref="ActionNameAttribute"/> with the specified action name.
		/// The assertion fails if the specified method does not have the attribute, or the attribute is present but with a different name.
		/// </summary>
		/// <typeparam name="TController">The controller type.</typeparam>
		/// <param name="expectedActionName">The name of the action the attribute is expected to specify.</param>
		/// <param name="expression">The method that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the method isn't called, it's just examined to see if it has [ActionName("Delete")]
		/// MvcAssert.ActionNameIs&lt;SomeController&gt;("Delete", x => x.DeleteConfirmed(0));
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void ActionNameIs<TController>(string expectedActionName, Expression<Func<TController, JsonResult>> expression)
		{
			ActionNameIsInternal<TController, JsonResult>(expectedActionName, expression);
		}

		/// <summary>
		/// Verifies that the specified controller method has been decorated with an <see cref="ActionNameAttribute"/> with the specified action name.
		/// The assertion fails if the specified method does not have the attribute, or the attribute is present but with a different name.
		/// </summary>
		/// <typeparam name="TController">The controller type.</typeparam>
		/// <param name="expectedActionName">The name of the action the attribute is expected to specify.</param>
		/// <param name="expression">The method that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the method isn't called, it's just examined to see if it has [ActionName("Delete")]
		/// MvcAssert.ActionNameIs&lt;SomeController&gt;("Delete", x => x.DeleteConfirmed(0));
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void ActionNameIs<TController>(string expectedActionName, Expression<Func<TController, ViewResult>> expression)
		{
			ActionNameIsInternal<TController, ViewResult>(expectedActionName, expression);
		}

		/// <summary>
		/// Verifies that the specified controller method has been decorated with an <see cref="ActionNameAttribute"/> with the specified action name.
		/// The assertion fails if the specified method does not have the attribute, or the attribute is present but with a different name.
		/// </summary>
		/// <typeparam name="TController">The controller type.</typeparam>
		/// <typeparam name="TReturnType">The method's return type.</typeparam>
		/// <param name="expectedActionName">The name of the action the attribute is expected to specify.</param>
		/// <param name="expression">The method that is expected to be decorated with the attribute.</param>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		private static void ActionNameIsInternal<TController, TReturnType>(string expectedActionName, Expression<Func<TController, TReturnType>> expression)
		{
			var attribute = AttributeHelpers.GetAttribute(expression, typeof(ActionNameAttribute));
			if (attribute == null)
			{
				Assert.Fail("No ActionName attribute found (exepcted ActionName to be {0})", expectedActionName);
			}
			var actualActionName = ((ActionNameAttribute)attribute).Name;
			Assert.AreEqual(expectedActionName, actualActionName);
		}
	}
}
