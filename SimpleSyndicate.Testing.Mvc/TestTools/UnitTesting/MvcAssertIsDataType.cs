﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SimpleSyndicate.Testing.Mvc.TestTools.UnitTesting
{
	/// <summary>
	/// Verifies conditions in unit tests using true/false propositions; this class provides all the functionality of <see cref="Assert"/>
	/// but with extra functionality specifically designed for MVC applications.
	/// </summary>
	public static partial class MvcAssert
	{
		/// <overloads>
		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DataTypeAttribute"/> with the specified data type.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different data type.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DataTypeAttribute"/> with the specified data type.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different data type.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataType">The expected data type.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DataType(DataType.Date)]
		/// MvcAssert.IsDataType&lt;SomeInputModel&gt;(DataType.Date, x => x.Date);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsDataType<TModel>(DataType expectedDataType, Expression<Func<TModel, bool>> expression)
		{
			IsDataTypeInternal<TModel, bool>(expectedDataType, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DataTypeAttribute"/> with the specified data type.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different data type.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataType">The expected data type.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DataType(DataType.Date)]
		/// MvcAssert.IsDataType&lt;SomeInputModel&gt;(DataType.Date, x => x.Date);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsDataType<TModel>(DataType expectedDataType, Expression<Func<TModel, bool?>> expression)
		{
			IsDataTypeInternal<TModel, bool?>(expectedDataType, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DataTypeAttribute"/> with the specified data type.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different data type.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataType">The expected data type.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DataType(DataType.Date)]
		/// MvcAssert.IsDataType&lt;SomeInputModel&gt;(DataType.Date, x => x.Date);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsDataType<TModel>(DataType expectedDataType, Expression<Func<TModel, byte>> expression)
		{
			IsDataTypeInternal<TModel, byte>(expectedDataType, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DataTypeAttribute"/> with the specified data type.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different data type.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataType">The expected data type.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DataType(DataType.Date)]
		/// MvcAssert.IsDataType&lt;SomeInputModel&gt;(DataType.Date, x => x.Date);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsDataType<TModel>(DataType expectedDataType, Expression<Func<TModel, byte?>> expression)
		{
			IsDataTypeInternal<TModel, byte?>(expectedDataType, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DataTypeAttribute"/> with the specified data type.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different data type.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataType">The expected data type.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DataType(DataType.Date)]
		/// MvcAssert.IsDataType&lt;SomeInputModel&gt;(DataType.Date, x => x.Date);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsDataType<TModel>(DataType expectedDataType, Expression<Func<TModel, char>> expression)
		{
			IsDataTypeInternal<TModel, char>(expectedDataType, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DataTypeAttribute"/> with the specified data type.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different data type.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataType">The expected data type.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DataType(DataType.Date)]
		/// MvcAssert.IsDataType&lt;SomeInputModel&gt;(DataType.Date, x => x.Date);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsDataType<TModel>(DataType expectedDataType, Expression<Func<TModel, char?>> expression)
		{
			IsDataTypeInternal<TModel, char?>(expectedDataType, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DataTypeAttribute"/> with the specified data type.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different data type.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataType">The expected data type.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DataType(DataType.Date)]
		/// MvcAssert.IsDataType&lt;SomeInputModel&gt;(DataType.Date, x => x.Date);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsDataType<TModel>(DataType expectedDataType, Expression<Func<TModel, DateTime>> expression)
		{
			IsDataTypeInternal<TModel, DateTime>(expectedDataType, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DataTypeAttribute"/> with the specified data type.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different data type.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataType">The expected data type.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DataType(DataType.Date)]
		/// MvcAssert.IsDataType&lt;SomeInputModel&gt;(DataType.Date, x => x.Date);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsDataType<TModel>(DataType expectedDataType, Expression<Func<TModel, DateTime?>> expression)
		{
			IsDataTypeInternal<TModel, DateTime?>(expectedDataType, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DataTypeAttribute"/> with the specified data type.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different data type.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataType">The expected data type.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DataType(DataType.Date)]
		/// MvcAssert.IsDataType&lt;SomeInputModel&gt;(DataType.Date, x => x.Date);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsDataType<TModel>(DataType expectedDataType, Expression<Func<TModel, decimal>> expression)
		{
			IsDataTypeInternal<TModel, decimal>(expectedDataType, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DataTypeAttribute"/> with the specified data type.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different data type.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataType">The expected data type.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DataType(DataType.Date)]
		/// MvcAssert.IsDataType&lt;SomeInputModel&gt;(DataType.Date, x => x.Date);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsDataType<TModel>(DataType expectedDataType, Expression<Func<TModel, decimal?>> expression)
		{
			IsDataTypeInternal<TModel, decimal?>(expectedDataType, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DataTypeAttribute"/> with the specified data type.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different data type.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataType">The expected data type.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DataType(DataType.Date)]
		/// MvcAssert.IsDataType&lt;SomeInputModel&gt;(DataType.Date, x => x.Date);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsDataType<TModel>(DataType expectedDataType, Expression<Func<TModel, double>> expression)
		{
			IsDataTypeInternal<TModel, double>(expectedDataType, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DataTypeAttribute"/> with the specified data type.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different data type.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataType">The expected data type.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DataType(DataType.Date)]
		/// MvcAssert.IsDataType&lt;SomeInputModel&gt;(DataType.Date, x => x.Date);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsDataType<TModel>(DataType expectedDataType, Expression<Func<TModel, double?>> expression)
		{
			IsDataTypeInternal<TModel, double?>(expectedDataType, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DataTypeAttribute"/> with the specified data type.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different data type.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataType">The expected data type.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DataType(DataType.Date)]
		/// MvcAssert.IsDataType&lt;SomeInputModel&gt;(DataType.Date, x => x.Date);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsDataType<TModel>(DataType expectedDataType, Expression<Func<TModel, float>> expression)
		{
			IsDataTypeInternal<TModel, float>(expectedDataType, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DataTypeAttribute"/> with the specified data type.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different data type.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataType">The expected data type.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DataType(DataType.Date)]
		/// MvcAssert.IsDataType&lt;SomeInputModel&gt;(DataType.Date, x => x.Date);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsDataType<TModel>(DataType expectedDataType, Expression<Func<TModel, float?>> expression)
		{
			IsDataTypeInternal<TModel, float?>(expectedDataType, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DataTypeAttribute"/> with the specified data type.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different data type.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataType">The expected data type.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DataType(DataType.Date)]
		/// MvcAssert.IsDataType&lt;SomeInputModel&gt;(DataType.Date, x => x.Date);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsDataType<TModel>(DataType expectedDataType, Expression<Func<TModel, int>> expression)
		{
			IsDataTypeInternal<TModel, int>(expectedDataType, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DataTypeAttribute"/> with the specified data type.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different data type.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataType">The expected data type.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DataType(DataType.Date)]
		/// MvcAssert.IsDataType&lt;SomeInputModel&gt;(DataType.Date, x => x.Date);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsDataType<TModel>(DataType expectedDataType, Expression<Func<TModel, int?>> expression)
		{
			IsDataTypeInternal<TModel, int?>(expectedDataType, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DataTypeAttribute"/> with the specified data type.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different data type.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataType">The expected data type.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DataType(DataType.Date)]
		/// MvcAssert.IsDataType&lt;SomeInputModel&gt;(DataType.Date, x => x.Date);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsDataType<TModel>(DataType expectedDataType, Expression<Func<TModel, long>> expression)
		{
			IsDataTypeInternal<TModel, long>(expectedDataType, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DataTypeAttribute"/> with the specified data type.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different data type.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataType">The expected data type.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DataType(DataType.Date)]
		/// MvcAssert.IsDataType&lt;SomeInputModel&gt;(DataType.Date, x => x.Date);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsDataType<TModel>(DataType expectedDataType, Expression<Func<TModel, long?>> expression)
		{
			IsDataTypeInternal<TModel, long?>(expectedDataType, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DataTypeAttribute"/> with the specified data type.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different data type.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataType">The expected data type.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DataType(DataType.Date)]
		/// MvcAssert.IsDataType&lt;SomeInputModel&gt;(DataType.Date, x => x.Date);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsDataType<TModel>(DataType expectedDataType, Expression<Func<TModel, sbyte>> expression)
		{
			IsDataTypeInternal<TModel, sbyte>(expectedDataType, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DataTypeAttribute"/> with the specified data type.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different data type.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataType">The expected data type.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DataType(DataType.Date)]
		/// MvcAssert.IsDataType&lt;SomeInputModel&gt;(DataType.Date, x => x.Date);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsDataType<TModel>(DataType expectedDataType, Expression<Func<TModel, sbyte?>> expression)
		{
			IsDataTypeInternal<TModel, sbyte?>(expectedDataType, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DataTypeAttribute"/> with the specified data type.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different data type.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataType">The expected data type.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DataType(DataType.Date)]
		/// MvcAssert.IsDataType&lt;SomeInputModel&gt;(DataType.Date, x => x.Date);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsDataType<TModel>(DataType expectedDataType, Expression<Func<TModel, short>> expression)
		{
			IsDataTypeInternal<TModel, short>(expectedDataType, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DataTypeAttribute"/> with the specified data type.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different data type.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataType">The expected data type.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DataType(DataType.Date)]
		/// MvcAssert.IsDataType&lt;SomeInputModel&gt;(DataType.Date, x => x.Date);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsDataType<TModel>(DataType expectedDataType, Expression<Func<TModel, short?>> expression)
		{
			IsDataTypeInternal<TModel, short?>(expectedDataType, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DataTypeAttribute"/> with the specified data type.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different data type.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataType">The expected data type.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DataType(DataType.Date)]
		/// MvcAssert.IsDataType&lt;SomeInputModel&gt;(DataType.Date, x => x.Date);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsDataType<TModel>(DataType expectedDataType, Expression<Func<TModel, string>> expression)
		{
			IsDataTypeInternal<TModel, string>(expectedDataType, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DataTypeAttribute"/> with the specified data type.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different data type.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataType">The expected data type.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DataType(DataType.Date)]
		/// MvcAssert.IsDataType&lt;SomeInputModel&gt;(DataType.Date, x => x.Date);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsDataType<TModel>(DataType expectedDataType, Expression<Func<TModel, uint>> expression)
		{
			IsDataTypeInternal<TModel, uint>(expectedDataType, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DataTypeAttribute"/> with the specified data type.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different data type.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataType">The expected data type.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DataType(DataType.Date)]
		/// MvcAssert.IsDataType&lt;SomeInputModel&gt;(DataType.Date, x => x.Date);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsDataType<TModel>(DataType expectedDataType, Expression<Func<TModel, uint?>> expression)
		{
			IsDataTypeInternal<TModel, uint?>(expectedDataType, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DataTypeAttribute"/> with the specified data type.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different data type.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataType">The expected data type.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DataType(DataType.Date)]
		/// MvcAssert.IsDataType&lt;SomeInputModel&gt;(DataType.Date, x => x.Date);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsDataType<TModel>(DataType expectedDataType, Expression<Func<TModel, ulong>> expression)
		{
			IsDataTypeInternal<TModel, ulong>(expectedDataType, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DataTypeAttribute"/> with the specified data type.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different data type.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataType">The expected data type.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DataType(DataType.Date)]
		/// MvcAssert.IsDataType&lt;SomeInputModel&gt;(DataType.Date, x => x.Date);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsDataType<TModel>(DataType expectedDataType, Expression<Func<TModel, ulong?>> expression)
		{
			IsDataTypeInternal<TModel, ulong?>(expectedDataType, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DataTypeAttribute"/> with the specified data type.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different data type.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataType">The expected data type.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DataType(DataType.Date)]
		/// MvcAssert.IsDataType&lt;SomeInputModel&gt;(DataType.Date, x => x.Date);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsDataType<TModel>(DataType expectedDataType, Expression<Func<TModel, ushort>> expression)
		{
			IsDataTypeInternal<TModel, ushort>(expectedDataType, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DataTypeAttribute"/> with the specified data type.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different data type.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataType">The expected data type.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DataType(DataType.Date)]
		/// MvcAssert.IsDataType&lt;SomeInputModel&gt;(DataType.Date, x => x.Date);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsDataType<TModel>(DataType expectedDataType, Expression<Func<TModel, ushort?>> expression)
		{
			IsDataTypeInternal<TModel, ushort?>(expectedDataType, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DataTypeAttribute"/> with the specified data type.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different data type.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <typeparam name="TType">The property type.</typeparam>
		/// <param name="expectedDataType">The expected data type.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		private static void IsDataTypeInternal<TModel, TType>(DataType expectedDataType, Expression<Func<TModel, TType>> expression)
		{
			var attribute = AttributeHelpers.GetAttribute(expression, typeof(DataTypeAttribute));
			if (attribute == null)
			{
				Assert.Fail("No DataType attribute found");
			}
			var actualDataType = ((DataTypeAttribute)attribute).DataType;
			Assert.AreEqual<DataType>(expectedDataType, actualDataType);
		}
	}
}
