﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SimpleSyndicate.Testing.Mvc.TestTools.UnitTesting
{
	/// <summary>
	/// Verifies conditions in unit tests using true/false propositions; this class provides all the functionality of <see cref="Assert"/>
	/// but with extra functionality specifically designed for MVC applications.
	/// </summary>
	public static partial class MvcAssert
	{
		/// <overloads>
		/// <summary>
		/// Verifies that a specified string is <c>null</c>, empty or consists only of white-space characters.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Verifies that the specified string is <c>null</c>, empty or consists only of white-space characters. The assertion fails if it is not <c>null</c>, not empty or doesn't consist only of white-space characters.
		/// </summary>
		/// <param name="value">The string to verify is <c>null</c>, empty or consists only of white-space characters.</param>
		public static void IsNullOrWhiteSpace(string value)
		{
			Assert.IsTrue(String.IsNullOrWhiteSpace(value));
		}

		/// <summary>
		/// Verifies that the specified string is <c>null</c>, empty or consists only of white-space characters. The assertion fails if it is not <c>null</c>, not empty or doesn't consist only of white-space characters. Displays a message if the assertion fails.
		/// </summary>
		/// <param name="value">The string to verify is <c>null</c>, empty or consists only of white-space characters.</param>
		/// <param name="message">A message to display if the assertion fails. This message can be seen in the unit test results.</param>
		public static void IsNullOrWhiteSpace(string value, string message)
		{
			Assert.IsTrue(String.IsNullOrWhiteSpace(value), message);
		}

		/// <summary>
		/// Verifies that the specified string is <c>null</c>, empty or consists only of white-space characters. The assertion fails if it is not <c>null</c>, not empty or doesn't consist only of white-space characters. Displays a message if the assertion fails, and applies the specified formatting to it.
		/// </summary>
		/// <param name="value">The string to verify is <c>null</c>, empty or consists only of white-space characters.</param>
		/// <param name="message">A message to display if the assertion fails. This message can be seen in the unit test results.</param>
		/// <param name="parameters">An array of parameters to use when formatting <paramref name="message"/>.</param>
		public static void IsNullOrWhiteSpace(string value, string message, params object[] parameters)
		{
			Assert.IsTrue(String.IsNullOrWhiteSpace(value), message, parameters);
		}
	}
}
