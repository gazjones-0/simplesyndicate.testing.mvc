﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using System.Web.Mvc.Html;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSyndicate.Testing.Mvc.Web.Mvc;

namespace SimpleSyndicate.Testing.Mvc.TestTools.UnitTesting
{
	/// <summary>
	/// Verifies conditions in unit tests using true/false propositions; this class provides all the functionality of <see cref="Assert"/>
	/// but with extra functionality specifically designed for MVC applications.
	/// </summary>
	public static partial class MvcAssert
	{
		/// <overloads>
		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> with the specified name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different name.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> with the specified name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different name.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDisplayName">The expected display name.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "User name")]
		/// MvcAssert.DisplayNameForEquals&lt;SomeViewModel&gt;("User name", x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayNameForEquals<TModel>(string expectedDisplayName, Expression<Func<TModel, bool>> expression)
		{
			DisplayNameForEqualsInternal<TModel, bool>(expectedDisplayName, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> with the specified name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different name.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDisplayName">The expected display name.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "User name")]
		/// MvcAssert.DisplayNameForEquals&lt;SomeViewModel&gt;("User name", x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayNameForEquals<TModel>(string expectedDisplayName, Expression<Func<TModel, bool?>> expression)
		{
			DisplayNameForEqualsInternal<TModel, bool?>(expectedDisplayName, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> with the specified name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different name.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDisplayName">The expected display name.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "User name")]
		/// MvcAssert.DisplayNameForEquals&lt;SomeViewModel&gt;("User name", x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayNameForEquals<TModel>(string expectedDisplayName, Expression<Func<TModel, byte>> expression)
		{
			DisplayNameForEqualsInternal<TModel, byte>(expectedDisplayName, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> with the specified name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different name.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDisplayName">The expected display name.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "User name")]
		/// MvcAssert.DisplayNameForEquals&lt;SomeViewModel&gt;("User name", x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayNameForEquals<TModel>(string expectedDisplayName, Expression<Func<TModel, byte?>> expression)
		{
			DisplayNameForEqualsInternal<TModel, byte?>(expectedDisplayName, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> with the specified name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different name.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDisplayName">The expected display name.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "User name")]
		/// MvcAssert.DisplayNameForEquals&lt;SomeViewModel&gt;("User name", x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayNameForEquals<TModel>(string expectedDisplayName, Expression<Func<TModel, char>> expression)
		{
			DisplayNameForEqualsInternal<TModel, char>(expectedDisplayName, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> with the specified name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different name.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDisplayName">The expected display name.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "User name")]
		/// MvcAssert.DisplayNameForEquals&lt;SomeViewModel&gt;("User name", x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayNameForEquals<TModel>(string expectedDisplayName, Expression<Func<TModel, char?>> expression)
		{
			DisplayNameForEqualsInternal<TModel, char?>(expectedDisplayName, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> with the specified name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different name.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDisplayName">The expected display name.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "User name")]
		/// MvcAssert.DisplayNameForEquals&lt;SomeViewModel&gt;("User name", x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayNameForEquals<TModel>(string expectedDisplayName, Expression<Func<TModel, DateTime>> expression)
		{
			DisplayNameForEqualsInternal<TModel, DateTime>(expectedDisplayName, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> with the specified name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different name.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDisplayName">The expected display name.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "User name")]
		/// MvcAssert.DisplayNameForEquals&lt;SomeViewModel&gt;("User name", x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayNameForEquals<TModel>(string expectedDisplayName, Expression<Func<TModel, DateTime?>> expression)
		{
			DisplayNameForEqualsInternal<TModel, DateTime?>(expectedDisplayName, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> with the specified name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different name.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDisplayName">The expected display name.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "User name")]
		/// MvcAssert.DisplayNameForEquals&lt;SomeViewModel&gt;("User name", x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayNameForEquals<TModel>(string expectedDisplayName, Expression<Func<TModel, decimal>> expression)
		{
			DisplayNameForEqualsInternal<TModel, decimal>(expectedDisplayName, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> with the specified name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different name.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDisplayName">The expected display name.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "User name")]
		/// MvcAssert.DisplayNameForEquals&lt;SomeViewModel&gt;("User name", x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayNameForEquals<TModel>(string expectedDisplayName, Expression<Func<TModel, decimal?>> expression)
		{
			DisplayNameForEqualsInternal<TModel, decimal?>(expectedDisplayName, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> with the specified name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different name.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDisplayName">The expected display name.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "User name")]
		/// MvcAssert.DisplayNameForEquals&lt;SomeViewModel&gt;("User name", x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayNameForEquals<TModel>(string expectedDisplayName, Expression<Func<TModel, double>> expression)
		{
			DisplayNameForEqualsInternal<TModel, double>(expectedDisplayName, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> with the specified name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different name.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDisplayName">The expected display name.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "User name")]
		/// MvcAssert.DisplayNameForEquals&lt;SomeViewModel&gt;("User name", x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayNameForEquals<TModel>(string expectedDisplayName, Expression<Func<TModel, double?>> expression)
		{
			DisplayNameForEqualsInternal<TModel, double?>(expectedDisplayName, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> with the specified name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different name.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDisplayName">The expected display name.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "User name")]
		/// MvcAssert.DisplayNameForEquals&lt;SomeViewModel&gt;("User name", x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayNameForEquals<TModel>(string expectedDisplayName, Expression<Func<TModel, float>> expression)
		{
			DisplayNameForEqualsInternal<TModel, float>(expectedDisplayName, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> with the specified name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different name.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDisplayName">The expected display name.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "User name")]
		/// MvcAssert.DisplayNameForEquals&lt;SomeViewModel&gt;("User name", x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayNameForEquals<TModel>(string expectedDisplayName, Expression<Func<TModel, float?>> expression)
		{
			DisplayNameForEqualsInternal<TModel, float?>(expectedDisplayName, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> with the specified name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different name.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDisplayName">The expected display name.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "User name")]
		/// MvcAssert.DisplayNameForEquals&lt;SomeViewModel&gt;("User name", x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayNameForEquals<TModel>(string expectedDisplayName, Expression<Func<TModel, int>> expression)
		{
			DisplayNameForEqualsInternal<TModel, int>(expectedDisplayName, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> with the specified name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different name.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDisplayName">The expected display name.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "User name")]
		/// MvcAssert.DisplayNameForEquals&lt;SomeViewModel&gt;("User name", x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayNameForEquals<TModel>(string expectedDisplayName, Expression<Func<TModel, int?>> expression)
		{
			DisplayNameForEqualsInternal<TModel, int?>(expectedDisplayName, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> with the specified name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different name.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDisplayName">The expected display name.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "User name")]
		/// MvcAssert.DisplayNameForEquals&lt;SomeViewModel&gt;("User name", x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayNameForEquals<TModel>(string expectedDisplayName, Expression<Func<TModel, long>> expression)
		{
			DisplayNameForEqualsInternal<TModel, long>(expectedDisplayName, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> with the specified name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different name.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDisplayName">The expected display name.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "User name")]
		/// MvcAssert.DisplayNameForEquals&lt;SomeViewModel&gt;("User name", x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayNameForEquals<TModel>(string expectedDisplayName, Expression<Func<TModel, long?>> expression)
		{
			DisplayNameForEqualsInternal<TModel, long?>(expectedDisplayName, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> with the specified name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different name.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDisplayName">The expected display name.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "User name")]
		/// MvcAssert.DisplayNameForEquals&lt;SomeViewModel&gt;("User name", x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayNameForEquals<TModel>(string expectedDisplayName, Expression<Func<TModel, sbyte>> expression)
		{
			DisplayNameForEqualsInternal<TModel, sbyte>(expectedDisplayName, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> with the specified name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different name.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDisplayName">The expected display name.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "User name")]
		/// MvcAssert.DisplayNameForEquals&lt;SomeViewModel&gt;("User name", x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayNameForEquals<TModel>(string expectedDisplayName, Expression<Func<TModel, sbyte?>> expression)
		{
			DisplayNameForEqualsInternal<TModel, sbyte?>(expectedDisplayName, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> with the specified name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different name.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDisplayName">The expected display name.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "User name")]
		/// MvcAssert.DisplayNameForEquals&lt;SomeViewModel&gt;("User name", x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayNameForEquals<TModel>(string expectedDisplayName, Expression<Func<TModel, short>> expression)
		{
			DisplayNameForEqualsInternal<TModel, short>(expectedDisplayName, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> with the specified name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different name.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDisplayName">The expected display name.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "User name")]
		/// MvcAssert.DisplayNameForEquals&lt;SomeViewModel&gt;("User name", x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayNameForEquals<TModel>(string expectedDisplayName, Expression<Func<TModel, short?>> expression)
		{
			DisplayNameForEqualsInternal<TModel, short?>(expectedDisplayName, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> with the specified name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different name.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDisplayName">The expected display name.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "User name")]
		/// MvcAssert.DisplayNameForEquals&lt;SomeViewModel&gt;("User name", x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayNameForEquals<TModel>(string expectedDisplayName, Expression<Func<TModel, string>> expression)
		{
			DisplayNameForEqualsInternal<TModel, string>(expectedDisplayName, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> with the specified name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different name.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDisplayName">The expected display name.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "User name")]
		/// MvcAssert.DisplayNameForEquals&lt;SomeViewModel&gt;("User name", x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayNameForEquals<TModel>(string expectedDisplayName, Expression<Func<TModel, uint>> expression)
		{
			DisplayNameForEqualsInternal<TModel, uint>(expectedDisplayName, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> with the specified name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different name.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDisplayName">The expected display name.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "User name")]
		/// MvcAssert.DisplayNameForEquals&lt;SomeViewModel&gt;("User name", x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayNameForEquals<TModel>(string expectedDisplayName, Expression<Func<TModel, uint?>> expression)
		{
			DisplayNameForEqualsInternal<TModel, uint?>(expectedDisplayName, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> with the specified name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different name.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDisplayName">The expected display name.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "User name")]
		/// MvcAssert.DisplayNameForEquals&lt;SomeViewModel&gt;("User name", x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayNameForEquals<TModel>(string expectedDisplayName, Expression<Func<TModel, ulong>> expression)
		{
			DisplayNameForEqualsInternal<TModel, ulong>(expectedDisplayName, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> with the specified name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different name.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDisplayName">The expected display name.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "User name")]
		/// MvcAssert.DisplayNameForEquals&lt;SomeViewModel&gt;("User name", x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayNameForEquals<TModel>(string expectedDisplayName, Expression<Func<TModel, ulong?>> expression)
		{
			DisplayNameForEqualsInternal<TModel, ulong?>(expectedDisplayName, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> with the specified name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different name.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDisplayName">The expected display name.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "User name")]
		/// MvcAssert.DisplayNameForEquals&lt;SomeViewModel&gt;("User name", x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayNameForEquals<TModel>(string expectedDisplayName, Expression<Func<TModel, ushort>> expression)
		{
			DisplayNameForEqualsInternal<TModel, ushort>(expectedDisplayName, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> with the specified name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different name.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDisplayName">The expected display name.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "User name")]
		/// MvcAssert.DisplayNameForEquals&lt;SomeViewModel&gt;("User name", x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayNameForEquals<TModel>(string expectedDisplayName, Expression<Func<TModel, ushort?>> expression)
		{
			DisplayNameForEqualsInternal<TModel, ushort?>(expectedDisplayName, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> with the specified name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different name.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <typeparam name="TType">The property type.</typeparam>
		/// <param name="expectedDisplayName">The expected display name.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		private static void DisplayNameForEqualsInternal<TModel, TType>(string expectedDisplayName, Expression<Func<TModel, TType>> expression)
		{
			var htmlhelper = HtmlHelperFactory.Create<TModel>();
			var value = htmlhelper.DisplayNameFor(expression);
			if (value == null)
			{
				Assert.Fail("No Display attribute found (DisplayNameFor returned null string)");
			}
			Assert.AreEqual(expectedDisplayName, value.ToString());
		}
	}
}
