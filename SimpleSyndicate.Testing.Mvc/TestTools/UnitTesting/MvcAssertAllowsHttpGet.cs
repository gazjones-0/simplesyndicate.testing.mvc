﻿using System;
using System.Linq.Expressions;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SimpleSyndicate.Testing.Mvc.TestTools.UnitTesting
{
	/// <summary>
	/// Verifies conditions in unit tests using true/false propositions; this class provides all the functionality of <see cref="Assert"/>
	/// but with extra functionality specifically designed for MVC applications.
	/// </summary>
	public static partial class MvcAssert
	{
		/// <overloads>
		/// <summary>
		/// Verifies that the specified controller method has been decorated with a <see cref="System.Web.Mvc.HttpGetAttribute"/> (for MVC
		/// methods) or <see cref="System.Web.Http.HttpGetAttribute"/> (Web API methods). The assertion fails if the specified method does
		/// not have the attribute.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Verifies that the specified controller method has been decorated with a <see cref="System.Web.Mvc.HttpGetAttribute"/>.
		/// The assertion fails if the specified method does not have the attribute.
		/// </summary>
		/// <typeparam name="TController">The controller type.</typeparam>
		/// <param name="expression">The method that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the method isn't called, it's just examined to see if it has [HttpGet]
		/// MvcAssert.AllowsHttpGet&lt;SomeController&gt;(x => x.SomeMethod());
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void AllowsHttpGet<TController>(Expression<Func<TController, ActionResult>> expression)
		{
			AllowsHttpGetInternal<TController, ActionResult>(expression, typeof(System.Web.Mvc.HttpGetAttribute));
		}

		/// <summary>
		/// Verifies that the specified controller method has been decorated with a <see cref="System.Web.Mvc.HttpGetAttribute"/>.
		/// The assertion fails if the specified method does not have the attribute.
		/// </summary>
		/// <typeparam name="TController">The controller type.</typeparam>
		/// <param name="expression">The method that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the method isn't called, it's just examined to see if it has [HttpGet]
		/// MvcAssert.AllowsHttpGet&lt;SomeController&gt;(x => x.SomeMethod());
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void AllowsHttpGet<TController>(Expression<Func<TController, JsonResult>> expression)
		{
			AllowsHttpGetInternal<TController, JsonResult>(expression, typeof(System.Web.Mvc.HttpGetAttribute));
		}

		/// <summary>
		/// Verifies that the specified controller method has been decorated with a <see cref="System.Web.Mvc.HttpGetAttribute"/>.
		/// The assertion fails if the specified method does not have the attribute.
		/// </summary>
		/// <typeparam name="TController">The controller type.</typeparam>
		/// <param name="expression">The method that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the method isn't called, it's just examined to see if it has [HttpGet]
		/// MvcAssert.AllowsHttpGet&lt;SomeController&gt;(x => x.SomeMethod());
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void AllowsHttpGet<TController>(Expression<Func<TController, ViewResult>> expression)
		{
			AllowsHttpGetInternal<TController, ViewResult>(expression, typeof(System.Web.Mvc.HttpGetAttribute));
		}

		/// <summary>
		/// Verifies that the specified controller method has been decorated with a <see cref="System.Web.Http.HttpGetAttribute"/>.
		/// The assertion fails if the specified method does not have the attribute.
		/// </summary>
		/// <typeparam name="TController">The controller type.</typeparam>
		/// <param name="expression">The method that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the method isn't called, it's just examined to see if it has [HttpGet]
		/// MvcAssert.AllowsHttpGet&lt;SomeController&gt;(x => x.SomeMethod());
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void AllowsHttpGet<TController>(Expression<Func<TController, HttpResponseMessage>> expression)
		{
			AllowsHttpGetInternal<TController, HttpResponseMessage>(expression, typeof(System.Web.Http.HttpGetAttribute));
		}

		/// <summary>
		/// Verifies that the specified controller method has been decorated with a <see cref="System.Web.Mvc.HttpGetAttribute"/> (for MVC
		/// methods) or <see cref="System.Web.Http.HttpGetAttribute"/> (Web API methods). The assertion fails if the specified method does
		/// not have the attribute.
		/// </summary>
		/// <typeparam name="TController">The controller type.</typeparam>
		/// <typeparam name="TReturnType">The method's return type.</typeparam>
		/// <param name="expression">The method that is expected to be decorated with the attribute.</param>
		/// <param name="type">The attribute type to check for.</param>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		private static void AllowsHttpGetInternal<TController, TReturnType>(Expression<Func<TController, TReturnType>> expression, Type type)
		{
			if (!AttributeHelpers.HasAttribute(expression, type))
			{
				if (type == typeof(System.Web.Http.HttpGetAttribute))
				{
					Assert.Fail("No System.Web.Http.HttpGet attribute found");
				}
				else
				{
					Assert.Fail("No System.Web.Mvc.HttpGet attribute found");
				}
			}
		}
	}
}
