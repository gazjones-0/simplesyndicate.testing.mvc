﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SimpleSyndicate.Testing.Mvc.TestTools.UnitTesting
{
	/// <summary>
	/// Verifies conditions in unit tests using true/false propositions; this class provides all the functionality of <see cref="Assert"/>
	/// but with extra functionality specifically designed for MVC applications.
	/// </summary>
	public static partial class MvcAssert
	{
		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RegularExpressionAttribute"/>,
		/// and the pattern it uses is the <paramref name="expectedPattern"/>. The assertion fails if the specified model property
		/// does not have the attribute, or it does not have the <paramref name="expectedPattern"/>.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedPattern">The expected regular expression pattern.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [RegularExpression]
		/// MvcAssert.HasRegularExpressionPattern&lt;SomeInputModel&gt;("some pattern", x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasRegularExpressionPattern<TModel>(string expectedPattern, Expression<Func<TModel, string>> expression)
		{
			var attribute = AttributeHelpers.GetAttribute(expression, typeof(RegularExpressionAttribute));
			if (attribute == null)
			{
				Assert.Fail("No RegularExpression attribute found");
			}
			Assert.AreEqual(expectedPattern, ((RegularExpressionAttribute)attribute).Pattern);
		}
	}
}
