﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SimpleSyndicate.Testing.Mvc.TestTools.UnitTesting
{
	/// <summary>
	/// Verifies conditions in unit tests using true/false propositions; this class provides all the functionality of <see cref="Assert"/>
	/// but with extra functionality specifically designed for MVC applications.
	/// </summary>
	public static partial class MvcAssert
	{
		/// <summary>
		/// Determines whether two objects are equal.
		/// </summary>
		/// <param name="objA">An object that can be cast to an Assert instance.</param>
		/// <param name="objB">An object that can be cast to an Assert instance.</param>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1625:ElementDocumentationMustNotBeCopiedAndPasted", Justification = "Documentation is copied directly from Microsoft's Asset class this is based on.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "obj", Justification = "Method that that wraps uses this naming convention; names kept for consistency.")]
		public static new void Equals(object objA, object objB)
		{
			Assert.Equals(objA, objB);
		}

		/// <summary>
		/// In a string, replaces <c>null</c> characters ('\0') with "\\0".
		/// </summary>
		/// <param name="input">The string in which to search for and replace <c>null</c> characters.</param>
		/// <returns>The converted string with <c>null</c> characters ('\0') replaced by "\\0".</returns>
		public static string ReplaceNullChars(string input)
		{
			return Assert.ReplaceNullChars(input);
		}
	}
}
