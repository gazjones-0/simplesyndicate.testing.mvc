﻿using System;
using System.Linq.Expressions;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SimpleSyndicate.Testing.Mvc.TestTools.UnitTesting
{
	/// <summary>
	/// Verifies conditions in unit tests using true/false propositions; this class provides all the functionality of <see cref="Assert"/>
	/// but with extra functionality specifically designed for MVC applications.
	/// </summary>
	public static partial class MvcAssert
	{
		/// <overloads>
		/// <summary>
		/// Verifies that the specified controller method has been decorated with a <see cref="ValidateAntiForgeryTokenAttribute"/>.
		/// The assertion fails if the specified method does not have the attribute.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Verifies that the specified controller method has been decorated with a <see cref="ValidateAntiForgeryTokenAttribute"/>.
		/// The assertion fails if the specified method does not have the attribute.
		/// </summary>
		/// <typeparam name="TController">The controller type.</typeparam>
		/// <param name="expression">The method that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the method isn't called, it's just examined to see if it has [ValidateAntiForgeryToken]
		/// MvcAssert.ValidatesAntiForgeryToken&lt;SomeController&gt;(x => x.SomeMethod());
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void ValidatesAntiForgeryToken<TController>(Expression<Func<TController, ActionResult>> expression)
		{
			ValidatesAntiForgeryTokenInternal<TController, ActionResult>(expression);
		}

		/// <summary>
		/// Verifies that the specified controller method has been decorated with a <see cref="ValidateAntiForgeryTokenAttribute"/>.
		/// The assertion fails if the specified method does not have the attribute.
		/// </summary>
		/// <typeparam name="TController">The controller type.</typeparam>
		/// <param name="expression">The method that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the method isn't called, it's just examined to see if it has [ValidateAntiForgeryToken]
		/// MvcAssert.ValidatesAntiForgeryToken&lt;SomeController&gt;(x => x.SomeMethod());
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void ValidatesAntiForgeryToken<TController>(Expression<Func<TController, JsonResult>> expression)
		{
			ValidatesAntiForgeryTokenInternal<TController, JsonResult>(expression);
		}

		/// <summary>
		/// Verifies that the specified controller method has been decorated with a <see cref="ValidateAntiForgeryTokenAttribute"/>.
		/// The assertion fails if the specified method does not have the attribute.
		/// </summary>
		/// <typeparam name="TController">The controller type.</typeparam>
		/// <param name="expression">The method that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the method isn't called, it's just examined to see if it has [ValidateAntiForgeryToken]
		/// MvcAssert.ValidatesAntiForgeryToken&lt;SomeController&gt;(x => x.SomeMethod());
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void ValidatesAntiForgeryToken<TController>(Expression<Func<TController, ViewResult>> expression)
		{
			ValidatesAntiForgeryTokenInternal<TController, ViewResult>(expression);
		}

		/// <summary>
		/// Verifies that the specified controller method has been decorated with a <see cref="ValidateAntiForgeryTokenAttribute"/>.
		/// The assertion fails if the specified method does not have the attribute.
		/// </summary>
		/// <typeparam name="TController">The controller type.</typeparam>
		/// <typeparam name="TReturnType">The return type.</typeparam>
		/// <param name="expression">The method that is expected to be decorated with the attribute.</param>
		private static void ValidatesAntiForgeryTokenInternal<TController, TReturnType>(Expression<Func<TController, TReturnType>> expression)
		{
			if (!AttributeHelpers.HasAttribute(expression, typeof(ValidateAntiForgeryTokenAttribute)))
			{
				Assert.Fail("No ValidateAntiForgeryToken attribute found");
			}
		}
	}
}
