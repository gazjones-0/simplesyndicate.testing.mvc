﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSyndicate.Testing.Mvc.Web.Mvc;

namespace SimpleSyndicate.Testing.Mvc.TestTools.UnitTesting
{
	/// <summary>
	/// Verifies conditions in unit tests using true/false propositions; this class provides all the functionality of <see cref="Assert"/>
	/// but with extra functionality specifically designed for MVC applications.
	/// </summary>
	public static partial class MvcAssert
	{
		/// <overloads>
		/// <summary>
		/// Verifies that there is a filter of the specified type in the global filters.
		/// The assertion fails if there is no filter of the specified type in the global filters list.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Verifies that there is a filter of the specified type in the global filters.
		/// The assertion fails if there is no filter of the specified type in the global filters list.
		/// </summary>
		/// <typeparam name="TType">Type of filter to check for.</typeparam>
		/// <example>
		/// <code language="cs">
		/// MvcAssert.HasGlobalFilter&lt;HandleErrorAttribute&gt;();
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Justification = "Clients shouldn't need to instantiate a filter just to check if a filter of that type is present.")]
		public static void HasGlobalFilter<TType>()
		{
			if (!GlobalFiltersHelpers.HasFilter<TType>())
			{
				Assert.Fail("No global filter of type {0}", typeof(TType).Name);
			}
		}

		/// <summary>
		/// Verifies that there is a filter of the specified type in the global filters.
		/// The assertion fails if there is no filter of the specified type in the global filters list.
		/// </summary>
		/// <typeparam name="TType">Type of filter to check for.</typeparam>
		/// <param name="objectOfTypeExpectedInGlobalFilters">An object that is of the type expected to be in the global filters list.</param>
		/// <example>
		/// <code language="cs">
		/// var someFilter = new SomeFilter();
		/// MvcAssert.HasGlobalFilter(someFilter);
		/// </code>
		/// </example>
		public static void HasGlobalFilter<TType>(TType objectOfTypeExpectedInGlobalFilters)
		{
			Assert.IsNotNull(objectOfTypeExpectedInGlobalFilters, "objectOfTypeExpectedInGlobalFilters is null");
			if (!GlobalFiltersHelpers.HasFilter(objectOfTypeExpectedInGlobalFilters))
			{
				Assert.Fail("No global filter of type {0}", objectOfTypeExpectedInGlobalFilters.GetType().Name);
			}
		}
	}
}
