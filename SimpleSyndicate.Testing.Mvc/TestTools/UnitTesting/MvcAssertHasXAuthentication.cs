﻿using Microsoft.AspNet.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSyndicate.Mvc.App_Start;
using SimpleSyndicate.Testing.Mvc.App_Start;

namespace SimpleSyndicate.Testing.Mvc.TestTools.UnitTesting
{
	/// <summary>
	/// Verifies conditions in unit tests using true/false propositions; this class provides all the functionality of <see cref="Assert"/>
	/// but with extra functionality specifically designed for MVC applications.
	/// </summary>
	public static partial class MvcAssert
	{
		/// <summary>
		/// Verifies that the specified authentication configuration has cookie authentication enabled.
		/// The assertion fails if cookie authentication has not been enabled.
		/// </summary>
		/// <param name="authenticationConfiguration">The authentication configuration.</param>
		/// <example>
		/// <code language="cs">
		/// MvcAssert.HasCookieAuthentication(new AuthenticationConfiguration());
		/// </code>
		/// </example>
		public static void HasCookieAuthentication(IAuthenticationConfiguration authenticationConfiguration)
		{
			if (!AuthenticationConfigurationHelpers.HasCookieAuthentication(authenticationConfiguration, DefaultAuthenticationTypes.ExternalCookie))
			{
				Assert.Fail("No cookie authentication configured for external cookie (named {0})", DefaultAuthenticationTypes.ExternalCookie);
			}
			if (!AuthenticationConfigurationHelpers.HasCookieAuthentication(authenticationConfiguration, DefaultAuthenticationTypes.ApplicationCookie, "/Account/Login"))
			{
				Assert.Fail("No cookie authentication configured for application cookie (named {0})", DefaultAuthenticationTypes.ApplicationCookie);
			}
		}

		/// <summary>
		/// Verifies that the specified authentication configuration has Facebook authentication enabled.
		/// The assertion fails if Facebook authentication has not been enabled.
		/// </summary>
		/// <param name="authenticationConfiguration">The authentication configuration.</param>
		/// <example>
		/// <code language="cs">
		/// MvcAssert.HasFacebookAuthentication(new AuthenticationConfiguration());
		/// </code>
		/// </example>
		public static void HasFacebookAuthentication(IAuthenticationConfiguration authenticationConfiguration)
		{
			if (!AuthenticationConfigurationHelpers.HasFacebookAuthentication(authenticationConfiguration))
			{
				Assert.Fail("No Facebook authentication configured");
			}
		}

		/// <summary>
		/// Verifies that the specified authentication configuration has Google authentication enabled.
		/// The assertion fails if Google authentication has not been enabled.
		/// </summary>
		/// <param name="authenticationConfiguration">The authentication configuration.</param>
		/// <example>
		/// <code language="cs">
		/// MvcAssert.HasGoogleAuthentication(new AuthenticationConfiguration());
		/// </code>
		/// </example>
		public static void HasGoogleAuthentication(IAuthenticationConfiguration authenticationConfiguration)
		{
			if (!AuthenticationConfigurationHelpers.HasGoogleAuthentication(authenticationConfiguration))
			{
				Assert.Fail("No Google authentication configured");
			}
		}

		/// <summary>
		/// Verifies that the specified authentication configuration has Microsoft authentication enabled.
		/// The assertion fails if Microsoft authentication has not been enabled.
		/// </summary>
		/// <param name="authenticationConfiguration">The authentication configuration.</param>
		/// <example>
		/// <code language="cs">
		/// MvcAssert.HasMicrosoftAuthentication(new AuthenticationConfiguration());
		/// </code>
		/// </example>
		public static void HasMicrosoftAuthentication(IAuthenticationConfiguration authenticationConfiguration)
		{
			if (!AuthenticationConfigurationHelpers.HasMicrosoftAuthentication(authenticationConfiguration))
			{
				Assert.Fail("No Microsoft authentication configured");
			}
		}

		/// <summary>
		/// Verifies that the specified authentication configuration has Twitter authentication enabled.
		/// The assertion fails if Twitter authentication has not been enabled.
		/// </summary>
		/// <param name="authenticationConfiguration">The authentication configuration.</param>
		/// <example>
		/// <code language="cs">
		/// MvcAssert.HasTwitterAuthentication(new AuthenticationConfiguration());
		/// </code>
		/// </example>
		public static void HasTwitterAuthentication(IAuthenticationConfiguration authenticationConfiguration)
		{
			if (!AuthenticationConfigurationHelpers.HasTwitterAuthentication(authenticationConfiguration))
			{
				Assert.Fail("No Twitter authentication configured");
			}
		}
	}
}
