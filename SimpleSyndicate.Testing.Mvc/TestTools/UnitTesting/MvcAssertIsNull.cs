﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SimpleSyndicate.Testing.Mvc.TestTools.UnitTesting
{
	/// <summary>
	/// Verifies conditions in unit tests using true/false propositions; this class provides all the functionality of <see cref="Assert"/>
	/// but with extra functionality specifically designed for MVC applications.
	/// </summary>
	public static partial class MvcAssert
	{
		/// <overloads>
		/// <summary>
		/// Verifies that a specified object is <c>null</c>.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Verifies that the specified object is <c>null</c>. The assertion fails if it is not <c>null</c>.
		/// </summary>
		/// <param name="value">The object to verify is <c>null</c>.</param>
		public static void IsNull(object value)
		{
			Assert.IsNull(value);
		}

		/// <summary>
		/// Verifies that the specified object is <c>null</c>. The assertion fails if it is not <c>null</c>. Displays a message if the assertion fails.
		/// </summary>
		/// <param name="value">The object to verify is <c>null</c>.</param>
		/// <param name="message">A message to display if the assertion fails. This message can be seen in the unit test results.</param>
		public static void IsNull(object value, string message)
		{
			Assert.IsNull(value, message);
		}

		/// <summary>
		/// Verifies that the specified object is <c>null</c>. The assertion fails if it is not <c>null</c>. Displays a message if the assertion fails, and applies the specified formatting to it.
		/// </summary>
		/// <param name="value">The object to verify is <c>null</c>.</param>
		/// <param name="message">A message to display if the assertion fails. This message can be seen in the unit test results.</param>
		/// <param name="parameters">An array of parameters to use when formatting <paramref name="message"/>.</param>
		public static void IsNull(object value, string message, params object[] parameters)
		{
			Assert.IsNull(value, message, parameters);
		}
	}
}
