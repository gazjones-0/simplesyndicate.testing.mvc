﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SimpleSyndicate.Testing.Mvc.TestTools.UnitTesting
{
	/// <summary>
	/// Verifies conditions in unit tests using true/false propositions; this class provides all the functionality of <see cref="Assert"/>
	/// but with extra functionality specifically designed for MVC applications.
	/// </summary>
	public static partial class MvcAssert
	{
		/// <overloads>
		/// <summary>
		/// Verifies that a specified condition is <c>true</c>.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Verifies that the specified condition is <c>true</c>. The assertion fails if the condition is <c>false</c>.
		/// </summary>
		/// <param name="condition">The condition to verify is <c>true</c>.</param>
		public static void IsTrue(bool condition)
		{
			Assert.IsTrue(condition);
		}

		/// <summary>
		/// Verifies that the specified condition is <c>true</c>. The assertion fails if the condition is <c>false</c>.
		/// </summary>
		/// <param name="condition">The condition to verify is <c>true</c>.</param>
		/// <param name="message">A message to display if the assertion fails. This message can be seen in the unit test results.</param>
		public static void IsTrue(bool condition, string message)
		{
			Assert.IsTrue(condition, message);
		}

		/// <summary>
		/// Verifies that the specified condition is <c>true</c>. The assertion fails if the condition is <c>false</c>.
		/// </summary>
		/// <param name="condition">The condition to verify is <c>true</c>.</param>
		/// <param name="message">A message to display if the assertion fails. This message can be seen in the unit test results.</param>
		/// <param name="parameters">An array of parameters to use when formatting <paramref name="message"/>.</param>
		public static void IsTrue(bool condition, string message, params object[] parameters)
		{
			Assert.IsTrue(condition, message, parameters);
		}
	}
}
