﻿using System;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SimpleSyndicate.Testing.Mvc.TestTools.UnitTesting
{
	/// <summary>
	/// Verifies conditions in unit tests using true/false propositions; this class provides all the functionality of <see cref="Assert"/>
	/// but with extra functionality specifically designed for MVC applications.
	/// </summary>
	public static partial class MvcAssert
	{
		/// <overloads>
		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="System.ComponentModel.DataAnnotations.CompareAttribute"/> with the specified other property to compare with.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different other property to compare with.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="System.ComponentModel.DataAnnotations.CompareAttribute"/> with the specified other property to compare with.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different other property to compare with.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="otherProperty">The property that the specified property is expected to compare with.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Compare("Password")]
		/// MvcAssert.IsCompared&lt;SomeViewModel&gt;("Password", x => x.ConfirmPassword);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsCompared<TModel>(string otherProperty, Expression<Func<TModel, bool>> expression)
		{
			IsComparedInternal<TModel, bool>(otherProperty, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="System.ComponentModel.DataAnnotations.CompareAttribute"/> with the specified other property to compare with.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different other property to compare with.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="otherProperty">The property that the specified property is expected to compare with.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Compare("Password")]
		/// MvcAssert.IsCompared&lt;SomeViewModel&gt;("Password", x => x.ConfirmPassword);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsCompared<TModel>(string otherProperty, Expression<Func<TModel, bool?>> expression)
		{
			IsComparedInternal<TModel, bool?>(otherProperty, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="System.ComponentModel.DataAnnotations.CompareAttribute"/> with the specified other property to compare with.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different other property to compare with.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="otherProperty">The property that the specified property is expected to compare with.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Compare("Password")]
		/// MvcAssert.IsCompared&lt;SomeViewModel&gt;("Password", x => x.ConfirmPassword);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsCompared<TModel>(string otherProperty, Expression<Func<TModel, byte>> expression)
		{
			IsComparedInternal<TModel, byte>(otherProperty, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="System.ComponentModel.DataAnnotations.CompareAttribute"/> with the specified other property to compare with.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different other property to compare with.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="otherProperty">The property that the specified property is expected to compare with.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Compare("Password")]
		/// MvcAssert.IsCompared&lt;SomeViewModel&gt;("Password", x => x.ConfirmPassword);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsCompared<TModel>(string otherProperty, Expression<Func<TModel, byte?>> expression)
		{
			IsComparedInternal<TModel, byte?>(otherProperty, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="System.ComponentModel.DataAnnotations.CompareAttribute"/> with the specified other property to compare with.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different other property to compare with.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="otherProperty">The property that the specified property is expected to compare with.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Compare("Password")]
		/// MvcAssert.IsCompared&lt;SomeViewModel&gt;("Password", x => x.ConfirmPassword);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsCompared<TModel>(string otherProperty, Expression<Func<TModel, char>> expression)
		{
			IsComparedInternal<TModel, char>(otherProperty, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="System.ComponentModel.DataAnnotations.CompareAttribute"/> with the specified other property to compare with.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different other property to compare with.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="otherProperty">The property that the specified property is expected to compare with.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Compare("Password")]
		/// MvcAssert.IsCompared&lt;SomeViewModel&gt;("Password", x => x.ConfirmPassword);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsCompared<TModel>(string otherProperty, Expression<Func<TModel, char?>> expression)
		{
			IsComparedInternal<TModel, char?>(otherProperty, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="System.ComponentModel.DataAnnotations.CompareAttribute"/> with the specified other property to compare with.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different other property to compare with.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="otherProperty">The property that the specified property is expected to compare with.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Compare("Password")]
		/// MvcAssert.IsCompared&lt;SomeViewModel&gt;("Password", x => x.ConfirmPassword);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsCompared<TModel>(string otherProperty, Expression<Func<TModel, DateTime>> expression)
		{
			IsComparedInternal<TModel, DateTime>(otherProperty, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="System.ComponentModel.DataAnnotations.CompareAttribute"/> with the specified other property to compare with.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different other property to compare with.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="otherProperty">The property that the specified property is expected to compare with.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Compare("Password")]
		/// MvcAssert.IsCompared&lt;SomeViewModel&gt;("Password", x => x.ConfirmPassword);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsCompared<TModel>(string otherProperty, Expression<Func<TModel, DateTime?>> expression)
		{
			IsComparedInternal<TModel, DateTime?>(otherProperty, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="System.ComponentModel.DataAnnotations.CompareAttribute"/> with the specified other property to compare with.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different other property to compare with.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="otherProperty">The property that the specified property is expected to compare with.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Compare("Password")]
		/// MvcAssert.IsCompared&lt;SomeViewModel&gt;("Password", x => x.ConfirmPassword);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsCompared<TModel>(string otherProperty, Expression<Func<TModel, decimal>> expression)
		{
			IsComparedInternal<TModel, decimal>(otherProperty, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="System.ComponentModel.DataAnnotations.CompareAttribute"/> with the specified other property to compare with.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different other property to compare with.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="otherProperty">The property that the specified property is expected to compare with.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Compare("Password")]
		/// MvcAssert.IsCompared&lt;SomeViewModel&gt;("Password", x => x.ConfirmPassword);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsCompared<TModel>(string otherProperty, Expression<Func<TModel, decimal?>> expression)
		{
			IsComparedInternal<TModel, decimal?>(otherProperty, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="System.ComponentModel.DataAnnotations.CompareAttribute"/> with the specified other property to compare with.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different other property to compare with.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="otherProperty">The property that the specified property is expected to compare with.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Compare("Password")]
		/// MvcAssert.IsCompared&lt;SomeViewModel&gt;("Password", x => x.ConfirmPassword);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsCompared<TModel>(string otherProperty, Expression<Func<TModel, double>> expression)
		{
			IsComparedInternal<TModel, double>(otherProperty, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="System.ComponentModel.DataAnnotations.CompareAttribute"/> with the specified other property to compare with.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different other property to compare with.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="otherProperty">The property that the specified property is expected to compare with.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Compare("Password")]
		/// MvcAssert.IsCompared&lt;SomeViewModel&gt;("Password", x => x.ConfirmPassword);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsCompared<TModel>(string otherProperty, Expression<Func<TModel, double?>> expression)
		{
			IsComparedInternal<TModel, double?>(otherProperty, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="System.ComponentModel.DataAnnotations.CompareAttribute"/> with the specified other property to compare with.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different other property to compare with.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="otherProperty">The property that the specified property is expected to compare with.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Compare("Password")]
		/// MvcAssert.IsCompared&lt;SomeViewModel&gt;("Password", x => x.ConfirmPassword);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsCompared<TModel>(string otherProperty, Expression<Func<TModel, float>> expression)
		{
			IsComparedInternal<TModel, float>(otherProperty, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="System.ComponentModel.DataAnnotations.CompareAttribute"/> with the specified other property to compare with.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different other property to compare with.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="otherProperty">The property that the specified property is expected to compare with.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Compare("Password")]
		/// MvcAssert.IsCompared&lt;SomeViewModel&gt;("Password", x => x.ConfirmPassword);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsCompared<TModel>(string otherProperty, Expression<Func<TModel, float?>> expression)
		{
			IsComparedInternal<TModel, float?>(otherProperty, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="System.ComponentModel.DataAnnotations.CompareAttribute"/> with the specified other property to compare with.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different other property to compare with.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="otherProperty">The property that the specified property is expected to compare with.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Compare("Password")]
		/// MvcAssert.IsCompared&lt;SomeViewModel&gt;("Password", x => x.ConfirmPassword);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsCompared<TModel>(string otherProperty, Expression<Func<TModel, int>> expression)
		{
			IsComparedInternal<TModel, int>(otherProperty, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="System.ComponentModel.DataAnnotations.CompareAttribute"/> with the specified other property to compare with.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different other property to compare with.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="otherProperty">The property that the specified property is expected to compare with.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Compare("Password")]
		/// MvcAssert.IsCompared&lt;SomeViewModel&gt;("Password", x => x.ConfirmPassword);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsCompared<TModel>(string otherProperty, Expression<Func<TModel, int?>> expression)
		{
			IsComparedInternal<TModel, int?>(otherProperty, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="System.ComponentModel.DataAnnotations.CompareAttribute"/> with the specified other property to compare with.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different other property to compare with.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="otherProperty">The property that the specified property is expected to compare with.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Compare("Password")]
		/// MvcAssert.IsCompared&lt;SomeViewModel&gt;("Password", x => x.ConfirmPassword);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsCompared<TModel>(string otherProperty, Expression<Func<TModel, long>> expression)
		{
			IsComparedInternal<TModel, long>(otherProperty, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="System.ComponentModel.DataAnnotations.CompareAttribute"/> with the specified other property to compare with.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different other property to compare with.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="otherProperty">The property that the specified property is expected to compare with.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Compare("Password")]
		/// MvcAssert.IsCompared&lt;SomeViewModel&gt;("Password", x => x.ConfirmPassword);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsCompared<TModel>(string otherProperty, Expression<Func<TModel, long?>> expression)
		{
			IsComparedInternal<TModel, long?>(otherProperty, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="System.ComponentModel.DataAnnotations.CompareAttribute"/> with the specified other property to compare with.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different other property to compare with.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="otherProperty">The property that the specified property is expected to compare with.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Compare("Password")]
		/// MvcAssert.IsCompared&lt;SomeViewModel&gt;("Password", x => x.ConfirmPassword);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsCompared<TModel>(string otherProperty, Expression<Func<TModel, sbyte>> expression)
		{
			IsComparedInternal<TModel, sbyte>(otherProperty, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="System.ComponentModel.DataAnnotations.CompareAttribute"/> with the specified other property to compare with.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different other property to compare with.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="otherProperty">The property that the specified property is expected to compare with.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Compare("Password")]
		/// MvcAssert.IsCompared&lt;SomeViewModel&gt;("Password", x => x.ConfirmPassword);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsCompared<TModel>(string otherProperty, Expression<Func<TModel, sbyte?>> expression)
		{
			IsComparedInternal<TModel, sbyte?>(otherProperty, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="System.ComponentModel.DataAnnotations.CompareAttribute"/> with the specified other property to compare with.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different other property to compare with.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="otherProperty">The property that the specified property is expected to compare with.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Compare("Password")]
		/// MvcAssert.IsCompared&lt;SomeViewModel&gt;("Password", x => x.ConfirmPassword);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsCompared<TModel>(string otherProperty, Expression<Func<TModel, short>> expression)
		{
			IsComparedInternal<TModel, short>(otherProperty, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="System.ComponentModel.DataAnnotations.CompareAttribute"/> with the specified other property to compare with.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different other property to compare with.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="otherProperty">The property that the specified property is expected to compare with.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Compare("Password")]
		/// MvcAssert.IsCompared&lt;SomeViewModel&gt;("Password", x => x.ConfirmPassword);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsCompared<TModel>(string otherProperty, Expression<Func<TModel, short?>> expression)
		{
			IsComparedInternal<TModel, short?>(otherProperty, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="System.ComponentModel.DataAnnotations.CompareAttribute"/> with the specified other property to compare with.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different other property to compare with.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="otherProperty">The property that the specified property is expected to compare with.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Compare("Password")]
		/// MvcAssert.IsCompared&lt;SomeViewModel&gt;("Password", x => x.ConfirmPassword);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsCompared<TModel>(string otherProperty, Expression<Func<TModel, string>> expression)
		{
			IsComparedInternal<TModel, string>(otherProperty, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="System.ComponentModel.DataAnnotations.CompareAttribute"/> with the specified other property to compare with.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different other property to compare with.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="otherProperty">The property that the specified property is expected to compare with.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Compare("Password")]
		/// MvcAssert.IsCompared&lt;SomeViewModel&gt;("Password", x => x.ConfirmPassword);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsCompared<TModel>(string otherProperty, Expression<Func<TModel, uint>> expression)
		{
			IsComparedInternal<TModel, uint>(otherProperty, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="System.ComponentModel.DataAnnotations.CompareAttribute"/> with the specified other property to compare with.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different other property to compare with.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="otherProperty">The property that the specified property is expected to compare with.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Compare("Password")]
		/// MvcAssert.IsCompared&lt;SomeViewModel&gt;("Password", x => x.ConfirmPassword);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsCompared<TModel>(string otherProperty, Expression<Func<TModel, uint?>> expression)
		{
			IsComparedInternal<TModel, uint?>(otherProperty, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="System.ComponentModel.DataAnnotations.CompareAttribute"/> with the specified other property to compare with.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different other property to compare with.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="otherProperty">The property that the specified property is expected to compare with.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Compare("Password")]
		/// MvcAssert.IsCompared&lt;SomeViewModel&gt;("Password", x => x.ConfirmPassword);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsCompared<TModel>(string otherProperty, Expression<Func<TModel, ulong>> expression)
		{
			IsComparedInternal<TModel, ulong>(otherProperty, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="System.ComponentModel.DataAnnotations.CompareAttribute"/> with the specified other property to compare with.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different other property to compare with.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="otherProperty">The property that the specified property is expected to compare with.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Compare("Password")]
		/// MvcAssert.IsCompared&lt;SomeViewModel&gt;("Password", x => x.ConfirmPassword);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsCompared<TModel>(string otherProperty, Expression<Func<TModel, ulong?>> expression)
		{
			IsComparedInternal<TModel, ulong?>(otherProperty, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="System.ComponentModel.DataAnnotations.CompareAttribute"/> with the specified other property to compare with.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different other property to compare with.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="otherProperty">The property that the specified property is expected to compare with.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Compare("Password")]
		/// MvcAssert.IsCompared&lt;SomeViewModel&gt;("Password", x => x.ConfirmPassword);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsCompared<TModel>(string otherProperty, Expression<Func<TModel, ushort>> expression)
		{
			IsComparedInternal<TModel, ushort>(otherProperty, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="System.ComponentModel.DataAnnotations.CompareAttribute"/> with the specified other property to compare with.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different other property to compare with.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="otherProperty">The property that the specified property is expected to compare with.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Compare("Password")]
		/// MvcAssert.IsCompared&lt;SomeViewModel&gt;("Password", x => x.ConfirmPassword);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsCompared<TModel>(string otherProperty, Expression<Func<TModel, ushort?>> expression)
		{
			IsComparedInternal<TModel, ushort?>(otherProperty, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="System.ComponentModel.DataAnnotations.CompareAttribute"/> with the specified other property to compare with.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different other property to compare with.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <typeparam name="TType">The property type.</typeparam>
		/// <param name="otherProperty">The property that the specified property is expected to compare with.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		private static void IsComparedInternal<TModel, TType>(string otherProperty, Expression<Func<TModel, TType>> expression)
		{
			var attribute = AttributeHelpers.GetAttribute(expression, typeof(System.ComponentModel.DataAnnotations.CompareAttribute));
			if (attribute == null)
			{
				Assert.Fail("No Compare attribute found (exepcted it to be comparing with {0})", otherProperty);
			}
			var actualProperty = ((System.ComponentModel.DataAnnotations.CompareAttribute)attribute).OtherProperty;
			Assert.AreEqual(otherProperty, actualProperty);
		}
	}
}
