﻿using System;
using System.Linq.Expressions;
using System.Net.Http;
using System.Web.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SimpleSyndicate.Testing.Mvc.TestTools.UnitTesting
{
	/// <summary>
	/// Verifies conditions in unit tests using true/false propositions; this class provides all the functionality of <see cref="Assert"/>
	/// but with extra functionality specifically designed for MVC applications.
	/// </summary>
	public static partial class MvcAssert
	{
		/// <overloads>
		/// <summary>
		/// Verifies that a controller method has been decorated with a <see cref="RouteAttribute"/>.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Verifies that the specified controller method has been decorated with a <see cref="RouteAttribute"/> and it has a name.
		/// The assertion fails if the specified method does not have the attribute, or the attribute is present but does not have a name specified.
		/// </summary>
		/// <typeparam name="TController">The controller type.</typeparam>
		/// <param name="expression">The method that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the method isn't called, it's just examined to see if it has [Route]
		/// MvcAssert.HasRoute&lt;SomeController&gt;(x => x.SomeMethod());
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasRoute<TController>(Expression<Func<TController, HttpResponseMessage>> expression)
		{
			HasRouteInternal<TController, HttpResponseMessage>(expression);
		}

		/// <summary>
		/// Verifies that the specified controller method has been decorated with a <see cref="HttpGetAttribute"/> with the specified name.
		/// The assertion fails if the specified method does not have the attribute, or the attribute is present but specifies a different name.
		/// </summary>
		/// <typeparam name="TController">The controller type.</typeparam>
		/// <param name="expectedName">The expected route name.</param>
		/// <param name="expression">The method that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the method isn't called, it's just examined to see if it has [Route]
		/// MvcAssert.HasRoute&lt;SomeController&gt;("/some/route", x => x.SomeMethod());
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasRoute<TController>(string expectedName, Expression<Func<TController, HttpResponseMessage>> expression)
		{
			HasRouteInternal<TController, HttpResponseMessage>(expectedName, expression);
		}

		/// <summary>
		/// Verifies that the specified controller method has been decorated with a <see cref="RouteAttribute"/> and it has a name.
		/// The assertion fails if the specified method does not have the attribute, or the attribute is present but does not have a name specified.
		/// </summary>
		/// <typeparam name="TController">The controller type.</typeparam>
		/// <typeparam name="TReturnType">The method's return type.</typeparam>
		/// <param name="expression">The method that is expected to be decorated with the attribute.</param>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		private static void HasRouteInternal<TController, TReturnType>(Expression<Func<TController, TReturnType>> expression)
		{
			var attribute = AttributeHelpers.GetAttribute(expression, typeof(RouteAttribute));
			if (attribute == null)
			{
				Assert.Fail("No Http.Route attribute found");
			}
			var actualName = ((RouteAttribute)attribute).Name;
			if (String.IsNullOrEmpty(actualName))
			{
				Assert.Fail("No name found on Http.Route attribute (Name returned empty string)");
			}
			if (String.IsNullOrWhiteSpace(actualName))
			{
				Assert.Fail("No name found on Http.Route attribute (Name returned string consisting only of white-space characters)");
			}
		}

		/// <summary>
		/// Verifies that the specified controller method has been decorated with a <see cref="HttpGetAttribute"/> with the specified name.
		/// The assertion fails if the specified method does not have the attribute, or the attribute is present but specifies a different name.
		/// </summary>
		/// <typeparam name="TController">The controller type.</typeparam>
		/// <typeparam name="TReturnType">The method's return type.</typeparam>
		/// <param name="expectedName">The expected route name.</param>
		/// <param name="expression">The method that is expected to be decorated with the attribute.</param>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		private static void HasRouteInternal<TController, TReturnType>(string expectedName, Expression<Func<TController, TReturnType>> expression)
		{
			var attribute = AttributeHelpers.GetAttribute(expression, typeof(RouteAttribute));
			if (attribute == null)
			{
				Assert.Fail("No Http.Route attribute found");
			}
			var actualName = ((RouteAttribute)attribute).Name;
			Assert.AreEqual(expectedName, actualName);
		}
	}
}
