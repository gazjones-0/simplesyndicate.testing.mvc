﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SimpleSyndicate.Testing.Mvc.TestTools.UnitTesting
{
	/// <summary>
	/// Verifies conditions in unit tests using true/false propositions; this class provides all the functionality of <see cref="Assert"/>
	/// but with extra functionality specifically designed for MVC applications.
	/// </summary>
	public static partial class MvcAssert
	{
		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RegularExpressionAttribute"/>.
		/// The assertion fails if the specified model property does not have the attribute.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [RegularExpression]
		/// MvcAssert.RegularExpressionIsValidated&lt;SomeInputModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void RegularExpressionIsValidated<TModel>(Expression<Func<TModel, string>> expression)
		{
			RegularExpressionIsValidatedInternal<TModel, string>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RegularExpressionAttribute"/>.
		/// The assertion fails if the specified model property does not have the attribute.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <typeparam name="TType">The property type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		private static void RegularExpressionIsValidatedInternal<TModel, TType>(Expression<Func<TModel, TType>> expression)
		{
			if (!AttributeHelpers.HasAttribute(expression, typeof(RegularExpressionAttribute)))
			{
				Assert.Fail("No RegularExpression attribute found");
			}
		}
	}
}
