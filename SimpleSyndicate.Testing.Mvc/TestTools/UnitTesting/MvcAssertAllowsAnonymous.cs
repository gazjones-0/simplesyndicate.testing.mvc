﻿using System;
using System.Linq.Expressions;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SimpleSyndicate.Testing.Mvc.TestTools.UnitTesting
{
	/// <summary>
	/// Verifies conditions in unit tests using true/false propositions; this class provides all the functionality of <see cref="Assert"/>
	/// but with extra functionality specifically designed for MVC applications.
	/// </summary>
	public static partial class MvcAssert
	{
		/// <overloads>
		/// <summary>
		/// Verifies that the specified controller method has been decorated with an <see cref="AllowAnonymousAttribute"/>.
		/// The assertion fails if the specified method does not have the attribute.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Verifies that the specified controller method has been decorated with an <see cref="AllowAnonymousAttribute"/>.
		/// The assertion fails if the specified method does not have the attribute.
		/// </summary>
		/// <typeparam name="TController">The controller type.</typeparam>
		/// <param name="expression">The method that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the method isn't called, it's just examined to see if it has [AllowAnonymous]
		/// MvcAssert.AllowsAnonymous&lt;SomeController&gt;(x => x.SomeMethod());
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void AllowsAnonymous<TController>(Expression<Func<TController, ActionResult>> expression)
		{
			AllowsAnonymousInternal<TController, ActionResult>(expression);
		}

		/// <summary>
		/// Verifies that the specified controller method has been decorated with an <see cref="AllowAnonymousAttribute"/>.
		/// The assertion fails if the specified method does not have the attribute.
		/// </summary>
		/// <typeparam name="TController">The controller type.</typeparam>
		/// <param name="expression">The method that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the method isn't called, it's just examined to see if it has [AllowAnonymous]
		/// MvcAssert.AllowsAnonymous&lt;SomeController&gt;(x => x.SomeMethod());
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void AllowsAnonymous<TController>(Expression<Func<TController, JsonResult>> expression)
		{
			AllowsAnonymousInternal<TController, JsonResult>(expression);
		}

		/// <summary>
		/// Verifies that the specified controller method has been decorated with an <see cref="AllowAnonymousAttribute"/>.
		/// The assertion fails if the specified method does not have the attribute.
		/// </summary>
		/// <typeparam name="TController">The controller type.</typeparam>
		/// <param name="expression">The method that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the method isn't called, it's just examined to see if it has [AllowAnonymous]
		/// MvcAssert.AllowsAnonymous&lt;SomeController&gt;(x => x.SomeMethod());
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void AllowsAnonymous<TController>(Expression<Func<TController, ViewResult>> expression)
		{
			AllowsAnonymousInternal<TController, ViewResult>(expression);
		}

		/// <summary>
		/// Verifies that the specified controller method has been decorated with an <see cref="AllowAnonymousAttribute"/>.
		/// The assertion fails if the specified method does not have the attribute.
		/// </summary>
		/// <typeparam name="TController">The controller type.</typeparam>
		/// <typeparam name="TReturnType">The method's return type.</typeparam>
		/// <param name="expression">The method that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the method isn't called, it's just examined to see if it has [AllowAnonymous]
		/// MvcAssert.ActionNameIs&lt;SomeController&gt;(x => x.SomeMethod());
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		private static void AllowsAnonymousInternal<TController, TReturnType>(Expression<Func<TController, TReturnType>> expression)
		{
			if (!AttributeHelpers.HasAttribute(expression, typeof(AllowAnonymousAttribute)))
			{
				Assert.Fail("No AllowAnonymous attribute found");
			}
		}
	}
}
