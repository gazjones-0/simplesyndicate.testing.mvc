﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SimpleSyndicate.Testing.Mvc.TestTools.UnitTesting
{
	/// <summary>
	/// Verifies conditions in unit tests using true/false propositions; this class provides all the functionality of <see cref="Assert"/>
	/// but with extra functionality specifically designed for MVC applications.
	/// </summary>
	public static partial class MvcAssert
	{
		/// <overloads>
		/// <summary>
		/// Verifies that a specified object is not an instance of a specified type.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Verifies that the specified object is not an instance of the specified type. The assertion fails if the type is found in the inheritance hierarchy of the object.
		/// </summary>
		/// <param name="value">The object to verify is not of <paramref name="wrongType"/>.</param>
		/// <param name="wrongType">The type that should not be found in the inheritance hierarchy of <paramref name="value"/>.</param>
		public static void IsNotInstanceOfType(object value, Type wrongType)
		{
			Assert.IsNotInstanceOfType(value, wrongType);
		}

		/// <summary>
		/// Verifies that the specified object is not an instance of the specified type. The assertion fails if the type is found in the inheritance hierarchy of the object. Displays a message if the assertion fails.
		/// </summary>
		/// <param name="value">The object to verify is not of <paramref name="wrongType"/>.</param>
		/// <param name="wrongType">The type that should not be found in the inheritance hierarchy of <paramref name="value"/>.</param>
		/// <param name="message">A message to display if the assertion fails. This message can be seen in the unit test results.</param>
		public static void IsNotInstanceOfType(object value, Type wrongType, string message)
		{
			Assert.IsNotInstanceOfType(value, wrongType, message);
		}

		/// <summary>
		/// Verifies that the specified object is not an instance of the specified type. The assertion fails if the type is found in the inheritance hierarchy of the object. Displays a message if the assertion fails, and applies the specified formatting to it.
		/// </summary>
		/// <param name="value">The object to verify is not of <paramref name="wrongType"/>.</param>
		/// <param name="wrongType">The type that should not be found in the inheritance hierarchy of <paramref name="value"/>.</param>
		/// <param name="message">A message to display if the assertion fails. This message can be seen in the unit test results.</param>
		/// <param name="parameters">An array of parameters to use when formatting <paramref name="message"/>.</param>
		public static void IsNotInstanceOfType(object value, Type wrongType, string message, params object[] parameters)
		{
			Assert.IsNotInstanceOfType(value, wrongType, message, parameters);
		}
	}
}
