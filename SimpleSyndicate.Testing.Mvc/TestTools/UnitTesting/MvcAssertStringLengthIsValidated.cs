﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SimpleSyndicate.Testing.Mvc.TestTools.UnitTesting
{
	/// <summary>
	/// Verifies conditions in unit tests using true/false propositions; this class provides all the functionality of <see cref="Assert"/>
	/// but with extra functionality specifically designed for MVC applications.
	/// </summary>
	public static partial class MvcAssert
	{
		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="StringLengthAttribute"/> with the specified minimum and
		/// maximum length. The assertion fails if the specified model property does not have the attribute, or the model property has the attribute
		/// but a different minimum or maximum length.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedMinimumLength">Expected minimum length.</param>
		/// <param name="expectedMaximumLength">Expected maximum length.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [StringLength]
		/// MvcAssert.StringLengthIsValidated&lt;SomeInputModel&gt;(1, 4, x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void StringLengthIsValidated<TModel>(int expectedMinimumLength, int expectedMaximumLength, Expression<Func<TModel, string>> expression)
		{
			StringLengthIsValidatedInternal<TModel, string>(expectedMinimumLength, expectedMaximumLength, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="StringLengthAttribute"/> with the specified minimum and
		/// maximum length. The assertion fails if the specified model property does not have the attribute, or the model property has the attribute
		/// but a different minimum or maximum length.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <typeparam name="TType">The property type.</typeparam>
		/// <param name="expectedMinimumLength">Expected minimum length.</param>
		/// <param name="expectedMaximumLength">Expected maximum length.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		private static void StringLengthIsValidatedInternal<TModel, TType>(int expectedMinimumLength, int expectedMaximumLength, Expression<Func<TModel, TType>> expression)
		{
			var attribute = AttributeHelpers.GetAttribute(expression, typeof(StringLengthAttribute));
			if (attribute == null)
			{
				Assert.Fail("No StringLength attribute found");
			}
			var stringLengthAttribute = (StringLengthAttribute)attribute;
			var actualMinimumLength = stringLengthAttribute.MinimumLength;
			var actualMaximumLength = stringLengthAttribute.MaximumLength;
			Assert.AreEqual(expectedMinimumLength, actualMinimumLength, "MinimumLength did not match expected value");
			Assert.AreEqual(expectedMaximumLength, actualMaximumLength, "MaximumLength did not match expected value");
		}
	}
}
