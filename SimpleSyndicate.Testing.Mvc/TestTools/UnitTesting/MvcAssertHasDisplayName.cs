﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using System.Web.Mvc.Html;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSyndicate.Testing.Mvc.Web.Mvc;

namespace SimpleSyndicate.Testing.Mvc.TestTools.UnitTesting
{
	/// <summary>
	/// Verifies conditions in unit tests using true/false propositions; this class provides all the functionality of <see cref="Assert"/>
	/// but with extra functionality specifically designed for MVC applications.
	/// </summary>
	public static partial class MvcAssert
	{
		/// <overloads>
		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> and it has a name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a name specified.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> and it has a name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a name specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "xxx")]
		/// MvcAssert.HasDisplayName&lt;SomeViewModel&gt;(x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayName<TModel>(Expression<Func<TModel, bool>> expression)
		{
			HasDisplayNameInternal<TModel, bool>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> and it has a name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a name specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "xxx")]
		/// MvcAssert.HasDisplayName&lt;SomeViewModel&gt;(x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayName<TModel>(Expression<Func<TModel, bool?>> expression)
		{
			HasDisplayNameInternal<TModel, bool?>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> and it has a name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a name specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "xxx")]
		/// MvcAssert.HasDisplayName&lt;SomeViewModel&gt;(x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayName<TModel>(Expression<Func<TModel, byte>> expression)
		{
			HasDisplayNameInternal<TModel, byte>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> and it has a name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a name specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "xxx")]
		/// MvcAssert.HasDisplayName&lt;SomeViewModel&gt;(x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayName<TModel>(Expression<Func<TModel, byte?>> expression)
		{
			HasDisplayNameInternal<TModel, byte?>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> and it has a name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a name specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "xxx")]
		/// MvcAssert.HasDisplayName&lt;SomeViewModel&gt;(x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayName<TModel>(Expression<Func<TModel, char>> expression)
		{
			HasDisplayNameInternal<TModel, char>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> and it has a name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a name specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "xxx")]
		/// MvcAssert.HasDisplayName&lt;SomeViewModel&gt;(x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayName<TModel>(Expression<Func<TModel, char?>> expression)
		{
			HasDisplayNameInternal<TModel, char?>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> and it has a name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a name specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "xxx")]
		/// MvcAssert.HasDisplayName&lt;SomeViewModel&gt;(x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayName<TModel>(Expression<Func<TModel, DateTime>> expression)
		{
			HasDisplayNameInternal<TModel, DateTime>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> and it has a name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a name specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "xxx")]
		/// MvcAssert.HasDisplayName&lt;SomeViewModel&gt;(x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayName<TModel>(Expression<Func<TModel, DateTime?>> expression)
		{
			HasDisplayNameInternal<TModel, DateTime?>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> and it has a name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a name specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "xxx")]
		/// MvcAssert.HasDisplayName&lt;SomeViewModel&gt;(x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayName<TModel>(Expression<Func<TModel, decimal>> expression)
		{
			HasDisplayNameInternal<TModel, decimal>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> and it has a name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a name specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "xxx")]
		/// MvcAssert.HasDisplayName&lt;SomeViewModel&gt;(x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayName<TModel>(Expression<Func<TModel, decimal?>> expression)
		{
			HasDisplayNameInternal<TModel, decimal?>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> and it has a name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a name specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "xxx")]
		/// MvcAssert.HasDisplayName&lt;SomeViewModel&gt;(x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayName<TModel>(Expression<Func<TModel, double>> expression)
		{
			HasDisplayNameInternal<TModel, double>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> and it has a name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a name specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "xxx")]
		/// MvcAssert.HasDisplayName&lt;SomeViewModel&gt;(x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayName<TModel>(Expression<Func<TModel, double?>> expression)
		{
			HasDisplayNameInternal<TModel, double?>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> and it has a name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a name specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "xxx")]
		/// MvcAssert.HasDisplayName&lt;SomeViewModel&gt;(x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayName<TModel>(Expression<Func<TModel, float>> expression)
		{
			HasDisplayNameInternal<TModel, float>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> and it has a name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a name specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "xxx")]
		/// MvcAssert.HasDisplayName&lt;SomeViewModel&gt;(x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayName<TModel>(Expression<Func<TModel, float?>> expression)
		{
			HasDisplayNameInternal<TModel, float?>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> and it has a name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a name specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "xxx")]
		/// MvcAssert.HasDisplayName&lt;SomeViewModel&gt;(x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayName<TModel>(Expression<Func<TModel, int>> expression)
		{
			HasDisplayNameInternal<TModel, int>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> and it has a name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a name specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "xxx")]
		/// MvcAssert.HasDisplayName&lt;SomeViewModel&gt;(x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayName<TModel>(Expression<Func<TModel, int?>> expression)
		{
			HasDisplayNameInternal<TModel, int?>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> and it has a name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a name specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "xxx")]
		/// MvcAssert.HasDisplayName&lt;SomeViewModel&gt;(x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayName<TModel>(Expression<Func<TModel, long>> expression)
		{
			HasDisplayNameInternal<TModel, long>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> and it has a name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a name specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "xxx")]
		/// MvcAssert.HasDisplayName&lt;SomeViewModel&gt;(x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayName<TModel>(Expression<Func<TModel, long?>> expression)
		{
			HasDisplayNameInternal<TModel, long?>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> and it has a name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a name specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "xxx")]
		/// MvcAssert.HasDisplayName&lt;SomeViewModel&gt;(x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayName<TModel>(Expression<Func<TModel, sbyte>> expression)
		{
			HasDisplayNameInternal<TModel, sbyte>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> and it has a name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a name specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "xxx")]
		/// MvcAssert.HasDisplayName&lt;SomeViewModel&gt;(x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayName<TModel>(Expression<Func<TModel, sbyte?>> expression)
		{
			HasDisplayNameInternal<TModel, sbyte?>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> and it has a name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a name specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "xxx")]
		/// MvcAssert.HasDisplayName&lt;SomeViewModel&gt;(x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayName<TModel>(Expression<Func<TModel, short>> expression)
		{
			HasDisplayNameInternal<TModel, short>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> and it has a name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a name specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "xxx")]
		/// MvcAssert.HasDisplayName&lt;SomeViewModel&gt;(x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayName<TModel>(Expression<Func<TModel, short?>> expression)
		{
			HasDisplayNameInternal<TModel, short?>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> and it has a name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a name specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "xxx")]
		/// MvcAssert.HasDisplayName&lt;SomeViewModel&gt;(x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayName<TModel>(Expression<Func<TModel, string>> expression)
		{
			HasDisplayNameInternal<TModel, string>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> and it has a name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a name specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "xxx")]
		/// MvcAssert.HasDisplayName&lt;SomeViewModel&gt;(x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayName<TModel>(Expression<Func<TModel, uint>> expression)
		{
			HasDisplayNameInternal<TModel, uint>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> and it has a name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a name specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "xxx")]
		/// MvcAssert.HasDisplayName&lt;SomeViewModel&gt;(x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayName<TModel>(Expression<Func<TModel, uint?>> expression)
		{
			HasDisplayNameInternal<TModel, uint?>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> and it has a name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a name specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "xxx")]
		/// MvcAssert.HasDisplayName&lt;SomeViewModel&gt;(x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayName<TModel>(Expression<Func<TModel, ulong>> expression)
		{
			HasDisplayNameInternal<TModel, ulong>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> and it has a name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a name specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "xxx")]
		/// MvcAssert.HasDisplayName&lt;SomeViewModel&gt;(x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayName<TModel>(Expression<Func<TModel, ulong?>> expression)
		{
			HasDisplayNameInternal<TModel, ulong?>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> and it has a name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a name specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "xxx")]
		/// MvcAssert.HasDisplayName&lt;SomeViewModel&gt;(x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayName<TModel>(Expression<Func<TModel, ushort>> expression)
		{
			HasDisplayNameInternal<TModel, ushort>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> and it has a name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a name specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Display(Name = "xxx")]
		/// MvcAssert.HasDisplayName&lt;SomeViewModel&gt;(x => x.UserName);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayName<TModel>(Expression<Func<TModel, ushort?>> expression)
		{
			HasDisplayNameInternal<TModel, ushort?>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayAttribute"/> and it has a name.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a name specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <typeparam name="TType">The property type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		private static void HasDisplayNameInternal<TModel, TType>(Expression<Func<TModel, TType>> expression)
		{
			var htmlhelper = HtmlHelperFactory.Create<TModel>();
			var value = htmlhelper.DisplayNameFor(expression);
			if (value == null)
			{
				Assert.Fail("No Display attribute found (DisplayNameFor returned null string)");
			}
			if (String.IsNullOrEmpty(value.ToString()))
			{
				Assert.Fail("No name found on Display attribute (DisplayNameFor returned empty string)");
			}
			if (String.IsNullOrWhiteSpace(value.ToString()))
			{
				Assert.Fail("No name found on Display attribute (DisplayNameFor returned string consisting only of white-space characters)");
			}
		}
	}
}
