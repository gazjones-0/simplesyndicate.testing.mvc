﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSyndicate.Mvc.Controllers;

namespace SimpleSyndicate.Testing.Mvc.TestTools.UnitTesting
{
	/// <summary>
	/// Verifies conditions in unit tests using true/false propositions; this class provides all the functionality of <see cref="Assert"/>
	/// but with extra functionality specifically designed for MVC applications.
	/// </summary>
	public static partial class MvcAssert
	{
		/// <overloads>
		/// <summary>
		/// Verifies that specified values are equal.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Verifies that two specified objects are equal. The assertion fails if the objects are not equal.
		/// </summary>
		/// <param name="expected">The first object to compare. This is the object the unit test expects.</param>
		/// <param name="actual">The second object to compare. This is the object the unit test produced.</param>
		public static void AreEqual(object expected, object actual)
		{
			Assert.AreEqual(expected, actual);
		}

		/// <summary>
		/// Verifies that two specified doubles are equal, or within the specified accuracy of each other. The assertion fails if they are not within the specified accuracy of each other.
		/// </summary>
		/// <param name="expected">The first double to compare. This is the double the unit test expects.</param>
		/// <param name="actual">The second double to compare. This is the double the unit test produced.</param>
		/// <param name="delta">The required accuracy. The assertion will fail only if <paramref name="expected"/> is different from <paramref name="actual"/> by more than <paramref name="delta"/>.</param>
		public static void AreEqual(double expected, double actual, double delta)
		{
			Assert.AreEqual(expected, actual, delta);
		}

		/// <summary>
		/// Verifies that two specified objects are equal. The assertion fails if the objects are not equal. Displays a message if the assertion fails.
		/// </summary>
		/// <param name="expected">The first object to compare. This is the object the unit test expects.</param>
		/// <param name="actual">The second object to compare. This is the object the unit test produced.</param>
		/// <param name="message">A message to display if the assertion fails. This message can be seen in the unit test results.</param>
		public static void AreEqual(object expected, object actual, string message)
		{
			Assert.AreEqual(expected, actual, message);
		}

		/// <summary>
		/// Verifies that two specified floats are equal, or within the specified accuracy of each other. The assertion fails if they are not within the specified accuracy of each other.
		/// </summary>
		/// <param name="expected">The first float to compare. This is the double the unit test expects.</param>
		/// <param name="actual">The second float to compare. This is the double the unit test produced.</param>
		/// <param name="delta">The required accuracy. The assertion will fail only if <paramref name="expected"/> is different from <paramref name="actual"/> by more than <paramref name="delta"/>.</param>
		public static void AreEqual(float expected, float actual, float delta)
		{
			Assert.AreEqual(expected, actual, delta);
		}

		/// <summary>
		/// Verifies that two specified strings are equal, ignoring case or not as specified. The assertion fails if they are not equal.
		/// </summary>
		/// <param name="expected">The first string to compare. This is the string the unit test expects.</param>
		/// <param name="actual">The second string to compare. This is the string the unit test produced.</param>
		/// <param name="ignoreCase">A Boolean value that indicates a case-sensitive or insensitive comparison. <c>true</c> indicates a case-insensitive comparison.</param>
		public static void AreEqual(string expected, string actual, bool ignoreCase)
		{
			Assert.AreEqual(expected, actual, ignoreCase, CultureInfo.InvariantCulture);
		}

		/// <summary>
		/// Verifies that two specified doubles are equal, or within the specified accuracy of each other. The assertion fails if they are not within the specified accuracy of each other. Displays a message if the assertion fails.
		/// </summary>
		/// <param name="expected">The first double to compare. This is the double the unit test expects.</param>
		/// <param name="actual">The second double to compare. This is the double the unit test produced.</param>
		/// <param name="delta">The required accuracy. The assertion will fail only if <paramref name="expected"/> is different from <paramref name="actual"/> by more than <paramref name="delta"/>.</param>
		/// <param name="message">A message to display if the assertion fails. This message can be seen in the unit test results.</param>
		public static void AreEqual(double expected, double actual, double delta, string message)
		{
			Assert.AreEqual(expected, actual, delta, message);
		}

		/// <summary>
		/// Verifies that two specified objects are equal. The assertion fails if the objects are not equal. Displays a message if the assertion fails, and applies the specified formatting to it.
		/// </summary>
		/// <param name="expected">The first object to compare. This is the object the unit test expects.</param>
		/// <param name="actual">The second object to compare. This is the object the unit test produced.</param>
		/// <param name="message">A message to display if the assertion fails. This message can be seen in the unit test results.</param>
		/// <param name="parameters">An array of parameters to use when formatting <paramref name="message"/>.</param>
		public static void AreEqual(object expected, object actual, string message, params object[] parameters)
		{
			Assert.AreEqual(expected, actual, message, parameters);
		}

		/// <summary>
		/// Verifies that two specified floats are equal, or within the specified accuracy of each other. The assertion fails if they are not within the specified accuracy of each other. Displays a message if the assertion fails.
		/// </summary>
		/// <param name="expected">The first float to compare. This is the double the unit test expects.</param>
		/// <param name="actual">The second float to compare. This is the double the unit test produced.</param>
		/// <param name="delta">The required accuracy. The assertion will fail only if <paramref name="expected"/> is different from <paramref name="actual"/> by more than <paramref name="delta"/>.</param>
		/// <param name="message">A message to display if the assertion fails. This message can be seen in the unit test results.</param>
		public static void AreEqual(float expected, float actual, float delta, string message)
		{
			Assert.AreEqual(expected, actual, delta, message);
		}

		/// <summary>
		/// Verifies that two specified strings are equal, ignoring case or not as specified, and using the culture info specified. The assertion fails if they are not equal.
		/// </summary>
		/// <param name="expected">The first string to compare. This is the string the unit test expects.</param>
		/// <param name="actual">The second string to compare. This is the string the unit test produced.</param>
		/// <param name="ignoreCase">A Boolean value that indicates a case-sensitive or insensitive comparison. <c>true</c> indicates a case-insensitive comparison.</param>
		/// <param name="cultureInfo">A <see cref="CultureInfo"/> object that supplies culture-specific comparison information.</param>
		public static void AreEqual(string expected, string actual, bool ignoreCase, CultureInfo cultureInfo)
		{
			Assert.AreEqual(expected, actual, ignoreCase, cultureInfo);
		}

		/// <summary>
		/// Verifies that two specified strings are equal, ignoring case or not as specified. The assertion fails if they are not equal. Displays a message if the assertion fails.
		/// </summary>
		/// <param name="expected">The first string to compare. This is the string the unit test expects.</param>
		/// <param name="actual">The second string to compare. This is the string the unit test produced.</param>
		/// <param name="ignoreCase">A Boolean value that indicates a case-sensitive or insensitive comparison. <c>true</c> indicates a case-insensitive comparison.</param>
		/// <param name="message">A message to display if the assertion fails. This message can be seen in the unit test results.</param>
		public static void AreEqual(string expected, string actual, bool ignoreCase, string message)
		{
			Assert.AreEqual(expected, actual, ignoreCase, message);
		}

		/// <summary>
		/// Verifies that two specified doubles are equal, or within the specified accuracy of each other. The assertion fails if they are not within the specified accuracy of each other. Displays a message if the assertion fails, and applies the specified formatting to it.
		/// </summary>
		/// <param name="expected">The first double to compare. This is the double the unit test expects.</param>
		/// <param name="actual">The second double to compare. This is the double the unit test produced.</param>
		/// <param name="delta">The required accuracy. The assertion will fail only if <paramref name="expected"/> is different from <paramref name="actual"/> by more than <paramref name="delta"/>.</param>
		/// <param name="message">A message to display if the assertion fails. This message can be seen in the unit test results.</param>
		/// <param name="parameters">An array of parameters to use when formatting <paramref name="message"/>.</param>
		public static void AreEqual(double expected, double actual, double delta, string message, params object[] parameters)
		{
			Assert.AreEqual(expected, actual, delta, message, parameters);
		}

		/// <summary>
		/// Verifies that two specified floats are equal, or within the specified accuracy of each other. The assertion fails if they are not within the specified accuracy of each other. Displays a message if the assertion fails, and applies the specified formatting to it.
		/// </summary>
		/// <param name="expected">The first float to compare. This is the double the unit test expects.</param>
		/// <param name="actual">The second float to compare. This is the double the unit test produced.</param>
		/// <param name="delta">The required accuracy. The assertion will fail only if <paramref name="expected"/> is different from <paramref name="actual"/> by more than <paramref name="delta"/>.</param>
		/// <param name="message">A message to display if the assertion fails. This message can be seen in the unit test results.</param>
		/// <param name="parameters">An array of parameters to use when formatting <paramref name="message"/>.</param>
		public static void AreEqual(float expected, float actual, float delta, string message, params object[] parameters)
		{
			Assert.AreEqual(expected, actual, delta, message, parameters);
		}

		/// <summary>
		/// Verifies that two specified strings are equal, ignoring case or not as specified, and using the culture info specified. The assertion fails if they are not equal. Displays a message if the assertion fails.
		/// </summary>
		/// <param name="expected">The first string to compare. This is the string the unit test expects.</param>
		/// <param name="actual">The second string to compare. This is the string the unit test produced.</param>
		/// <param name="ignoreCase">A Boolean value that indicates a case-sensitive or insensitive comparison. <c>true</c> indicates a case-insensitive comparison.</param>
		/// <param name="cultureInfo">A <see cref="CultureInfo"/> object that supplies culture-specific comparison information.</param>
		/// <param name="message">A message to display if the assertion fails. This message can be seen in the unit test results.</param>
		public static void AreEqual(string expected, string actual, bool ignoreCase, CultureInfo cultureInfo, string message)
		{
			Assert.AreEqual(expected, actual, ignoreCase, cultureInfo, message);
		}

		/// <summary>
		/// Verifies that two specified strings are equal, ignoring case or not as specified, and using the culture info specified. The assertion fails if they are not equal. Displays a message if the assertion fails, and applies the specified formatting to it.
		/// </summary>
		/// <param name="expected">The first string to compare. This is the string the unit test expects.</param>
		/// <param name="actual">The second string to compare. This is the string the unit test produced.</param>
		/// <param name="ignoreCase">A Boolean value that indicates a case-sensitive or insensitive comparison. <c>true</c> indicates a case-insensitive comparison.</param>
		/// <param name="cultureInfo">A <see cref="CultureInfo"/> object that supplies culture-specific comparison information.</param>
		/// <param name="message">A message to display if the assertion fails. This message can be seen in the unit test results.</param>
		/// <param name="parameters">An array of parameters to use when formatting <paramref name="message"/>.</param>
		public static void AreEqual(string expected, string actual, bool ignoreCase, CultureInfo cultureInfo, string message, params object[] parameters)
		{
			Assert.AreEqual(expected, actual, ignoreCase, cultureInfo, message, parameters);
		}

		/// <summary>
		/// Verifies that two specified generic type data are equal by using the equality operator. The assertion fails if they are not equal.
		/// </summary>
		/// <typeparam name="T">The type.</typeparam>
		/// <param name="expected">The first generic type data to compare. This is the generic type data the unit test expects.</param>
		/// <param name="actual">The second generic type data to compare. This is the generic type data the unit test produced.</param>
		public static void AreEqual<T>(T expected, T actual)
		{
			Assert.AreEqual<T>(expected, actual);
		}

		/// <summary>
		/// Verifies that two specified generic type data are equal by using the equality operator. The assertion fails if they are not equal. Displays a message if the assertion fails.
		/// </summary>
		/// <typeparam name="T">The type.</typeparam>
		/// <param name="expected">The first generic type data to compare. This is the generic type data the unit test expects.</param>
		/// <param name="actual">The second generic type data to compare. This is the generic type data the unit test produced.</param>
		/// <param name="message">A message to display if the assertion fails. This message can be seen in the unit test results.</param>
		public static void AreEqual<T>(T expected, T actual, string message)
		{
			Assert.AreEqual<T>(expected, actual, message);
		}

		/// <summary>
		/// Verifies that two specified generic type data are equal by using the equality operator. The assertion fails if they are not equal. Displays a message if the assertion fails, and applies the specified formatting to it.
		/// </summary>
		/// <typeparam name="T">The type.</typeparam>
		/// <param name="expected">The first generic type data to compare. This is the generic type data the unit test expects.</param>
		/// <param name="actual">The second generic type data to compare. This is the generic type data the unit test produced.</param>
		/// <param name="message">A message to display if the assertion fails. This message can be seen in the unit test results.</param>
		/// <param name="parameters">An array of parameters to use when formatting <paramref name="message"/>.</param>
		public static void AreEqual<T>(T expected, T actual, string message, params object[] parameters)
		{
			Assert.AreEqual<T>(expected, actual, message, parameters);
		}

		/// <summary>
		/// Verifies that the specified <see cref="IEnumerable{SelectListItemViewModel}"/> is equal to the specified <see cref="IEnumerable{SelectListItem}"/>
		/// by checking they have the same number of items and that each item in <paramref name="expected"/> is in <paramref name="actual"/>. The order of
		/// the items is ignored.
		/// </summary>
		/// <param name="expected">The <see cref="IEnumerable{SelectListItemViewModel}"/> to compare. This is the type the unit test expects.</param>
		/// <param name="actual">The <see cref="IEnumerable{SelectListItem}"/> to compare. This is the type the unit test produced.</param>
		public static void AreEqual(IEnumerable<SelectListItemViewModel> expected, IEnumerable<SelectListItem> actual)
		{
			if ((expected != null) && (actual == null))
			{
				Assert.Fail("Not equal; expected is not null, actual is null");
			}
			if ((expected == null) && (actual != null))
			{
				Assert.Fail("Not equal; expected is null, actual is not null");
			}
			if ((expected != null) && (actual != null))
			{
				Assert.AreEqual(expected.Count(), actual.Count(), "IEnumerable Count");
				foreach (var expectedItem in expected)
				{
					var actualItems = actual.Where(x => x.Value == expectedItem.Id.ToString(CultureInfo.InvariantCulture));
					if (actualItems.Count() == 0)
					{
						Assert.Fail("Item not found; Id {0}, Name \"{1}\"", expectedItem.Id.ToString(CultureInfo.InvariantCulture), expectedItem.Name);
					}
					if (actualItems.Count() > 1)
					{
						Assert.Fail("Multiple matching items found; Id {0}, Name \"{1}\"", expectedItem.Id.ToString(CultureInfo.InvariantCulture), expectedItem.Name);
					}
				}
			}
		}
	}
}
