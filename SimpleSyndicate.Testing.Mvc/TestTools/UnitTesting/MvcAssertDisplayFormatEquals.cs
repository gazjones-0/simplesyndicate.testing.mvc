﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SimpleSyndicate.Testing.Mvc.TestTools.UnitTesting
{
	/// <summary>
	/// Verifies conditions in unit tests using true/false propositions; this class provides all the functionality of <see cref="Assert"/>
	/// but with extra functionality specifically designed for MVC applications.
	/// </summary>
	public static partial class MvcAssert
	{
		/// <overloads>
		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> with the specified format.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different format.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> with the specified format.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different format.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataFormatString">The expected display format.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "format")]
		/// MvcAssert.DisplayFormatEquals&lt;SomeViewModel&gt;("format", x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "string", Justification = "Parameter is named same as property to make it more understandable for users.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayFormatEquals<TModel>(string expectedDataFormatString, Expression<Func<TModel, bool>> expression)
		{
			DisplayFormatEqualsInternal<TModel, bool>(expectedDataFormatString, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> with the specified format.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different format.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataFormatString">The expected display format.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "format")]
		/// MvcAssert.DisplayFormatEquals&lt;SomeViewModel&gt;("format", x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "string", Justification = "Parameter is named same as property to make it more understandable for users.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayFormatEquals<TModel>(string expectedDataFormatString, Expression<Func<TModel, bool?>> expression)
		{
			DisplayFormatEqualsInternal<TModel, bool?>(expectedDataFormatString, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> with the specified format.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different format.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataFormatString">The expected display format.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "format")]
		/// MvcAssert.DisplayFormatEquals&lt;SomeViewModel&gt;("format", x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "string", Justification = "Parameter is named same as property to make it more understandable for users.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayFormatEquals<TModel>(string expectedDataFormatString, Expression<Func<TModel, byte>> expression)
		{
			DisplayFormatEqualsInternal<TModel, byte>(expectedDataFormatString, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> with the specified format.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different format.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataFormatString">The expected display format.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "format")]
		/// MvcAssert.DisplayFormatEquals&lt;SomeViewModel&gt;("format", x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "string", Justification = "Parameter is named same as property to make it more understandable for users.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayFormatEquals<TModel>(string expectedDataFormatString, Expression<Func<TModel, byte?>> expression)
		{
			DisplayFormatEqualsInternal<TModel, byte?>(expectedDataFormatString, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> with the specified format.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different format.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataFormatString">The expected display format.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "format")]
		/// MvcAssert.DisplayFormatEquals&lt;SomeViewModel&gt;("format", x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "string", Justification = "Parameter is named same as property to make it more understandable for users.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayFormatEquals<TModel>(string expectedDataFormatString, Expression<Func<TModel, char>> expression)
		{
			DisplayFormatEqualsInternal<TModel, char>(expectedDataFormatString, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> with the specified format.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different format.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataFormatString">The expected display format.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "format")]
		/// MvcAssert.DisplayFormatEquals&lt;SomeViewModel&gt;("format", x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "string", Justification = "Parameter is named same as property to make it more understandable for users.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayFormatEquals<TModel>(string expectedDataFormatString, Expression<Func<TModel, char?>> expression)
		{
			DisplayFormatEqualsInternal<TModel, char?>(expectedDataFormatString, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> with the specified format.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different format.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataFormatString">The expected display format.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "format")]
		/// MvcAssert.DisplayFormatEquals&lt;SomeViewModel&gt;("format", x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "string", Justification = "Parameter is named same as property to make it more understandable for users.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayFormatEquals<TModel>(string expectedDataFormatString, Expression<Func<TModel, DateTime>> expression)
		{
			DisplayFormatEqualsInternal<TModel, DateTime>(expectedDataFormatString, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> with the specified format.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different format.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataFormatString">The expected display format.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "format")]
		/// MvcAssert.DisplayFormatEquals&lt;SomeViewModel&gt;("format", x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "string", Justification = "Parameter is named same as property to make it more understandable for users.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayFormatEquals<TModel>(string expectedDataFormatString, Expression<Func<TModel, DateTime?>> expression)
		{
			DisplayFormatEqualsInternal<TModel, DateTime?>(expectedDataFormatString, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> with the specified format.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different format.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataFormatString">The expected display format.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "format")]
		/// MvcAssert.DisplayFormatEquals&lt;SomeViewModel&gt;("format", x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "string", Justification = "Parameter is named same as property to make it more understandable for users.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayFormatEquals<TModel>(string expectedDataFormatString, Expression<Func<TModel, decimal>> expression)
		{
			DisplayFormatEqualsInternal<TModel, decimal>(expectedDataFormatString, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> with the specified format.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different format.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataFormatString">The expected display format.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "format")]
		/// MvcAssert.DisplayFormatEquals&lt;SomeViewModel&gt;("format", x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "string", Justification = "Parameter is named same as property to make it more understandable for users.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayFormatEquals<TModel>(string expectedDataFormatString, Expression<Func<TModel, decimal?>> expression)
		{
			DisplayFormatEqualsInternal<TModel, decimal?>(expectedDataFormatString, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> with the specified format.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different format.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataFormatString">The expected display format.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "format")]
		/// MvcAssert.DisplayFormatEquals&lt;SomeViewModel&gt;("format", x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "string", Justification = "Parameter is named same as property to make it more understandable for users.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayFormatEquals<TModel>(string expectedDataFormatString, Expression<Func<TModel, double>> expression)
		{
			DisplayFormatEqualsInternal<TModel, double>(expectedDataFormatString, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> with the specified format.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different format.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataFormatString">The expected display format.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "format")]
		/// MvcAssert.DisplayFormatEquals&lt;SomeViewModel&gt;("format", x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "string", Justification = "Parameter is named same as property to make it more understandable for users.")]
		public static void DisplayFormatEquals<TModel>(string expectedDataFormatString, Expression<Func<TModel, double?>> expression)
		{
			DisplayFormatEqualsInternal<TModel, double?>(expectedDataFormatString, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> with the specified format.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different format.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataFormatString">The expected display format.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "format")]
		/// MvcAssert.DisplayFormatEquals&lt;SomeViewModel&gt;("format", x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "string", Justification = "Parameter is named same as property to make it more understandable for users.")]
		public static void DisplayFormatEquals<TModel>(string expectedDataFormatString, Expression<Func<TModel, float>> expression)
		{
			DisplayFormatEqualsInternal<TModel, float>(expectedDataFormatString, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> with the specified format.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different format.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataFormatString">The expected display format.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "format")]
		/// MvcAssert.DisplayFormatEquals&lt;SomeViewModel&gt;("format", x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "string", Justification = "Parameter is named same as property to make it more understandable for users.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayFormatEquals<TModel>(string expectedDataFormatString, Expression<Func<TModel, float?>> expression)
		{
			DisplayFormatEqualsInternal<TModel, float?>(expectedDataFormatString, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> with the specified format.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different format.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataFormatString">The expected display format.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "format")]
		/// MvcAssert.DisplayFormatEquals&lt;SomeViewModel&gt;("format", x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "string", Justification = "Parameter is named same as property to make it more understandable for users.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayFormatEquals<TModel>(string expectedDataFormatString, Expression<Func<TModel, int>> expression)
		{
			DisplayFormatEqualsInternal<TModel, int>(expectedDataFormatString, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> with the specified format.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different format.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataFormatString">The expected display format.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "format")]
		/// MvcAssert.DisplayFormatEquals&lt;SomeViewModel&gt;("format", x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "string", Justification = "Parameter is named same as property to make it more understandable for users.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayFormatEquals<TModel>(string expectedDataFormatString, Expression<Func<TModel, int?>> expression)
		{
			DisplayFormatEqualsInternal<TModel, int?>(expectedDataFormatString, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> with the specified format.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different format.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataFormatString">The expected display format.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "format")]
		/// MvcAssert.DisplayFormatEquals&lt;SomeViewModel&gt;("format", x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "string", Justification = "Parameter is named same as property to make it more understandable for users.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayFormatEquals<TModel>(string expectedDataFormatString, Expression<Func<TModel, long>> expression)
		{
			DisplayFormatEqualsInternal<TModel, long>(expectedDataFormatString, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> with the specified format.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different format.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataFormatString">The expected display format.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "format")]
		/// MvcAssert.DisplayFormatEquals&lt;SomeViewModel&gt;("format", x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "string", Justification = "Parameter is named same as property to make it more understandable for users.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayFormatEquals<TModel>(string expectedDataFormatString, Expression<Func<TModel, long?>> expression)
		{
			DisplayFormatEqualsInternal<TModel, long?>(expectedDataFormatString, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> with the specified format.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different format.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataFormatString">The expected display format.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "format")]
		/// MvcAssert.DisplayFormatEquals&lt;SomeViewModel&gt;("format", x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "string", Justification = "Parameter is named same as property to make it more understandable for users.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayFormatEquals<TModel>(string expectedDataFormatString, Expression<Func<TModel, sbyte>> expression)
		{
			DisplayFormatEqualsInternal<TModel, sbyte>(expectedDataFormatString, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> with the specified format.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different format.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataFormatString">The expected display format.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "format")]
		/// MvcAssert.DisplayFormatEquals&lt;SomeViewModel&gt;("format", x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "string", Justification = "Parameter is named same as property to make it more understandable for users.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayFormatEquals<TModel>(string expectedDataFormatString, Expression<Func<TModel, sbyte?>> expression)
		{
			DisplayFormatEqualsInternal<TModel, sbyte?>(expectedDataFormatString, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> with the specified format.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different format.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataFormatString">The expected display format.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "format")]
		/// MvcAssert.DisplayFormatEquals&lt;SomeViewModel&gt;("format", x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "string", Justification = "Parameter is named same as property to make it more understandable for users.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayFormatEquals<TModel>(string expectedDataFormatString, Expression<Func<TModel, short>> expression)
		{
			DisplayFormatEqualsInternal<TModel, short>(expectedDataFormatString, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> with the specified format.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different format.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataFormatString">The expected display format.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "format")]
		/// MvcAssert.DisplayFormatEquals&lt;SomeViewModel&gt;("format", x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "string", Justification = "Parameter is named same as property to make it more understandable for users.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayFormatEquals<TModel>(string expectedDataFormatString, Expression<Func<TModel, short?>> expression)
		{
			DisplayFormatEqualsInternal<TModel, short?>(expectedDataFormatString, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> with the specified format.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different format.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataFormatString">The expected display format.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "format")]
		/// MvcAssert.DisplayFormatEquals&lt;SomeViewModel&gt;("format", x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "string", Justification = "Parameter is named same as property to make it more understandable for users.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayFormatEquals<TModel>(string expectedDataFormatString, Expression<Func<TModel, string>> expression)
		{
			DisplayFormatEqualsInternal<TModel, string>(expectedDataFormatString, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> with the specified format.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different format.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataFormatString">The expected display format.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "format")]
		/// MvcAssert.DisplayFormatEquals&lt;SomeViewModel&gt;("format", x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "string", Justification = "Parameter is named same as property to make it more understandable for users.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayFormatEquals<TModel>(string expectedDataFormatString, Expression<Func<TModel, uint>> expression)
		{
			DisplayFormatEqualsInternal<TModel, uint>(expectedDataFormatString, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> with the specified format.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different format.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataFormatString">The expected display format.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "format")]
		/// MvcAssert.DisplayFormatEquals&lt;SomeViewModel&gt;("format", x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "string", Justification = "Parameter is named same as property to make it more understandable for users.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayFormatEquals<TModel>(string expectedDataFormatString, Expression<Func<TModel, uint?>> expression)
		{
			DisplayFormatEqualsInternal<TModel, uint?>(expectedDataFormatString, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> with the specified format.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different format.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataFormatString">The expected display format.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "format")]
		/// MvcAssert.DisplayFormatEquals&lt;SomeViewModel&gt;("format", x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "string", Justification = "Parameter is named same as property to make it more understandable for users.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayFormatEquals<TModel>(string expectedDataFormatString, Expression<Func<TModel, ulong>> expression)
		{
			DisplayFormatEqualsInternal<TModel, ulong>(expectedDataFormatString, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> with the specified format.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different format.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataFormatString">The expected display format.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "format")]
		/// MvcAssert.DisplayFormatEquals&lt;SomeViewModel&gt;("format", x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "string", Justification = "Parameter is named same as property to make it more understandable for users.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayFormatEquals<TModel>(string expectedDataFormatString, Expression<Func<TModel, ulong?>> expression)
		{
			DisplayFormatEqualsInternal<TModel, ulong?>(expectedDataFormatString, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> with the specified format.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different format.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataFormatString">The expected display format.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "format")]
		/// MvcAssert.DisplayFormatEquals&lt;SomeViewModel&gt;("format", x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "string", Justification = "Parameter is named same as property to make it more understandable for users.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayFormatEquals<TModel>(string expectedDataFormatString, Expression<Func<TModel, ushort>> expression)
		{
			DisplayFormatEqualsInternal<TModel, ushort>(expectedDataFormatString, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> with the specified format.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different format.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedDataFormatString">The expected display format.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "format")]
		/// MvcAssert.DisplayFormatEquals&lt;SomeViewModel&gt;("format", x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "string", Justification = "Parameter is named same as property to make it more understandable for users.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void DisplayFormatEquals<TModel>(string expectedDataFormatString, Expression<Func<TModel, ushort?>> expression)
		{
			DisplayFormatEqualsInternal<TModel, ushort?>(expectedDataFormatString, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> with the specified format.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but specifies a different format.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <typeparam name="TType">The property type.</typeparam>
		/// <param name="expectedDataFormatString">The expected display format.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "string", Justification = "Parameter is named same as property to make it more understandable for users.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		private static void DisplayFormatEqualsInternal<TModel, TType>(string expectedDataFormatString, Expression<Func<TModel, TType>> expression)
		{
			var attribute = AttributeHelpers.GetAttribute(expression, typeof(DisplayFormatAttribute));
			if (attribute == null)
			{
				Assert.Fail("No DisplayFormat attribute found");
			}
			var actualDataFormatString = ((DisplayFormatAttribute)attribute).DataFormatString;
			Assert.AreEqual(expectedDataFormatString, actualDataFormatString);
		}
	}
}
