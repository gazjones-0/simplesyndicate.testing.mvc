﻿using System;
using System.Linq.Expressions;
using System.Net.Http;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SimpleSyndicate.Testing.Mvc.TestTools.UnitTesting
{
	/// <summary>
	/// Verifies conditions in unit tests using true/false propositions; this class provides all the functionality of <see cref="Assert"/>
	/// but with extra functionality specifically designed for MVC applications.
	/// </summary>
	public static partial class MvcAssert
	{
		/// <overloads>
		/// <summary>
		/// Verifies that the specified controller method has been decorated with the specified attribute and returns it.
		/// The assertion fails if the specified method does not have the attribute.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Verifies that the specified controller method has been decorated with the specified attribute and returns it.
		/// The assertion fails if the specified method does not have the attribute.
		/// </summary>
		/// <typeparam name="TController">The controller type.</typeparam>
		/// <typeparam name="TExpectedType">The attribute type expected to be found on the method.</typeparam>
		/// <param name="expression">The method that is expected to be decorated with the attribute.</param>
		/// <returns>The attribute of the specified type found on the method.</returns>
		/// <example>
		/// <code language="cs">
		/// // note that the method isn't called, it's just examined to see if it has the attribute
		/// var attribute = MvcAssert.GetAttribute&lt;SomeController, HttpGetAttribute&gt;(x => x.SomeMethod());
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static TExpectedType GetAttribute<TController, TExpectedType>(Expression<Func<TController, ActionResult>> expression)
			where TExpectedType : Attribute
		{
			return GetAttributeInternal<TController, ActionResult, TExpectedType>(expression);
		}

		/// <summary>
		/// Verifies that the specified controller method has been decorated with the specified attribute and returns it.
		/// The assertion fails if the specified method does not have the attribute.
		/// </summary>
		/// <typeparam name="TController">The controller type.</typeparam>
		/// <typeparam name="TExpectedType">The attribute type expected to be found on the method.</typeparam>
		/// <param name="expression">The method that is expected to be decorated with the attribute.</param>
		/// <returns>The attribute of the specified type found on the method.</returns>
		/// <example>
		/// <code language="cs">
		/// // note that the method isn't called, it's just examined to see if it has the attribute
		/// var attribute = MvcAssert.GetAttribute&lt;SomeController, HttpGetAttribute&gt;(x => x.SomeMethod());
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static TExpectedType GetAttribute<TController, TExpectedType>(Expression<Func<TController, JsonResult>> expression)
			where TExpectedType : Attribute
		{
			return GetAttributeInternal<TController, JsonResult, TExpectedType>(expression);
		}

		/// <summary>
		/// Verifies that the specified controller method has been decorated with the specified attribute and returns it.
		/// The assertion fails if the specified method does not have the attribute.
		/// </summary>
		/// <typeparam name="TController">The controller type.</typeparam>
		/// <typeparam name="TExpectedType">The attribute type expected to be found on the method.</typeparam>
		/// <param name="expression">The method that is expected to be decorated with the attribute.</param>
		/// <returns>The attribute of the specified type found on the method.</returns>
		/// <example>
		/// <code language="cs">
		/// // note that the method isn't called, it's just examined to see if it has the attribute
		/// var attribute = MvcAssert.GetAttribute&lt;SomeController, HttpGetAttribute&gt;(x => x.SomeMethod());
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static TExpectedType GetAttribute<TController, TExpectedType>(Expression<Func<TController, ViewResult>> expression)
			where TExpectedType : Attribute
		{
			return GetAttributeInternal<TController, ViewResult, TExpectedType>(expression);
		}

		/// <summary>
		/// Verifies that the specified controller method has been decorated with the specified attribute and returns it.
		/// The assertion fails if the specified method does not have the attribute.
		/// </summary>
		/// <typeparam name="TController">The controller type.</typeparam>
		/// <typeparam name="TExpectedType">The attribute type expected to be found on the method.</typeparam>
		/// <param name="expression">The method that is expected to be decorated with the attribute.</param>
		/// <returns>The attribute of the specified type found on the method.</returns>
		/// <example>
		/// <code language="cs">
		/// // note that the method isn't called, it's just examined to see if it has the attribute
		/// var attribute = MvcAssert.GetAttribute&lt;SomeController, HttpGetAttribute&gt;(x => x.SomeMethod());
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static TExpectedType GetAttribute<TController, TExpectedType>(Expression<Func<TController, HttpResponseMessage>> expression)
			where TExpectedType : Attribute
		{
			return GetAttributeInternal<TController, HttpResponseMessage, TExpectedType>(expression);
		}

		/// <summary>
		/// Verifies that the specified controller method has been decorated with the specified attribute and returns it.
		/// The assertion fails if the specified method does not have the attribute.
		/// </summary>
		/// <typeparam name="TController">The controller type.</typeparam>
		/// <typeparam name="TReturnType">The method's return type.</typeparam>
		/// <typeparam name="TExpectedType">The attribute type expected to be found on the method.</typeparam>
		/// <param name="expression">The method that is expected to be decorated with the attribute.</param>
		/// <returns>The attribute of the specified type found on the method.</returns>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		private static TExpectedType GetAttributeInternal<TController, TReturnType, TExpectedType>(Expression<Func<TController, TReturnType>> expression)
			where TExpectedType : Attribute
		{
			var attribute = AttributeHelpers.GetAttribute(expression, typeof(TExpectedType));
			if (attribute == null)
			{
				Assert.Fail("No " + typeof(TExpectedType).Name + " attribute found");
			}
			return (TExpectedType)attribute;
		}
	}
}
