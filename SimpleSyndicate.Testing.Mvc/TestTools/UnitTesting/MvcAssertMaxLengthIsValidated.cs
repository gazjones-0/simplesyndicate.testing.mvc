﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SimpleSyndicate.Testing.Mvc.TestTools.UnitTesting
{
	/// <summary>
	/// Verifies conditions in unit tests using true/false propositions; this class provides all the functionality of <see cref="Assert"/>
	/// but with extra functionality specifically designed for MVC applications.
	/// </summary>
	public static partial class MvcAssert
	{
		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="MaxLengthAttribute"/> with the specified
		/// maximum length. The assertion fails if the specified model property does not have the attribute, or the model property has
		/// the attribute but a different maximum length.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expectedMaximumLength">Expected maximum length.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [MaxLength]
		/// MvcAssert.MaxLengthIsValidated&lt;SomeInputModel&gt;(4, x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void MaxLengthIsValidated<TModel>(int expectedMaximumLength, Expression<Func<TModel, string>> expression)
		{
			MaxLengthIsValidatedInternal<TModel, string>(expectedMaximumLength, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="MaxLengthAttribute"/> with the specified
		/// maximum length. The assertion fails if the specified model property does not have the attribute, or the model property has
		/// the attribute but a different maximum length.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <typeparam name="TType">The property type.</typeparam>
		/// <param name="expectedMaximumLength">Expected maximum length.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		private static void MaxLengthIsValidatedInternal<TModel, TType>(int expectedMaximumLength, Expression<Func<TModel, TType>> expression)
		{
			var attribute = AttributeHelpers.GetAttribute(expression, typeof(MaxLengthAttribute));
			if (attribute == null)
			{
				Assert.Fail("No MaxLength attribute found");
			}
			var maxLengthAttribute = (MaxLengthAttribute)attribute;
			var actualMaximumLength = maxLengthAttribute.Length;
			Assert.AreEqual(expectedMaximumLength, actualMaximumLength, "MaxLength did not match expected value");
		}
	}
}
