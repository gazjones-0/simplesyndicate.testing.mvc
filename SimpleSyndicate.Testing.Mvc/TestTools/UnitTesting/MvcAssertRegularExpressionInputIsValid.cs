﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SimpleSyndicate.Testing.Mvc.TestTools.UnitTesting
{
	/// <summary>
	/// Verifies conditions in unit tests using true/false propositions; this class provides all the functionality of <see cref="Assert"/>
	/// but with extra functionality specifically designed for MVC applications.
	/// </summary>
	public static partial class MvcAssert
	{
		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RegularExpressionAttribute"/> and that
		/// the specified input matches the regular expression. The assertion fails if the specified model property does not have the
		/// attribute, or the model property has the attribute but the specified input doesn't validate.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="input">The input that is expected to validate against the regular expression.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [RegularExpression]
		/// MvcAssert.RegularExpressionInputIsValid&lt;SomeInputModel&gt;("valid input", x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void RegularExpressionInputIsValid<TModel>(string input, Expression<Func<TModel, string>> expression)
		{
			RegularExpressionInputIsValidInternal<TModel, string>(input, expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RegularExpressionAttribute"/> and that
		/// the specified input matches the regular expression. The assertion fails if the specified model property does not have the
		/// attribute, or the model property has the attribute but the specified input doesn't validate.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <typeparam name="TType">The property type.</typeparam>
		/// <param name="input">The input that is expected to validate against the regular expression.</param>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [RegularExpression]
		/// MvcAssert.RegularExpressionInputIsValid&lt;SomeInputModel&gt;("valid input", x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		private static void RegularExpressionInputIsValidInternal<TModel, TType>(string input, Expression<Func<TModel, TType>> expression)
		{
			var attribute = (RegularExpressionAttribute)AttributeHelpers.GetAttribute(expression, typeof(RegularExpressionAttribute));
			if (attribute == null)
			{
				Assert.Fail("No RegularExpression attribute found");
			}
			Assert.IsTrue(attribute.IsValid(input), "Regular expression was invalid for input \"{0}\" (input did not pass validation)", input);
		}
	}
}
