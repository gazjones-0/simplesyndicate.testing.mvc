﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SimpleSyndicate.Testing.Mvc.TestTools.UnitTesting
{
	/// <summary>
	/// Verifies conditions in unit tests using true/false propositions; this class provides all the functionality of <see cref="Assert"/>
	/// but with extra functionality specifically designed for MVC applications.
	/// </summary>
	public static partial class MvcAssert
	{
		/// <overloads>
		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RequiredAttribute"/>.
		/// The assertion fails if the specified model property does not have the attribute.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RequiredAttribute"/>.
		/// The assertion fails if the specified model property does not have the attribute.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Required]
		/// MvcAssert.IsRequired&lt;SomeInputModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsRequired<TModel>(Expression<Func<TModel, bool>> expression)
		{
			IsRequiredInternal<TModel, bool>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RequiredAttribute"/>.
		/// The assertion fails if the specified model property does not have the attribute.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Required]
		/// MvcAssert.IsRequired&lt;SomeInputModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsRequired<TModel>(Expression<Func<TModel, bool?>> expression)
		{
			IsRequiredInternal<TModel, bool?>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RequiredAttribute"/>.
		/// The assertion fails if the specified model property does not have the attribute.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Required]
		/// MvcAssert.IsRequired&lt;SomeInputModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsRequired<TModel>(Expression<Func<TModel, byte>> expression)
		{
			IsRequiredInternal<TModel, byte>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RequiredAttribute"/>.
		/// The assertion fails if the specified model property does not have the attribute.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Required]
		/// MvcAssert.IsRequired&lt;SomeInputModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsRequired<TModel>(Expression<Func<TModel, byte?>> expression)
		{
			IsRequiredInternal<TModel, byte?>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RequiredAttribute"/>.
		/// The assertion fails if the specified model property does not have the attribute.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Required]
		/// MvcAssert.IsRequired&lt;SomeInputModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsRequired<TModel>(Expression<Func<TModel, char>> expression)
		{
			IsRequiredInternal<TModel, char>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RequiredAttribute"/>.
		/// The assertion fails if the specified model property does not have the attribute.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Required]
		/// MvcAssert.IsRequired&lt;SomeInputModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsRequired<TModel>(Expression<Func<TModel, char?>> expression)
		{
			IsRequiredInternal<TModel, char?>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RequiredAttribute"/>.
		/// The assertion fails if the specified model property does not have the attribute.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Required]
		/// MvcAssert.IsRequired&lt;SomeInputModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsRequired<TModel>(Expression<Func<TModel, DateTime>> expression)
		{
			IsRequiredInternal<TModel, DateTime>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RequiredAttribute"/>.
		/// The assertion fails if the specified model property does not have the attribute.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Required]
		/// MvcAssert.IsRequired&lt;SomeInputModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsRequired<TModel>(Expression<Func<TModel, DateTime?>> expression)
		{
			IsRequiredInternal<TModel, DateTime?>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RequiredAttribute"/>.
		/// The assertion fails if the specified model property does not have the attribute.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Required]
		/// MvcAssert.IsRequired&lt;SomeInputModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsRequired<TModel>(Expression<Func<TModel, decimal>> expression)
		{
			IsRequiredInternal<TModel, decimal>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RequiredAttribute"/>.
		/// The assertion fails if the specified model property does not have the attribute.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Required]
		/// MvcAssert.IsRequired&lt;SomeInputModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsRequired<TModel>(Expression<Func<TModel, decimal?>> expression)
		{
			IsRequiredInternal<TModel, decimal?>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RequiredAttribute"/>.
		/// The assertion fails if the specified model property does not have the attribute.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Required]
		/// MvcAssert.IsRequired&lt;SomeInputModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsRequired<TModel>(Expression<Func<TModel, double>> expression)
		{
			IsRequiredInternal<TModel, double>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RequiredAttribute"/>.
		/// The assertion fails if the specified model property does not have the attribute.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Required]
		/// MvcAssert.IsRequired&lt;SomeInputModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsRequired<TModel>(Expression<Func<TModel, double?>> expression)
		{
			IsRequiredInternal<TModel, double?>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RequiredAttribute"/>.
		/// The assertion fails if the specified model property does not have the attribute.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Required]
		/// MvcAssert.IsRequired&lt;SomeInputModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsRequired<TModel>(Expression<Func<TModel, float>> expression)
		{
			IsRequiredInternal<TModel, float>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RequiredAttribute"/>.
		/// The assertion fails if the specified model property does not have the attribute.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Required]
		/// MvcAssert.IsRequired&lt;SomeInputModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsRequired<TModel>(Expression<Func<TModel, float?>> expression)
		{
			IsRequiredInternal<TModel, float?>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RequiredAttribute"/>.
		/// The assertion fails if the specified model property does not have the attribute.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Required]
		/// MvcAssert.IsRequired&lt;SomeInputModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsRequired<TModel>(Expression<Func<TModel, int>> expression)
		{
			IsRequiredInternal<TModel, int>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RequiredAttribute"/>.
		/// The assertion fails if the specified model property does not have the attribute.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Required]
		/// MvcAssert.IsRequired&lt;SomeInputModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsRequired<TModel>(Expression<Func<TModel, int?>> expression)
		{
			IsRequiredInternal<TModel, int?>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RequiredAttribute"/>.
		/// The assertion fails if the specified model property does not have the attribute.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Required]
		/// MvcAssert.IsRequired&lt;SomeInputModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsRequired<TModel>(Expression<Func<TModel, long>> expression)
		{
			IsRequiredInternal<TModel, long>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RequiredAttribute"/>.
		/// The assertion fails if the specified model property does not have the attribute.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Required]
		/// MvcAssert.IsRequired&lt;SomeInputModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsRequired<TModel>(Expression<Func<TModel, long?>> expression)
		{
			IsRequiredInternal<TModel, long?>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RequiredAttribute"/>.
		/// The assertion fails if the specified model property does not have the attribute.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Required]
		/// MvcAssert.IsRequired&lt;SomeInputModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsRequired<TModel>(Expression<Func<TModel, sbyte>> expression)
		{
			IsRequiredInternal<TModel, sbyte>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RequiredAttribute"/>.
		/// The assertion fails if the specified model property does not have the attribute.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Required]
		/// MvcAssert.IsRequired&lt;SomeInputModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsRequired<TModel>(Expression<Func<TModel, sbyte?>> expression)
		{
			IsRequiredInternal<TModel, sbyte?>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RequiredAttribute"/>.
		/// The assertion fails if the specified model property does not have the attribute.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Required]
		/// MvcAssert.IsRequired&lt;SomeInputModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsRequired<TModel>(Expression<Func<TModel, short>> expression)
		{
			IsRequiredInternal<TModel, short>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RequiredAttribute"/>.
		/// The assertion fails if the specified model property does not have the attribute.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Required]
		/// MvcAssert.IsRequired&lt;SomeInputModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsRequired<TModel>(Expression<Func<TModel, short?>> expression)
		{
			IsRequiredInternal<TModel, short?>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RequiredAttribute"/>.
		/// The assertion fails if the specified model property does not have the attribute.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Required]
		/// MvcAssert.IsRequired&lt;SomeInputModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsRequired<TModel>(Expression<Func<TModel, string>> expression)
		{
			IsRequiredInternal<TModel, string>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RequiredAttribute"/>.
		/// The assertion fails if the specified model property does not have the attribute.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Required]
		/// MvcAssert.IsRequired&lt;SomeInputModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsRequired<TModel>(Expression<Func<TModel, uint>> expression)
		{
			IsRequiredInternal<TModel, uint>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RequiredAttribute"/>.
		/// The assertion fails if the specified model property does not have the attribute.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Required]
		/// MvcAssert.IsRequired&lt;SomeInputModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsRequired<TModel>(Expression<Func<TModel, uint?>> expression)
		{
			IsRequiredInternal<TModel, uint?>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RequiredAttribute"/>.
		/// The assertion fails if the specified model property does not have the attribute.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Required]
		/// MvcAssert.IsRequired&lt;SomeInputModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsRequired<TModel>(Expression<Func<TModel, ulong>> expression)
		{
			IsRequiredInternal<TModel, ulong>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RequiredAttribute"/>.
		/// The assertion fails if the specified model property does not have the attribute.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Required]
		/// MvcAssert.IsRequired&lt;SomeInputModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsRequired<TModel>(Expression<Func<TModel, ulong?>> expression)
		{
			IsRequiredInternal<TModel, ulong?>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RequiredAttribute"/>.
		/// The assertion fails if the specified model property does not have the attribute.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Required]
		/// MvcAssert.IsRequired&lt;SomeInputModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsRequired<TModel>(Expression<Func<TModel, ushort>> expression)
		{
			IsRequiredInternal<TModel, ushort>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RequiredAttribute"/>.
		/// The assertion fails if the specified model property does not have the attribute.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [Required]
		/// MvcAssert.IsRequired&lt;SomeInputModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void IsRequired<TModel>(Expression<Func<TModel, ushort?>> expression)
		{
			IsRequiredInternal<TModel, ushort?>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="RequiredAttribute"/>.
		/// The assertion fails if the specified model property does not have the attribute.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <typeparam name="TType">The property type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		private static void IsRequiredInternal<TModel, TType>(Expression<Func<TModel, TType>> expression)
		{
			if (!AttributeHelpers.HasAttribute(expression, typeof(RequiredAttribute)))
			{
				Assert.Fail("No Required attribute found");
			}
		}
	}
}
