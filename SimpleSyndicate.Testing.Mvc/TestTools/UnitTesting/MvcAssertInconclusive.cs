﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SimpleSyndicate.Testing.Mvc.TestTools.UnitTesting
{
	/// <summary>
	/// Verifies conditions in unit tests using true/false propositions; this class provides all the functionality of <see cref="Assert"/>
	/// but with extra functionality specifically designed for MVC applications.
	/// </summary>
	public static partial class MvcAssert
	{
		/// <overloads>
		/// <summary>
		/// Indicates that the assertion cannot be verified.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Indicates that the assertion cannot be verified.
		/// </summary>
		public static void Inconclusive()
		{
			Assert.Inconclusive();
		}

		/// <summary>
		/// Indicates that the assertion can not be verified. Displays a message.
		/// </summary>
		/// <param name="message">A message to display. This message can be seen in the unit test results.</param>
		public static void Inconclusive(string message)
		{
			Assert.Inconclusive(message);
		}

		/// <summary>
		/// Indicates that an assertion can not be verified. Displays a message, and applies the specified formatting to it.
		/// </summary>
		/// <param name="message">A message to display. This message can be seen in the unit test results.</param>
		/// <param name="parameters">An array of parameters to use when formatting <paramref name="message"/>.</param>
		public static void Inconclusive(string message, params object[] parameters)
		{
			Assert.Inconclusive(message, parameters);
		}
	}
}
