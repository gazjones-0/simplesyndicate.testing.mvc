﻿using System.Globalization;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SimpleSyndicate.Testing.Mvc.TestTools.UnitTesting
{
	/// <summary>
	/// Verifies conditions in unit tests using true/false propositions; this class provides all the functionality of <see cref="Assert"/>
	/// but with extra functionality specifically designed for MVC applications.
	/// </summary>
	public static partial class MvcAssert
	{
		/// <overloads>
		/// <summary>
		/// Verifies that specified values are not equal.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Verifies that two specified objects are not equal. The assertion fails if the objects are equal.
		/// </summary>
		/// <param name="notExpected">The first object to compare. This is the object the unit test expects not to match <paramref name="actual"/>.</param>
		/// <param name="actual">The second object to compare. This is the object the unit test produced.</param>
		public static void AreNotEqual(object notExpected, object actual)
		{
			Assert.AreNotEqual(notExpected, actual);
		}

		/// <summary>
		/// Verifies that two specified doubles are not equal, and not within the specified accuracy of each other. The assertion fails if they are equal or within the specified accuracy of each other.
		/// </summary>
		/// <param name="notExpected">The first double to compare. This is the double the unit test expects not to match <paramref name="actual"/>.</param>
		/// <param name="actual">The second double to compare. This is the double the unit test produced.</param>
		/// <param name="delta">The required inaccuracy. The assertion fails only if <paramref name="notExpected"/> is equal to <paramref name="actual"/> or different from it by less than <paramref name="delta"/>.</param>
		public static void AreNotEqual(double notExpected, double actual, double delta)
		{
			Assert.AreNotEqual(notExpected, actual, delta);
		}

		/// <summary>
		/// Verifies that two specified objects are not equal. The assertion fails if the objects are equal. Displays a message if the assertion fails.
		/// </summary>
		/// <param name="notExpected">The first object to compare. This is the object the unit test expects not to match <paramref name="actual"/>.</param>
		/// <param name="actual">The second object to compare. This is the object the unit test produced.</param>
		/// <param name="message">A message to display if the assertion fails. This message can be seen in the unit test results.</param>
		public static void AreNotEqual(object notExpected, object actual, string message)
		{
			Assert.AreNotEqual(notExpected, actual, message);
		}

		/// <summary>
		/// Verifies that two specified floats are not equal, and not within the specified accuracy of each other. The assertion fails if they are equal or within the specified accuracy of each other.
		/// </summary>
		/// <param name="notExpected">The first float to compare. This is the double the unit test expects not to match <paramref name="actual"/>.</param>
		/// <param name="actual">The second float to compare. This is the double the unit test produced.</param>
		/// <param name="delta">The required inaccuracy. The assertion fails only if <paramref name="notExpected"/> is equal to <paramref name="actual"/> or different from it by less than <paramref name="delta"/>.</param>
		public static void AreNotEqual(float notExpected, float actual, float delta)
		{
			Assert.AreNotEqual(notExpected, actual, delta);
		}

		/// <summary>
		/// Verifies that two specified strings are not equal, ignoring case or not as specified. The assertion fails if they are equal.
		/// </summary>
		/// <param name="notExpected">The first string to compare. This is the string the unit test expects not to match <paramref name="actual"/>.</param>
		/// <param name="actual">The second string to compare. This is the string the unit test produced.</param>
		/// <param name="ignoreCase">A Boolean value that indicates a case-sensitive or insensitive comparison. <c>true</c> indicates a case-insensitive comparison.</param>
		public static void AreNotEqual(string notExpected, string actual, bool ignoreCase)
		{
			Assert.AreNotEqual(notExpected, actual, ignoreCase, CultureInfo.InvariantCulture);
		}

		/// <summary>
		/// Verifies that two specified doubles are not equal, and not within the specified accuracy of each other. The assertion fails if they are equal or within the specified accuracy of each other. Displays a message if the assertion fails.
		/// </summary>
		/// <param name="notExpected">The first double to compare. This is the double the unit test expects not to match <paramref name="actual"/>.</param>
		/// <param name="actual">The second double to compare. This is the double the unit test produced.</param>
		/// <param name="delta">The required inaccuracy. The assertion fails only if <paramref name="notExpected"/> is equal to <paramref name="actual"/> or different from it by less than <paramref name="delta"/>.</param>
		/// <param name="message">A message to display if the assertion fails. This message can be seen in the unit test results.</param>
		public static void AreNotEqual(double notExpected, double actual, double delta, string message)
		{
			Assert.AreNotEqual(notExpected, actual, delta, message);
		}

		/// <summary>
		/// Verifies that two specified objects are not equal. The assertion fails if the objects are equal. Displays a message if the assertion fails, and applies the specified formatting to it.
		/// </summary>
		/// <param name="notExpected">The first object to compare. This is the object the unit test expects not to match <paramref name="actual"/>.</param>
		/// <param name="actual">The second object to compare. This is the object the unit test produced.</param>
		/// <param name="message">A message to display if the assertion fails. This message can be seen in the unit test results.</param>
		/// <param name="parameters">An array of parameters to use when formatting <paramref name="message"/>.</param>
		public static void AreNotEqual(object notExpected, object actual, string message, params object[] parameters)
		{
			Assert.AreNotEqual(notExpected, actual, message, parameters);
		}

		/// <summary>
		/// Verifies that two specified floats are not equal, and not within the specified accuracy of each other. The assertion fails if they are equal or within the specified accuracy of each other. Displays a message if the assertion fails.
		/// </summary>
		/// <param name="notExpected">The first float to compare. This is the double the unit test expects not to match <paramref name="actual"/>.</param>
		/// <param name="actual">The second float to compare. This is the double the unit test produced.</param>
		/// <param name="delta">The required inaccuracy. The assertion fails only if <paramref name="notExpected"/> is equal to <paramref name="actual"/> or different from it by less than <paramref name="delta"/>.</param>
		/// <param name="message">A message to display if the assertion fails. This message can be seen in the unit test results.</param>
		public static void AreNotEqual(float notExpected, float actual, float delta, string message)
		{
			Assert.AreNotEqual(notExpected, actual, delta, message);
		}

		/// <summary>
		/// Verifies that two specified strings are not equal, ignoring case or not as specified, and using the culture info specified. The assertion fails if they are equal.
		/// </summary>
		/// <param name="notExpected">The first string to compare. This is the string the unit test expects not to match <paramref name="actual"/>.</param>
		/// <param name="actual">The second string to compare. This is the string the unit test produced.</param>
		/// <param name="ignoreCase">A Boolean value that indicates a case-sensitive or insensitive comparison. <c>true</c> indicates a case-insensitive comparison.</param>
		/// <param name="cultureInfo">A <see cref="CultureInfo"/> object that supplies culture-specific comparison information.</param>
		public static void AreNotEqual(string notExpected, string actual, bool ignoreCase, CultureInfo cultureInfo)
		{
			Assert.AreNotEqual(notExpected, actual, ignoreCase, cultureInfo);
		}

		/// <summary>
		/// Verifies that two specified strings are not equal, ignoring case or not as specified. The assertion fails if they are equal. Displays a message if the assertion fails.
		/// </summary>
		/// <param name="notExpected">The first string to compare. This is the string the unit test expects not to match <paramref name="actual"/>.</param>
		/// <param name="actual">The second string to compare. This is the string the unit test produced.</param>
		/// <param name="ignoreCase">A Boolean value that indicates a case-sensitive or insensitive comparison. <c>true</c> indicates a case-insensitive comparison.</param>
		/// <param name="message">A message to display if the assertion fails. This message can be seen in the unit test results.</param>
		public static void AreNotEqual(string notExpected, string actual, bool ignoreCase, string message)
		{
			Assert.AreNotEqual(notExpected, actual, ignoreCase, message);
		}

		/// <summary>
		/// Verifies that two specified doubles are not equal, and not within the specified accuracy of each other. The assertion fails if they are equal or within the specified accuracy of each other. Displays a message if the assertion fails, and applies the specified formatting to it.
		/// </summary>
		/// <param name="notExpected">The first double to compare. This is the double the unit test expects not to match <paramref name="actual"/>.</param>
		/// <param name="actual">The second double to compare. This is the double the unit test produced.</param>
		/// <param name="delta">The required inaccuracy. The assertion fails only if <paramref name="notExpected"/> is equal to <paramref name="actual"/> or different from it by less than <paramref name="delta"/>.</param>
		/// <param name="message">A message to display if the assertion fails. This message can be seen in the unit test results.</param>
		/// <param name="parameters">An array of parameters to use when formatting <paramref name="message"/>.</param>
		public static void AreNotEqual(double notExpected, double actual, double delta, string message, params object[] parameters)
		{
			Assert.AreNotEqual(notExpected, actual, delta, message, parameters);
		}

		/// <summary>
		/// Verifies that two specified floats are not equal, and not within the specified accuracy of each other. The assertion fails if they are equal or within the specified accuracy of each other. Displays a message if the assertion fails, and applies the specified formatting to it.
		/// </summary>
		/// <param name="notExpected">The first float to compare. This is the double the unit test expects not to match <paramref name="actual"/>.</param>
		/// <param name="actual">The second float to compare. This is the double the unit test produced.</param>
		/// <param name="delta">The required inaccuracy. The assertion fails only if <paramref name="notExpected"/> is equal to <paramref name="actual"/> or different from it by less than <paramref name="delta"/>.</param>
		/// <param name="message">A message to display if the assertion fails. This message can be seen in the unit test results.</param>
		/// <param name="parameters">An array of parameters to use when formatting <paramref name="message"/>.</param>
		public static void AreNotEqual(float notExpected, float actual, float delta, string message, params object[] parameters)
		{
			Assert.AreNotEqual(notExpected, actual, delta, message, parameters);
		}

		/// <summary>
		/// Verifies that two specified strings are not equal, ignoring case or not as specified, and using the culture info specified. The assertion fails if they are equal. Displays a message if the assertion fails.
		/// </summary>
		/// <param name="notExpected">The first string to compare. This is the string the unit test expects not to match <paramref name="actual"/>.</param>
		/// <param name="actual">The second string to compare. This is the string the unit test produced.</param>
		/// <param name="ignoreCase">A Boolean value that indicates a case-sensitive or insensitive comparison. <c>true</c> indicates a case-insensitive comparison.</param>
		/// <param name="cultureInfo">A <see cref="CultureInfo"/> object that supplies culture-specific comparison information.</param>
		/// <param name="message">A message to display if the assertion fails. This message can be seen in the unit test results.</param>
		public static void AreNotEqual(string notExpected, string actual, bool ignoreCase, CultureInfo cultureInfo, string message)
		{
			Assert.AreNotEqual(notExpected, actual, ignoreCase, cultureInfo, message);
		}

		/// <summary>
		/// Verifies that two specified strings are not equal, ignoring case or not as specified. The assertion fails if they are equal. Displays a message if the assertion fails, and applies the specified formatting to it. 
		/// </summary>
		/// <param name="notExpected">The first string to compare. This is the string the unit test expects not to match <paramref name="actual"/>.</param>
		/// <param name="actual">The second string to compare. This is the string the unit test produced.</param>
		/// <param name="ignoreCase">A Boolean value that indicates a case-sensitive or insensitive comparison. <c>true</c> indicates a case-insensitive comparison.</param>
		/// <param name="cultureInfo">A <see cref="CultureInfo"/> object that supplies culture-specific comparison information.</param>
		/// <param name="message">A message to display if the assertion fails. This message can be seen in the unit test results.</param>
		/// <param name="parameters">An array of parameters to use when formatting <paramref name="message"/>.</param>
		public static void AreNotEqual(string notExpected, string actual, bool ignoreCase, CultureInfo cultureInfo, string message, params object[] parameters)
		{
			Assert.AreNotEqual(notExpected, actual, ignoreCase, cultureInfo, message, parameters);
		}

		/// <summary>
		/// Verifies that two specified generic type data are not equal. The assertion fails if they are equal.
		/// </summary>
		/// <typeparam name="T">The type.</typeparam>
		/// <param name="notExpected">The first generic type data to compare. This is the generic type data the unit test expects not to match <paramref name="actual"/>.</param>
		/// <param name="actual">The second generic type data to compare. This is the generic type data the unit test produced.</param>
		public static void AreNotEqual<T>(T notExpected, T actual)
		{
			Assert.AreNotEqual<T>(notExpected, actual);
		}

		/// <summary>
		/// Verifies that two specified generic type data are not equal. The assertion fails if they are equal. Displays a message if the assertion fails.
		/// </summary>
		/// <typeparam name="T">The type.</typeparam>
		/// <param name="notExpected">The first generic type data to compare. This is the generic type data the unit test expects not to match <paramref name="actual"/>.</param>
		/// <param name="actual">The second generic type data to compare. This is the generic type data the unit test produced.</param>
		/// <param name="message">A message to display if the assertion fails. This message can be seen in the unit test results.</param>
		public static void AreNotEqual<T>(T notExpected, T actual, string message)
		{
			Assert.AreNotEqual<T>(notExpected, actual, message);
		}

		/// <summary>
		/// Verifies that two specified generic type data are not equal. The assertion fails if they are equal. Displays a message if the assertion fails, and applies the specified formatting to it.
		/// </summary>
		/// <typeparam name="T">The type.</typeparam>
		/// <param name="notExpected">The first generic type data to compare. This is the generic type data the unit test expects not to match <paramref name="actual"/>.</param>
		/// <param name="actual">The second generic type data to compare. This is the generic type data the unit test produced.</param>
		/// <param name="message">A message to display if the assertion fails. This message can be seen in the unit test results.</param>
		/// <param name="parameters">An array of parameters to use when formatting <paramref name="message"/>.</param>
		public static void AreNotEqual<T>(T notExpected, T actual, string message, params object[] parameters)
		{
			Assert.AreNotEqual<T>(notExpected, actual, message, parameters);
		}
	}
}
