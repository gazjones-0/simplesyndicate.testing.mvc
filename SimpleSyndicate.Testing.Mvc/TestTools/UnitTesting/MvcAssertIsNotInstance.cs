﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SimpleSyndicate.Testing.Mvc.TestTools.UnitTesting
{
	/// <summary>
	/// Verifies conditions in unit tests using true/false propositions; this class provides all the functionality of <see cref="Assert"/>
	/// but with extra functionality specifically designed for MVC applications.
	/// </summary>
	public static partial class MvcAssert
	{
		/// <overloads>
		/// <summary>
		/// Verifies that the specified object is not an instance of the specified type. The assertion fails if the type is found in the inheritance hierarchy of the object.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Verifies that the specified object is not an instance of the specified type. The assertion fails if the type is found in the inheritance hierarchy of the object.
		/// </summary>
		/// <typeparam name="TWrongType">The type that should not be found in the inheritance hierarchy of <paramref name="value"/>.</typeparam>
		/// <param name="value">The object to verify is not of <typeparamref name="TWrongType"/>.</param>
		/// <example>
		/// <code language="cs">
		/// MvcAssert.IsNotInstance&lt;SomeType&gt;(someValue);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Justification = "Inferred type might not be type client wants to verify against")]
		public static void IsNotInstance<TWrongType>(object value)
		{
			Assert.IsNotInstanceOfType(value, typeof(TWrongType));
		}

		/// <summary>
		/// Verifies that the specified object is not an instance of the specified type. The assertion fails if the type is found in the inheritance hierarchy of the object. Displays a message if the assertion fails.
		/// </summary>
		/// <typeparam name="TWrongType">The type that should not be found in the inheritance hierarchy of <paramref name="value"/>.</typeparam>
		/// <param name="value">The object to verify is not of <typeparamref name="TWrongType"/>.</param>
		/// <param name="message">A message to display if the assertion fails. This message can be seen in the unit test results.</param>
		/// <example>
		/// <code language="cs">
		/// MvcAssert.IsNotInstance&lt;SomeType&gt;(someValue, "Some message");
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Justification = "Inferred type might not be type client wants to verify against")]
		public static void IsNotInstance<TWrongType>(object value, string message)
		{
			Assert.IsNotInstanceOfType(value, typeof(TWrongType), message);
		}

		/// <summary>
		/// Verifies that the specified object is not an instance of the specified type. The assertion fails if the type is found in the inheritance hierarchy of the object. Displays a message if the assertion fails, and applies the specified formatting to it.
		/// </summary>
		/// <typeparam name="TWrongType">The type that should not be found in the inheritance hierarchy of <paramref name="value"/>.</typeparam>
		/// <param name="value">The object to verify is not of <typeparamref name="TWrongType"/>.</param>
		/// <param name="message">A message to display if the assertion fails. This message can be seen in the unit test results.</param>
		/// <param name="parameters">An array of parameters to use when formatting <paramref name="message"/>.</param>
		/// <example>
		/// <code language="cs">
		/// MvcAssert.IsNotInstance&lt;SomeType&gt;(someValue, "Some message {0}", someParameter);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Justification = "Inferred type might not be type client wants to verify against")]
		public static void IsNotInstance<TWrongType>(object value, string message, params object[] parameters)
		{
			Assert.IsNotInstanceOfType(value, typeof(TWrongType), message, parameters);
		}
	}
}
