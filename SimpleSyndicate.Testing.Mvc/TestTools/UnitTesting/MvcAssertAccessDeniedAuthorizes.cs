﻿using System;
using System.Linq.Expressions;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSyndicate.Mvc.Web.Mvc;

namespace SimpleSyndicate.Testing.Mvc.TestTools.UnitTesting
{
	/// <summary>
	/// Verifies conditions in unit tests using true/false propositions; this class provides all the functionality of <see cref="Assert"/>
	/// but with extra functionality specifically designed for MVC applications.
	/// </summary>
	public static partial class MvcAssert
	{
		/// <overloads>
		/// <summary>
		/// Verifies that the specified controller method has been decorated with an <see cref="AccessDeniedAuthorizeAttribute"/> with the specified roles.
		/// The assertion fails if the specified method does not have the attribute, or the attribute is present but with different roles.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Verifies that the specified controller method has been decorated with an <see cref="AccessDeniedAuthorizeAttribute"/> with the specified roles.
		/// The assertion fails if the specified method does not have the attribute, or the attribute is present but with different roles.
		/// </summary>
		/// <typeparam name="TController">The controller type.</typeparam>
		/// <param name="expectedRoles">The roles the attribute is expected to specify.</param>
		/// <param name="expression">The method that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the method isn't called, it's just examined to see if it has [AccessDeniedAuthorize(Roles = "canView")]
		/// MvcAssert.AccessDeniedAuthorizes&lt;SomeController&gt;("canView", x => x.SomeMethod());
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void AccessDeniedAuthorizes<TController>(string expectedRoles, Expression<Func<TController, ActionResult>> expression)
		{
			AccessDeniedAuthorizesInternal<TController, ActionResult>(expectedRoles, expression);
		}

		/// <summary>
		/// Verifies that the specified controller method has been decorated with an <see cref="AccessDeniedAuthorizeAttribute"/> with the specified roles.
		/// The assertion fails if the specified method does not have the attribute, or the attribute is present but with different roles.
		/// </summary>
		/// <typeparam name="TController">The controller type.</typeparam>
		/// <param name="expectedRoles">The roles the attribute is expected to specify.</param>
		/// <param name="expression">The method that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the method isn't called, it's just examined to see if it has [AccessDeniedAuthorize(Roles = "canView")]
		/// MvcAssert.AccessDeniedAuthorizes&lt;SomeController&gt;("canView", x => x.SomeMethod());
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void AccessDeniedAuthorizes<TController>(string expectedRoles, Expression<Func<TController, JsonResult>> expression)
		{
			AccessDeniedAuthorizesInternal<TController, JsonResult>(expectedRoles, expression);
		}

		/// <summary>
		/// Verifies that the specified controller method has been decorated with an <see cref="AccessDeniedAuthorizeAttribute"/> with the specified roles.
		/// The assertion fails if the specified method does not have the attribute, or the attribute is present but with different roles.
		/// </summary>
		/// <typeparam name="TController">The controller type.</typeparam>
		/// <param name="expectedRoles">The roles the attribute is expected to specify.</param>
		/// <param name="expression">The method that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the method isn't called, it's just examined to see if it has [AccessDeniedAuthorize(Roles = "canView")]
		/// MvcAssert.AccessDeniedAuthorizes&lt;SomeController&gt;("canView", x => x.SomeMethod());
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void AccessDeniedAuthorizes<TController>(string expectedRoles, Expression<Func<TController, ViewResult>> expression)
		{
			AccessDeniedAuthorizesInternal<TController, ViewResult>(expectedRoles, expression);
		}

		/// <summary>
		/// Verifies that the specified controller method has been decorated with an <see cref="AccessDeniedAuthorizeAttribute"/> with the specified roles.
		/// The assertion fails if the specified method does not have the attribute, or the attribute is present but with different roles.
		/// </summary>
		/// <typeparam name="TController">The controller type.</typeparam>
		/// <typeparam name="TReturnType">The method's return type.</typeparam>
		/// <param name="expectedRoles">The roles the attribute is expected to specify.</param>
		/// <param name="expression">The method that is expected to be decorated with the attribute.</param>
		private static void AccessDeniedAuthorizesInternal<TController, TReturnType>(string expectedRoles, Expression<Func<TController, TReturnType>> expression)
		{
			var attribute = AttributeHelpers.GetAttribute(expression, typeof(AccessDeniedAuthorizeAttribute));
			if (attribute == null)
			{
				Assert.Fail("No AccessDeniedAuthorize attribute found (expected Roles on attribute to be {0})", expectedRoles);
			}
			var actualRoles = ((AccessDeniedAuthorizeAttribute)attribute).Roles;
			Assert.AreEqual(expectedRoles, actualRoles);
		}
	}
}
