﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SimpleSyndicate.Testing.Mvc.TestTools.UnitTesting
{
	/// <summary>
	/// Verifies conditions in unit tests using true/false propositions; this class provides all the functionality of <see cref="Assert"/>
	/// but with extra functionality specifically designed for MVC applications.
	/// </summary>
	public static partial class MvcAssert
	{
		/// <overloads>
		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> and it has a data format string.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a data format string specified.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> and it has a data format string.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a data format string specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "xxx")]
		/// MvcAssert.HasDisplayFormat&lt;SomeViewModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayFormat<TModel>(Expression<Func<TModel, bool>> expression)
		{
			HasDisplayFormatInternal<TModel, bool>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> and it has a data format string.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a data format string specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "xxx")]
		/// MvcAssert.HasDisplayFormat&lt;SomeViewModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayFormat<TModel>(Expression<Func<TModel, bool?>> expression)
		{
			HasDisplayFormatInternal<TModel, bool?>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> and it has a data format string.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a data format string specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "xxx")]
		/// MvcAssert.HasDisplayFormat&lt;SomeViewModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayFormat<TModel>(Expression<Func<TModel, byte>> expression)
		{
			HasDisplayFormatInternal<TModel, byte>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> and it has a data format string.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a data format string specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "xxx")]
		/// MvcAssert.HasDisplayFormat&lt;SomeViewModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayFormat<TModel>(Expression<Func<TModel, byte?>> expression)
		{
			HasDisplayFormatInternal<TModel, byte?>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> and it has a data format string.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a data format string specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "xxx")]
		/// MvcAssert.HasDisplayFormat&lt;SomeViewModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayFormat<TModel>(Expression<Func<TModel, char>> expression)
		{
			HasDisplayFormatInternal<TModel, char>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> and it has a data format string.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a data format string specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "xxx")]
		/// MvcAssert.HasDisplayFormat&lt;SomeViewModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayFormat<TModel>(Expression<Func<TModel, char?>> expression)
		{
			HasDisplayFormatInternal<TModel, char?>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> and it has a data format string.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a data format string specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "xxx")]
		/// MvcAssert.HasDisplayFormat&lt;SomeViewModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayFormat<TModel>(Expression<Func<TModel, DateTime>> expression)
		{
			HasDisplayFormatInternal<TModel, DateTime>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> and it has a data format string.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a data format string specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "xxx")]
		/// MvcAssert.HasDisplayFormat&lt;SomeViewModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayFormat<TModel>(Expression<Func<TModel, DateTime?>> expression)
		{
			HasDisplayFormatInternal<TModel, DateTime?>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> and it has a data format string.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a data format string specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "xxx")]
		/// MvcAssert.HasDisplayFormat&lt;SomeViewModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayFormat<TModel>(Expression<Func<TModel, decimal>> expression)
		{
			HasDisplayFormatInternal<TModel, decimal>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> and it has a data format string.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a data format string specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "xxx")]
		/// MvcAssert.HasDisplayFormat&lt;SomeViewModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayFormat<TModel>(Expression<Func<TModel, decimal?>> expression)
		{
			HasDisplayFormatInternal<TModel, decimal?>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> and it has a data format string.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a data format string specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "xxx")]
		/// MvcAssert.HasDisplayFormat&lt;SomeViewModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayFormat<TModel>(Expression<Func<TModel, double>> expression)
		{
			HasDisplayFormatInternal<TModel, double>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> and it has a data format string.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a data format string specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "xxx")]
		/// MvcAssert.HasDisplayFormat&lt;SomeViewModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayFormat<TModel>(Expression<Func<TModel, double?>> expression)
		{
			HasDisplayFormatInternal<TModel, double?>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> and it has a data format string.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a data format string specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "xxx")]
		/// MvcAssert.HasDisplayFormat&lt;SomeViewModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayFormat<TModel>(Expression<Func<TModel, float>> expression)
		{
			HasDisplayFormatInternal<TModel, float>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> and it has a data format string.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a data format string specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "xxx")]
		/// MvcAssert.HasDisplayFormat&lt;SomeViewModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayFormat<TModel>(Expression<Func<TModel, float?>> expression)
		{
			HasDisplayFormatInternal<TModel, float?>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> and it has a data format string.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a data format string specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "xxx")]
		/// MvcAssert.HasDisplayFormat&lt;SomeViewModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayFormat<TModel>(Expression<Func<TModel, int>> expression)
		{
			HasDisplayFormatInternal<TModel, int>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> and it has a data format string.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a data format string specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "xxx")]
		/// MvcAssert.HasDisplayFormat&lt;SomeViewModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayFormat<TModel>(Expression<Func<TModel, int?>> expression)
		{
			HasDisplayFormatInternal<TModel, int?>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> and it has a data format string.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a data format string specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "xxx")]
		/// MvcAssert.HasDisplayFormat&lt;SomeViewModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayFormat<TModel>(Expression<Func<TModel, long>> expression)
		{
			HasDisplayFormatInternal<TModel, long>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> and it has a data format string.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a data format string specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "xxx")]
		/// MvcAssert.HasDisplayFormat&lt;SomeViewModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayFormat<TModel>(Expression<Func<TModel, long?>> expression)
		{
			HasDisplayFormatInternal<TModel, long?>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> and it has a data format string.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a data format string specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "xxx")]
		/// MvcAssert.HasDisplayFormat&lt;SomeViewModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayFormat<TModel>(Expression<Func<TModel, sbyte>> expression)
		{
			HasDisplayFormatInternal<TModel, sbyte>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> and it has a data format string.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a data format string specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "xxx")]
		/// MvcAssert.HasDisplayFormat&lt;SomeViewModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayFormat<TModel>(Expression<Func<TModel, sbyte?>> expression)
		{
			HasDisplayFormatInternal<TModel, sbyte?>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> and it has a data format string.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a data format string specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "xxx")]
		/// MvcAssert.HasDisplayFormat&lt;SomeViewModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayFormat<TModel>(Expression<Func<TModel, short>> expression)
		{
			HasDisplayFormatInternal<TModel, short>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> and it has a data format string.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a data format string specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "xxx")]
		/// MvcAssert.HasDisplayFormat&lt;SomeViewModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayFormat<TModel>(Expression<Func<TModel, short?>> expression)
		{
			HasDisplayFormatInternal<TModel, short?>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> and it has a data format string.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a data format string specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "xxx")]
		/// MvcAssert.HasDisplayFormat&lt;SomeViewModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayFormat<TModel>(Expression<Func<TModel, string>> expression)
		{
			HasDisplayFormatInternal<TModel, string>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> and it has a data format string.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a data format string specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "xxx")]
		/// MvcAssert.HasDisplayFormat&lt;SomeViewModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayFormat<TModel>(Expression<Func<TModel, uint>> expression)
		{
			HasDisplayFormatInternal<TModel, uint>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> and it has a data format string.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a data format string specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "xxx")]
		/// MvcAssert.HasDisplayFormat&lt;SomeViewModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayFormat<TModel>(Expression<Func<TModel, uint?>> expression)
		{
			HasDisplayFormatInternal<TModel, uint?>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> and it has a data format string.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a data format string specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "xxx")]
		/// MvcAssert.HasDisplayFormat&lt;SomeViewModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayFormat<TModel>(Expression<Func<TModel, ulong>> expression)
		{
			HasDisplayFormatInternal<TModel, ulong>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> and it has a data format string.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a data format string specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "xxx")]
		/// MvcAssert.HasDisplayFormat&lt;SomeViewModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayFormat<TModel>(Expression<Func<TModel, ulong?>> expression)
		{
			HasDisplayFormatInternal<TModel, ulong?>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> and it has a data format string.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a data format string specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "xxx")]
		/// MvcAssert.HasDisplayFormat&lt;SomeViewModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayFormat<TModel>(Expression<Func<TModel, ushort>> expression)
		{
			HasDisplayFormatInternal<TModel, ushort>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> and it has a data format string.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a data format string specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the property isn't accessed, it's just examined to see if it has [DisplayFormat(DataFormatString = "xxx")]
		/// MvcAssert.HasDisplayFormat&lt;SomeViewModel&gt;(x => x.SomeProperty);
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasDisplayFormat<TModel>(Expression<Func<TModel, ushort?>> expression)
		{
			HasDisplayFormatInternal<TModel, ushort?>(expression);
		}

		/// <summary>
		/// Verifies that the specified model property has been decorated with a <see cref="DisplayFormatAttribute"/> and it has a data format string.
		/// The assertion fails if the specified model property does not have the attribute, or the attribute is present but does not have a data format string specified.
		/// </summary>
		/// <typeparam name="TModel">The model type.</typeparam>
		/// <typeparam name="TType">The property type.</typeparam>
		/// <param name="expression">The model property that is expected to be decorated with the attribute.</param>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		private static void HasDisplayFormatInternal<TModel, TType>(Expression<Func<TModel, TType>> expression)
		{
			var attribute = AttributeHelpers.GetAttribute(expression, typeof(DisplayFormatAttribute));
			if (attribute == null)
			{
				Assert.Fail("No DisplayFormat attribute found");
			}
			var actualDataFormatString = ((DisplayFormatAttribute)attribute).DataFormatString;
			if (actualDataFormatString == null)
			{
				Assert.Fail("No data format string found on DisplayFormat attribute (DataFormatString is null)");
			}
			if (String.IsNullOrEmpty(actualDataFormatString))
			{
				Assert.Fail("No data format string found on DisplayFormat attribute (DataFormatString is empty string)");
			}
			if (String.IsNullOrWhiteSpace(actualDataFormatString))
			{
				Assert.Fail("No data format string found on DisplayFormat attribute (DataFormatString consists only of white-space characters)");
			}
		}
	}
}
