﻿using System;
using System.Linq.Expressions;
using System.Net.Http;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SimpleSyndicate.Testing.Mvc.TestTools.UnitTesting
{
	/// <summary>
	/// Verifies conditions in unit tests using true/false propositions; this class provides all the functionality of <see cref="Assert"/>
	/// but with extra functionality specifically designed for MVC applications.
	/// </summary>
	public static partial class MvcAssert
	{
		/// <overloads>
		/// <summary>
		/// Verifies that the specified controller method has been decorated with the specified attribute.
		/// The assertion fails if the specified method does not have the attribute.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Verifies that the specified controller method has been decorated with the specified attribute.
		/// The assertion fails if the specified method does not have the attribute.
		/// </summary>
		/// <typeparam name="TController">The controller type.</typeparam>
		/// <typeparam name="TExpectedType">The attribute type expected to be found on the method.</typeparam>
		/// <param name="expression">The method that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the method isn't called, it's just examined to see if it has the attribute
		/// MvcAssert.HasAttribute&lt;SomeController, HttpGetAttribute&gt;(x => x.SomeMethod());
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Justification = "No type parameter as want to inspect a method rather than call it.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasAttribute<TController, TExpectedType>(Expression<Func<TController, ActionResult>> expression)
			where TExpectedType : Attribute
		{
			HasAttributeInternal<TController, ActionResult, TExpectedType>(expression);
		}

		/// <summary>
		/// Verifies that the specified controller method has been decorated with the specified attribute.
		/// The assertion fails if the specified method does not have the attribute.
		/// </summary>
		/// <typeparam name="TController">The controller type.</typeparam>
		/// <typeparam name="TExpectedType">The attribute type expected to be found on the method.</typeparam>
		/// <param name="expression">The method that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the method isn't called, it's just examined to see if it has the attribute
		/// MvcAssert.HasAttribute&lt;SomeController, HttpGetAttribute&gt;(x => x.SomeMethod());
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Justification = "No type parameter as want to inspect a method rather than call it.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasAttribute<TController, TExpectedType>(Expression<Func<TController, JsonResult>> expression)
			where TExpectedType : Attribute
		{
			HasAttributeInternal<TController, JsonResult, TExpectedType>(expression);
		}

		/// <summary>
		/// Verifies that the specified controller method has been decorated with the specified attribute.
		/// The assertion fails if the specified method does not have the attribute.
		/// </summary>
		/// <typeparam name="TController">The controller type.</typeparam>
		/// <typeparam name="TExpectedType">The attribute type expected to be found on the method.</typeparam>
		/// <param name="expression">The method that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the method isn't called, it's just examined to see if it has the attribute
		/// MvcAssert.HasAttribute&lt;SomeController, HttpGetAttribute&gt;(x => x.SomeMethod());
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Justification = "No type parameter as want to inspect a method rather than call it.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasAttribute<TController, TExpectedType>(Expression<Func<TController, ViewResult>> expression)
			where TExpectedType : Attribute
		{
			HasAttributeInternal<TController, ViewResult, TExpectedType>(expression);
		}

		/// <summary>
		/// Verifies that the specified controller method has been decorated with the specified attribute.
		/// The assertion fails if the specified method does not have the attribute.
		/// </summary>
		/// <typeparam name="TController">The controller type.</typeparam>
		/// <typeparam name="TExpectedType">The attribute type expected to be found on the method.</typeparam>
		/// <param name="expression">The method that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the method isn't called, it's just examined to see if it has the attribute
		/// MvcAssert.HasAttribute&lt;SomeController, HttpGetAttribute&gt;(x => x.SomeMethod());
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Justification = "No type parameter as want to inspect a method rather than call it.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasAttribute<TController, TExpectedType>(Expression<Func<TController, HttpResponseMessage>> expression)
			where TExpectedType : Attribute
		{
			HasAttributeInternal<TController, HttpResponseMessage, TExpectedType>(expression);
		}

		/// <summary>
		/// Verifies that the specified controller method has been decorated with the specified attribute.
		/// The assertion fails if the specified method does not have the attribute.
		/// </summary>
		/// <typeparam name="TController">The controller type.</typeparam>
		/// <typeparam name="TReturnType">The method's return type.</typeparam>
		/// <typeparam name="TExpectedType">The attribute type expected to be found on the method.</typeparam>
		/// <param name="expression">The method that is expected to be decorated with the attribute.</param>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Justification = "No type parameter as want to inspect a method rather than call it.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		private static void HasAttributeInternal<TController, TReturnType, TExpectedType>(Expression<Func<TController, TReturnType>> expression)
			where TExpectedType : Attribute
		{
			if (!AttributeHelpers.HasAttribute(expression, typeof(TExpectedType)))
			{
				Assert.Fail("No " + typeof(TExpectedType).Name + " attribute found");
			}
		}
	}
}
