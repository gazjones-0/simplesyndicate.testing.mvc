﻿using System;
using System.Linq.Expressions;
using System.Net.Http;
using System.Web.Http.Description;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SimpleSyndicate.Testing.Mvc.TestTools.UnitTesting
{
	/// <summary>
	/// Verifies conditions in unit tests using true/false propositions; this class provides all the functionality of <see cref="Assert"/>
	/// but with extra functionality specifically designed for MVC applications.
	/// </summary>
	public static partial class MvcAssert
	{
		/// <overloads>
		/// <summary>
		/// Verifies that a controller method has been decorated with a <see cref="ResponseTypeAttribute"/>.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Verifies that the specified controller method has been decorated with a <see cref="ResponseTypeAttribute"/> and it has a type.
		/// The assertion fails if the specified method does not have the attribute, or the attribute is present but does not have a type specified.
		/// </summary>
		/// <typeparam name="TController">The controller type.</typeparam>
		/// <param name="expression">The method that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the method isn't called, it's just examined to see if it has [ResponseType]
		/// MvcAssert.HasResponseType&lt;SomeController&gt;(x => x.SomeMethod());
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasResponseType<TController>(Expression<Func<TController, HttpResponseMessage>> expression)
		{
			HasResponseTypeInternal<TController, HttpResponseMessage>(expression);
		}

		/// <summary>
		/// Verifies that the specified controller method has been decorated with a <see cref="ResponseTypeAttribute"/> with the specified name.
		/// The assertion fails if the specified method does not have the attribute, or the attribute is present but specifies a different name.
		/// </summary>
		/// <typeparam name="TController">The controller type.</typeparam>
		/// <param name="expectedResponseType">The expected response type.</param>
		/// <param name="expression">The method that is expected to be decorated with the attribute.</param>
		/// <example>
		/// <code language="cs">
		/// // note that the method isn't called, it's just examined to see if it has [ResponseType]
		/// MvcAssert.HasResponseType&lt;SomeController&gt;(typeof(SomeType), x => x.SomeMethod());
		/// </code>
		/// </example>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		public static void HasResponseType<TController>(Type expectedResponseType, Expression<Func<TController, HttpResponseMessage>> expression)
		{
			HasResponseTypeInternal<TController, HttpResponseMessage>(expectedResponseType, expression);
		}

		/// <summary>
		/// Verifies that the specified controller method has been decorated with a <see cref="ResponseTypeAttribute"/> and it has a type.
		/// The assertion fails if the specified method does not have the attribute, or the attribute is present but does not have a type specified.
		/// </summary>
		/// <typeparam name="TController">The controller type.</typeparam>
		/// <typeparam name="TReturnType">The method's return type.</typeparam>
		/// <param name="expression">The method that is expected to be decorated with the attribute.</param>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		private static void HasResponseTypeInternal<TController, TReturnType>(Expression<Func<TController, HttpResponseMessage>> expression)
		{
			var attribute = AttributeHelpers.GetAttribute(expression, typeof(ResponseTypeAttribute));
			if (attribute == null)
			{
				Assert.Fail("No ResponseType attribute found");
			}
			var actualResponseType = ((ResponseTypeAttribute)attribute).ResponseType;
			if (actualResponseType == null)
			{
				Assert.Fail("No response type found on ResponseType attribute (ResponseType returned null)");
			}
		}

		/// <summary>
		/// Verifies that the specified controller method has been decorated with a <see cref="ResponseTypeAttribute"/> with the specified name.
		/// The assertion fails if the specified method does not have the attribute, or the attribute is present but specifies a different name.
		/// </summary>
		/// <typeparam name="TController">The controller type.</typeparam>
		/// <typeparam name="TReturnType">The method's return type.</typeparam>
		/// <param name="expectedResponseType">The expected response type.</param>
		/// <param name="expression">The method that is expected to be decorated with the attribute.</param>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Nested generics appropriate here")]
		private static void HasResponseTypeInternal<TController, TReturnType>(Type expectedResponseType, Expression<Func<TController, TReturnType>> expression)
		{
			var attribute = AttributeHelpers.GetAttribute(expression, typeof(ResponseTypeAttribute));
			if (attribute == null)
			{
				Assert.Fail("No ResponseType attribute found");
			}
			var actualResponseType = ((ResponseTypeAttribute)attribute).ResponseType;
			Assert.AreEqual(expectedResponseType, actualResponseType);
		}
	}
}
