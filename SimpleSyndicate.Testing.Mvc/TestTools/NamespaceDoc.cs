﻿namespace SimpleSyndicate.Testing.Mvc.TestTools
{
	/// <summary>
	/// The <see cref="TestTools"/> namespace contains classes providing testing functionality for MVC applications.
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGeneratedAttribute]
	internal class NamespaceDoc
	{
	}
}
