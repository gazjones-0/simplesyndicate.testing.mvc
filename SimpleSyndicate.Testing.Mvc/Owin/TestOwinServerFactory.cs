﻿using Microsoft.Owin.Testing;
using Owin;

namespace SimpleSyndicate.Testing.Mvc.Owin
{
	/// <summary>
	/// Factory class for creating test Owin servers.
	/// </summary>
	public static class TestOwinServerFactory
	{
		/// <summary>
		/// Owin server; we keep it as a private variable so that it doesn't get instantly disposed as soon as we've created the app builder.
		/// </summary>
		private static TestServer testServer = null;

		/// <summary>
		/// Creates a new test Owin server.
		/// </summary>
		/// <returns>A test Owin server object that implements <see cref="IAppBuilder"/>.</returns>
		public static IAppBuilder Create()
		{
			if (testServer != null)
			{
				testServer.Dispose();
			}
			IAppBuilder appBuilder = null;
			testServer = TestServer.Create(app =>
			{
				appBuilder = app;
			});
			return appBuilder;
		}
	}
}
