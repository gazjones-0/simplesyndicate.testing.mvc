﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSyndicate.Testing.Mvc.TestData;

namespace SimpleSyndicate.Testing.Mvc.Tests.TestData
{
	public class TestMvcRepositoriesContainer : MvcRepositoriesContainer
	{
	}

	[TestClass]
	public class MvcRepositoriesContainerTests
	{
		[TestMethod]
		public void DisposeDisposes()
		{
			// arrange
			var container = new TestMvcRepositoriesContainer();
			container.AspNetUserDefaultRoleRepository = MvcRepositoriesContainerFactory.CreateAspNetUserDefaultRoleRepository();
			container.AspNetUserLoginDetailRepository = MvcRepositoriesContainerFactory.CreateAspNetUserLoginDetailRepository();
			container.SelectListItemViewModelRepository = MvcRepositoriesContainerFactory.CreateSelectListItemViewModelRepository();

			// act
			container.Dispose();

			// assert
			Assert.IsNull(container.AspNetUserDefaultRoleRepository);
			Assert.IsNull(container.AspNetUserLoginDetailRepository);
			Assert.IsNull(container.SelectListItemViewModelRepository);
		}
	}
}
