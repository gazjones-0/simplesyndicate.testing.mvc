﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSyndicate.Mvc.Models;
using SimpleSyndicate.Testing.Mvc.TestData;

namespace SimpleSyndicate.Testing.Mvc.Tests.TestData
{
	[TestClass]
	public class AspNetContainerFactoryTests
	{
		[TestMethod]
		public void CreateCreates()
		{
			// arrange
			var container = AspNetContainerFactory.Create<ApplicationUser>();

			// assert
			Assert.IsNotNull(container.RoleStore);
			Assert.IsNotNull(container.UserManager);
			Assert.IsNotNull(container.UserStore);
		}

		[TestMethod]
		public void CreateRoleStoreCreates()
		{
			// arrange
			var roleStore = AspNetIdentityFactory.CreateRoleStore();

			// assert
			Assert.IsNotNull(roleStore);
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void CreateUserManagerThrowsArgumentNullException()
		{
			// act
			var userManager = AspNetIdentityFactory.CreateUserManager<ApplicationUser>(null);
		}

		[TestMethod]
		public void CreateUserManagerCreates()
		{
			// arrange
			var userStore = AspNetIdentityFactory.CreateUserStore<ApplicationUser>();
			var userManager = AspNetIdentityFactory.CreateUserManager<ApplicationUser>(userStore);

			// assert
			Assert.IsNotNull(userManager);
		}

		[TestMethod]
		public void CreateUserStoreCreates()
		{
			// arrange
			var userStore = AspNetIdentityFactory.CreateUserStore<ApplicationUser>();

			// assert
			Assert.IsNotNull(userStore);
		}
	}
}
