﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSyndicate.Mvc.Models;
using SimpleSyndicate.Testing.Mvc.TestData;

namespace SimpleSyndicate.Testing.Mvc.Tests.TestData
{
	[TestClass]
	public class AspNetContainerTests
	{
		[TestMethod]
		public void DisposeDisposes()
		{
			// arrange
			var container = AspNetContainerFactory.Create<ApplicationUser>();

			// act
			container.Dispose();

			// assert
			Assert.IsNull(container.RoleStore);
			Assert.IsNull(container.UserManager);
			Assert.IsNull(container.UserStore);
		}
	}
}
