﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSyndicate.Testing.Mvc.TestData;

namespace SimpleSyndicate.Testing.Mvc.Tests.TestData
{
	[TestClass]
	public class HttpContextContainerFactoryTests
	{
		[TestMethod]
		public void CreateCreates()
		{
			// arrange
			var container = HttpContextContainerFactory.Create();

			// assert
			Assert.IsNotNull(container.HttpContext);
			Assert.IsNotNull(container.HttpContextWithPaging);
			Assert.IsNotNull(container.HttpContextWrapper);
			Assert.IsNotNull(container.HttpContextWrapperWithPaging);
		}

		[TestMethod]
		public void CreateHttpContextCreates()
		{
			// arrange
			var httpContext = HttpContextFactory.CreateHttpContext();

			// assert
			Assert.IsNotNull(httpContext);
		}

		[TestMethod]
		public void CreateHttpContextWithPagingCreates()
		{
			// arrange
			var httpContext = HttpContextFactory.CreateHttpContextWithPaging();

			// assert
			Assert.IsNotNull(httpContext);
		}

		[TestMethod]
		public void CreateHttpContextWithPagingHasPaging()
		{
			// arrange
			var httpContext = HttpContextFactory.CreateHttpContextWithPaging();

			// assert
			Assert.IsNotNull(httpContext.Request.QueryString["grid-page"]);
		}

		[TestMethod]
		public void CreateHttpContextWrapperCreates()
		{
			// arrange
			var httpContextWrapper = HttpContextFactory.CreateHttpContextWrapper();

			// assert
			Assert.IsNotNull(httpContextWrapper);
		}

		[TestMethod]
		public void CreateHttpContextWrapperWithPagingCreates()
		{
			// arrange
			var httpContextWrapper = HttpContextFactory.CreateHttpContextWrapperWithPaging();

			// assert
			Assert.IsNotNull(httpContextWrapper);
		}

		[TestMethod]
		public void CreateHttpContextWrapperWithPagingHasPaging()
		{
			// arrange
			var httpContextWrapper = HttpContextFactory.CreateHttpContextWrapperWithPaging();

			// assert
			Assert.IsNotNull(httpContextWrapper.Request.QueryString["grid-page"]);
		}
	}
}
