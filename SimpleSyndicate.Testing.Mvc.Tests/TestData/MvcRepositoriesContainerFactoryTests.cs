﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSyndicate.Testing.Mvc.TestData;

namespace SimpleSyndicate.Testing.Mvc.Tests.TestData
{
	[TestClass]
	public class MvcRepositoriesContainerFactoryTests
	{
		[TestMethod]
		public void CreateAspNetUserDefaultRoleRepositoryCreates()
		{
			// arrange
			var repository = MvcRepositoriesContainerFactory.CreateAspNetUserDefaultRoleRepository();

			// assert
			Assert.IsNotNull(repository);
		}

		[TestMethod]
		public void CreateAspNetUserDefaultRoleRepositoryCreatesWithItems()
		{
			// arrange
			var repository = MvcRepositoriesContainerFactory.CreateAspNetUserDefaultRoleRepository();

			// assert
			Assert.IsTrue(repository.Count() > 0);
		}

		[TestMethod]
		public void CreateAspNetUserLoginDetailRepositoryCreates()
		{
			// arrange
			var repository = MvcRepositoriesContainerFactory.CreateAspNetUserLoginDetailRepository();

			// assert
			Assert.IsNotNull(repository);
		}

		[TestMethod]
		public void CreateAspNetUserLoginDetailRepositoryCreatesWithItems()
		{
			// arrange
			var repository = MvcRepositoriesContainerFactory.CreateAspNetUserLoginDetailRepository();

			// assert
			Assert.IsTrue(repository.Count() > 0);
		}

		[TestMethod]
		public void CreateSelectListItemViewModelRepositoryCreates()
		{
			// arrange
			var repository = MvcRepositoriesContainerFactory.CreateSelectListItemViewModelRepository();

			// assert
			Assert.IsNotNull(repository);
		}

		[TestMethod]
		public void CreateSelectListItemViewModelRepositoryWithItems()
		{
			// arrange
			var repository = MvcRepositoriesContainerFactory.CreateSelectListItemViewModelRepository();

			// assert
			Assert.IsTrue(repository.Count() > 0);
		}
	}
}
