﻿using System;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSyndicate.Testing.Mvc.Web.Mvc;

namespace SimpleSyndicate.Testing.Mvc.Tests.Web.Mvc
{
	[TestClass]
	public class JsonResultExtensionsTests
	{
		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void IsNotNullAssertsIfNull()
		{
			// arrange
			JsonResult jsonResult = null;

			// act
			jsonResult.IsNotNull();
		}

		[TestMethod]
		public void IsNotNullDoesNotAssertIfNotNull()
		{
			// arrange
			JsonResult jsonResult = new JsonResult();

			// act
			jsonResult.IsNotNull();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void GenericDataIsAssertsIfWrongType()
		{
			// arrange
			JsonResult jsonResult = new JsonResult();
			jsonResult.Data = new String('a', 1);

			// act
			jsonResult.DataIs<DateTime>();
		}

		[TestMethod]
		public void GenericDataIsDoesNotAssertIfExpectedType()
		{
			// arrange
			JsonResult jsonResult = new JsonResult();
			jsonResult.Data = new String('a', 1);

			// act
			jsonResult.DataIs<String>();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void InferredDataIsAssertsIfWrongType()
		{
			// arrange
			JsonResult jsonResult = new JsonResult();
			jsonResult.Data = new String('a', 1);

			// act
			jsonResult.DataIs(new DateTime());
		}

		[TestMethod]
		public void InferredDataIsDoesNotAssertIfExpectedType()
		{
			// arrange
			JsonResult jsonResult = new JsonResult();
			jsonResult.Data = new String('a', 1);

			// act
			jsonResult.DataIs(new String('b', 1));
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void DataIsAssertsIfWrongType()
		{
			// arrange
			JsonResult jsonResult = new JsonResult();
			jsonResult.Data = new String('a', 1);

			// act
			jsonResult.DataIs(typeof(DateTime));
		}

		[TestMethod]
		public void DataIsDoesNotAssertIfExpectedType()
		{
			// arrange
			JsonResult jsonResult = new JsonResult();
			jsonResult.Data = new String('a', 1);

			// act
			jsonResult.DataIs(typeof(String));
		}
	}
}
