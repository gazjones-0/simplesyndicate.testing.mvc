﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSyndicate.Testing.Mvc.Web.Mvc;

namespace SimpleSyndicate.Testing.Mvc.Tests.Web.Mvc
{
	[TestClass]
	public class TestViewDataContainerTests
	{
		[TestMethod]
		public void ViewDataExists()
		{
			// arrange
			var viewDataContainer = new TestViewDataContainer();

			// assert
			Assert.IsNotNull(viewDataContainer.ViewData);
		}

		[TestMethod]
		public void ViewDataIsEmpty()
		{
			// arrange
			var viewDataContainer = new TestViewDataContainer();

			// assert
			Assert.AreEqual(0, viewDataContainer.ViewData.Count);
		}
	}
}
