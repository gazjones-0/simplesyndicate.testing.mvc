﻿using System;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSyndicate.Testing.Mvc.TestData;
using SimpleSyndicate.Testing.Mvc.Web.Mvc;

namespace SimpleSyndicate.Testing.Mvc.Tests.Web.Mvc
{
	public class TestController : Controller
	{
	}

	[TestClass]
	public class ControllerExtensionsTests
	{
		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void WithInvalidModelThrowsArgumentNullException()
		{
			// arrange
			Controller controller = null;

			// act
			controller.WithInvalidModel();
		}

		[TestMethod]
		public void WithInvalidModelSetsModelStateToInvalid()
		{
			// arrange
			var controller = new TestController();

			// act
			controller.WithInvalidModel();

			// assert
			Assert.IsFalse(controller.ModelState.IsValid);
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void WithMockedContextThrowsArgumentNullException()
		{
			// arrange
			Controller controller = null;

			// act
			controller.WithMockedContext();
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void WithMockedContextThrowsArgumentNullException2()
		{
			// arrange
			Controller controller = null;

			// act
			controller.WithMockedContext(null, null);
		}

		[TestMethod]
		public void WithMockedContextSetsHttpContextCurrent()
		{
			// arrange
			var controller = new TestController();
			System.Web.HttpContext.Current = null;
			var httpContext = HttpContextFactory.CreateHttpContext();

			// act
			controller.WithMockedContext(httpContext, null);

			// assert
			Assert.AreEqual(httpContext, System.Web.HttpContext.Current);
		}

		[TestMethod]
		public void WithMockedContextSetsControllerContext()
		{
			// arrange
			var controller = new TestController();
			controller.ControllerContext = new ControllerContext();
			controller.ControllerContext.HttpContext = null;
			var httpContextWrapper = HttpContextFactory.CreateHttpContextWrapper();

			// act
			controller.WithMockedContext(null, httpContextWrapper);

			// assert
			Assert.AreEqual(httpContextWrapper, controller.ControllerContext.HttpContext);
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void WithMockedValueProviderThrowsArgumentNullException()
		{
			// arrange
			Controller controller = null;

			// act
			controller.WithMockedValueProvider();
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void WithMockedValueProviderThrowsArgumentNullException2()
		{
			// arrange
			Controller controller = null;

			// act
			controller.WithMockedValueProvider(null);
		}

		[TestMethod]
		public void WithMockedValueProviderSetsValueProvider()
		{
			// arrange
			var controller = new TestController();
			controller.ValueProvider = null;
			var formCollection = new FormCollection();

			// act
			controller.WithMockedValueProvider(formCollection);

			// assert
			Assert.AreEqual(formCollection, controller.ValueProvider);
		}
	}
}
