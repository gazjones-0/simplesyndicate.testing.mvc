﻿using System;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSyndicate.Testing.Mvc.Web.Mvc;

namespace SimpleSyndicate.Testing.Mvc.Tests.Web.Mvc
{
	[TestClass]
	public class ViewResultBaseExtensionsTests
	{
		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void IsDefaultViewAssertsIfNull()
		{
			// arrange
			ViewResult viewResult = null;

			// assert
			viewResult.IsDefaultView();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void IsDefaultViewDoesNotAssertIfViewNameIsNotDefault()
		{
			// arrange
			var viewResult = new ViewResult();
			viewResult.ViewName = "test";

			// assert
			viewResult.IsDefaultView();
		}

		[TestMethod]
		public void IsDefaultViewDoesNotAssertIfViewNameIsNull()
		{
			// arrange
			var viewResult = new ViewResult();
			viewResult.ViewName = null;

			// assert
			viewResult.IsDefaultView();
		}

		[TestMethod]
		public void IsDefaultViewDoesNotAssertIfViewNameIsEmpty()
		{
			// arrange
			var viewResult = new ViewResult();
			viewResult.ViewName = String.Empty;

			// assert
			viewResult.IsDefaultView();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void IsNotNullAssertsIfNull()
		{
			// arrange
			ViewResult viewResult = null;

			// assert
			viewResult.IsNotNull();
		}

		[TestMethod]
		public void IsNotNullDoesNotAssertIfNotNull()
		{
			// arrange
			var viewResult = new ViewResult();

			// assert
			viewResult.IsNotNull();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void ModelAsAssertsIfNull()
		{
			// arrange
			ViewResult viewResult = null;

			// assert
			viewResult.ModelAs<object>();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void ModelAsAssertsIfViewDataIsNull()
		{
			// arrange
			var viewResult = new ViewResult();
			viewResult.ViewData = null;

			// assert
			viewResult.ModelAs<object>();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void ModelAsAssertsIfModelIsWrongType()
		{
			// arrange
			string viewModel = "test";
			var viewData = new ViewDataDictionary();
			viewData.Model = viewModel;
			var viewResult = new ViewResult() { ViewData = viewData };

			// assert
			viewResult.ModelAs<DateTime>();
		}

		[TestMethod]
		public void ModelAsDoesNotAssertIfModelIsExpectedType()
		{
			// arrange
			string viewModel = "test";
			var viewData = new ViewDataDictionary();
			viewData.Model = viewModel;
			var viewResult = new ViewResult() { ViewData = viewData };

			// assert
			viewResult.ModelAs<string>();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void GenericModelIsAssertsIfNull()
		{
			// arrange
			ViewResult viewResult = null;

			// assert
			viewResult.ModelIs<object>();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void GenericModelIsAssertsIfModelIsWrongType()
		{
			// arrange
			string viewModel = "test";
			var viewData = new ViewDataDictionary();
			viewData.Model = viewModel;
			var viewResult = new ViewResult() { ViewData = viewData };

			// assert
			viewResult.ModelIs<DateTime>();
		}

		[TestMethod]
		public void GenericModelIsDoesNotAssertIfModelIsExpectedType()
		{
			// arrange
			string viewModel = "test";
			var viewData = new ViewDataDictionary();
			viewData.Model = viewModel;
			var viewResult = new ViewResult() { ViewData = viewData };

			// assert
			viewResult.ModelIs<string>();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void InferredModelIsAssertsIfNull()
		{
			// arrange
			ViewResult viewResult = null;

			// assert
			viewResult.ModelIs(new object());
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void InferredModelIsAssertsIfModelIsWrongType()
		{
			// arrange
			string viewModel = "test";
			var viewData = new ViewDataDictionary();
			viewData.Model = viewModel;
			var viewResult = new ViewResult() { ViewData = viewData };

			// assert
			viewResult.ModelIs(new DateTime());
		}

		[TestMethod]
		public void InferredModelIsDoesNotAssertIfModelIsExpectedType()
		{
			// arrange
			string viewModel = "test";
			var viewData = new ViewDataDictionary();
			viewData.Model = viewModel;
			var viewResult = new ViewResult() { ViewData = viewData };

			// assert
			viewResult.ModelIs("test");
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void ModelIsAssertsIfNull()
		{
			// arrange
			ViewResult viewResult = null;

			// assert
			viewResult.ModelIs(typeof(object));
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void ModelIsAssertsIfModelIsWrongType()
		{
			// arrange
			string viewModel = "test";
			var viewData = new ViewDataDictionary();
			viewData.Model = viewModel;
			var viewResult = new ViewResult() { ViewData = viewData };

			// assert
			viewResult.ModelIs(typeof(DateTime));
		}

		[TestMethod]
		public void ModelIsDoesNotAssertIfModelIsExpectedType()
		{
			// arrange
			string viewModel = "test";
			var viewData = new ViewDataDictionary();
			viewData.Model = viewModel;
			var viewResult = new ViewResult() { ViewData = viewData };

			// assert
			viewResult.ModelIs(typeof(string));
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void ModelIsInvalidAssertsIfNull()
		{
			// arrange
			ViewResult viewResult = null;

			// assert
			viewResult.ModelIsInvalid();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void ModelIsInvalidAssertsIfModelIsValid()
		{
			// arrange
			string viewModel = "test";
			var viewData = new ViewDataDictionary();
			viewData.Model = viewModel;
			var viewResult = new ViewResult() { ViewData = viewData };

			// assert
			viewResult.ModelIsInvalid();
		}

		[TestMethod]
		public void ModelIsInvalidDoesNotAssertIfModelIsInvalid()
		{
			// arrange
			string viewModel = "test";
			var viewData = new ViewDataDictionary();
			viewData.Model = viewModel;
			var viewResult = new ViewResult() { ViewData = viewData };
			viewResult.ViewData.ModelState.AddModelError("error", "errorMessage");

			// assert
			viewResult.ModelIsInvalid();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void ModelIsValidAssertsIfNull()
		{
			// arrange
			ViewResult viewResult = null;

			// assert
			viewResult.ModelIsValid();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void ModelIsValidAssertsIfModelIsInvalid()
		{
			// arrange
			string viewModel = "test";
			var viewData = new ViewDataDictionary();
			viewData.Model = viewModel;
			var viewResult = new ViewResult() { ViewData = viewData };
			viewResult.ViewData.ModelState.AddModelError("error", "errorMessage");

			// assert
			viewResult.ModelIsValid();
		}

		[TestMethod]
		public void ModelIsValidDoesNotAssertIfModelIsValid()
		{
			// arrange
			string viewModel = "test";
			var viewData = new ViewDataDictionary();
			viewData.Model = viewModel;
			var viewResult = new ViewResult() { ViewData = viewData };

			// assert
			viewResult.ModelIsValid();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void ViewBagContainsAssertsIfNull()
		{
			// arrange
			ViewResult viewResult = null;

			// assert
			viewResult.ViewBagContains("expectedKey");
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void ViewBagContainsAssertsIfExpectedKeyNotFound()
		{
			// arrange
			var viewResult = new ViewResult();

			// assert
			viewResult.ViewBagContains("expectedKey");
		}

		[TestMethod]
		public void ViewBagContainsDoesNotAssertIfExpectedKeyFound()
		{
			// arrange
			var viewResult = new ViewResult();
			viewResult.ViewData.Add("expectedKey", "expectedValue");

			// assert
			viewResult.ViewBagContains("expectedKey");
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void ViewBagContainsValueAssertsIfNull()
		{
			// arrange
			ViewResult viewResult = null;

			// assert
			viewResult.ViewBagContains("expectedKey", "expectedValue");
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void ViewBagContainsValueAssertsIfExpectedKeyNotFound()
		{
			// arrange
			var viewResult = new ViewResult();

			// assert
			viewResult.ViewBagContains("expectedKey", "expectedValue");
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void ViewBagContainsValueAssertsIfExpectedValueNotFound()
		{
			// arrange
			var viewResult = new ViewResult();
			viewResult.ViewData.Add("expectedKey", "expectedValue");

			// assert
			viewResult.ViewBagContains("expectedKey", "notExpectedValue");
		}

		[TestMethod]
		public void ViewBagContainsDoesNotAssertIfExpectedKeyWithValueFound()
		{
			// arrange
			var viewResult = new ViewResult();
			viewResult.ViewData.Add("expectedKey", "expectedValue");

			// assert
			viewResult.ViewBagContains("expectedKey", "expectedValue");
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void ViewNameIsAssertsIfNull()
		{
			// arrange
			ViewResult viewResult = null;

			// assert
			viewResult.ViewNameIs("expectedViewName");
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void ViewNameIsAssertsIfWrongViewName()
		{
			// arrange
			var viewResult = new ViewResult();
			viewResult.ViewName = "expectedViewName";

			// assert
			viewResult.ViewNameIs("notExpectedViewName");
		}

		[TestMethod]
		public void ViewNameIsDoesNotAssertIfExpectedViewName()
		{
			// arrange
			var viewResult = new ViewResult();
			viewResult.ViewName = "expectedViewName";

			// assert
			viewResult.ViewNameIs("expectedViewName");
		}
	}
}
