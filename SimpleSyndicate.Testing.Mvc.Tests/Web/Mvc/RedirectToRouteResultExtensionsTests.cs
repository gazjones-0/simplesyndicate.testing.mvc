﻿using System.Web.Mvc;
using System.Web.Routing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSyndicate.Testing.Mvc.Web.Mvc;

namespace SimpleSyndicate.Testing.Mvc.Tests.Web.Mvc
{
	[TestClass]
	public class RedirectToRouteResultExtensionsTests
	{
		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void IsNotNullAssertsIfNull()
		{
			// arrange
			RedirectToRouteResult redirectToRouteResult = null;

			// act
			redirectToRouteResult.IsNotNull();
		}

		[TestMethod]
		public void IsNotNullDoesNotAssertIfNotNull()
		{
			// arrange
			var routeValueDictionary = new RouteValueDictionary();
			var redirectToRouteResult = new RedirectToRouteResult(routeValueDictionary);

			// act
			redirectToRouteResult.IsNotNull();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void ActionAssertsIfNull()
		{
			// arrange
			RedirectToRouteResult redirectToRouteResult = null;

			// act
			redirectToRouteResult.Action();
		}

		[TestMethod]
		public void ActionReturnsAction()
		{
			// arrange
			var routeValueDictionary = new RouteValueDictionary();
			routeValueDictionary.Add("action", "expectedAction");
			var redirectToRouteResult = new RedirectToRouteResult(routeValueDictionary);

			// act
			var actualAction = redirectToRouteResult.Action();

			// assert
			Assert.AreEqual("expectedAction", actualAction);
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void ActionIsAssertsIfNull()
		{
			// arrange
			RedirectToRouteResult redirectToRouteResult = null;

			// assert
			redirectToRouteResult.ActionIs("test");
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void ActionIsAssertsIfWrongAction()
		{
			// arrange
			var routeValueDictionary = new RouteValueDictionary();
			routeValueDictionary.Add("action", "expectedAction");
			var redirectToRouteResult = new RedirectToRouteResult(routeValueDictionary);

			// assert
			redirectToRouteResult.ActionIs("notExpectedAction");
		}

		[TestMethod]
		public void ActionIsDoesNotAssertIfExpectedAction()
		{
			// arrange
			var routeValueDictionary = new RouteValueDictionary();
			routeValueDictionary.Add("action", "expectedAction");
			var redirectToRouteResult = new RedirectToRouteResult(routeValueDictionary);

			// assert
			redirectToRouteResult.ActionIs("expectedAction");
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void ControllerAssertsIfNull()
		{
			// arrange
			RedirectToRouteResult redirectToRouteResult = null;

			// act
			redirectToRouteResult.Controller();
		}

		[TestMethod]
		public void ControllerReturnsController()
		{
			// arrange
			var routeValueDictionary = new RouteValueDictionary();
			routeValueDictionary.Add("controller", "expectedController");
			var redirectToRouteResult = new RedirectToRouteResult(routeValueDictionary);

			// act
			var actualController = redirectToRouteResult.Controller();

			// assert
			Assert.AreEqual("expectedController", actualController);
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void ControllerIsAssertsIfNull()
		{
			// arrange
			RedirectToRouteResult redirectToRouteResult = null;

			// assert
			redirectToRouteResult.ControllerIs("test");
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void ControllerIsAssertsIfWrongController()
		{
			// arrange
			var routeValueDictionary = new RouteValueDictionary();
			routeValueDictionary.Add("controller", "expectedController");
			var redirectToRouteResult = new RedirectToRouteResult(routeValueDictionary);

			// assert
			redirectToRouteResult.ControllerIs("notExpectedController");
		}

		[TestMethod]
		public void ControllerIsDoesNotAssertIfExpectedController()
		{
			// arrange
			var routeValueDictionary = new RouteValueDictionary();
			routeValueDictionary.Add("controller", "expectedController");
			var redirectToRouteResult = new RedirectToRouteResult(routeValueDictionary);

			// assert
			redirectToRouteResult.ControllerIs("expectedController");
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void HasPagingAssertsIfNull()
		{
			// arrange
			RedirectToRouteResult redirectToRouteResult = null;

			// assert
			redirectToRouteResult.HasPaging();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void HasPagingAssertsIfNoPaging()
		{
			// arrange
			var routeValueDictionary = new RouteValueDictionary();
			var redirectToRouteResult = new RedirectToRouteResult(routeValueDictionary);

			// assert
			redirectToRouteResult.HasPaging();
		}

		[TestMethod]
		public void HasPagingDoesNotAssertIfPagingPresent()
		{
			// arrange
			var routeValueDictionary = new RouteValueDictionary();
			routeValueDictionary.Add("grid-page", "3");
			var redirectToRouteResult = new RedirectToRouteResult(routeValueDictionary);

			// assert
			redirectToRouteResult.HasPaging();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void HasRouteValueAssertsIfNull()
		{
			// arrange
			RedirectToRouteResult redirectToRouteResult = null;

			// assert
			redirectToRouteResult.HasRouteValue("expectedKey");
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void HasRouteValueAssertsIfRouteValueNotFound()
		{
			// arrange
			var routeValueDictionary = new RouteValueDictionary();
			var redirectToRouteResult = new RedirectToRouteResult(routeValueDictionary);

			// assert
			redirectToRouteResult.HasRouteValue("expectedKey");
		}

		[TestMethod]
		public void HasRouteValueDoesNotAssertIfRouteValueFound()
		{
			// arrange
			var routeValueDictionary = new RouteValueDictionary();
			routeValueDictionary.Add("expectedKey", "value");
			var redirectToRouteResult = new RedirectToRouteResult(routeValueDictionary);

			// assert
			redirectToRouteResult.HasRouteValue("expectedKey");
		}
	}
}
