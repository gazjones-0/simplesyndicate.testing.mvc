﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSyndicate.Mvc.Controllers;
using SimpleSyndicate.Testing.Mvc.Web.Mvc;

namespace SimpleSyndicate.Testing.Mvc.Tests.Web.Mvc
{
	public class TestBuilder : Builder
	{
	}

	public class TestCommand : Command
	{
	}

	public class TestGenericController : GenericController<TestBuilder>
	{
		public void TestSetConfirmationMessage(string entity, ConfirmationMessageActionPerformed actionPerformed)
		{
			this.SetConfirmationMessage(entity, actionPerformed);
		}
	}

	public class TestGenericControllerWithCommand : GenericController<TestBuilder, TestCommand>
	{
		public void TestSetConfirmationMessage(string entity, ConfirmationMessageActionPerformed actionPerformed)
		{
			this.SetConfirmationMessage(entity, actionPerformed);
		}
	}

	[TestClass]
	public class GenericControllerExtensionsTests
	{
		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void HasConfirmationMessageForEntityAddedAssertsIfNoConfirmationMessage()
		{
			// arrange
			var controller = new TestGenericController();

			// assert
			controller.HasConfirmationMessageForEntityAdded();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void HasConfirmationMessageForEntityAddedAssertsIfWrongConfirmationMessage()
		{
			// arrange
			var controller = new TestGenericController();

			// act
			controller.TestSetConfirmationMessage("test", ConfirmationMessageActionPerformed.EntityDeleted);

			// assert
			controller.HasConfirmationMessageForEntityAdded();
		}

		[TestMethod]
		public void HasConfirmationMessageForEntityAddedDoesNotAssertIfConfirmationMessage()
		{
			// arrange
			var controller = new TestGenericController();

			// act
			controller.TestSetConfirmationMessage("test", ConfirmationMessageActionPerformed.EntityAdded);

			// assert
			controller.HasConfirmationMessageForEntityAdded();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void HasConfirmationMessageForEntityDeletedAssertsIfNoConfirmationMessage()
		{
			// arrange
			var controller = new TestGenericController();

			// assert
			controller.HasConfirmationMessageForEntityDeleted();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void HasConfirmationMessageForEntityDeletedAssertsIfWrongConfirmationMessage()
		{
			// arrange
			var controller = new TestGenericController();

			// act
			controller.TestSetConfirmationMessage("test", ConfirmationMessageActionPerformed.EntityAdded);

			// assert
			controller.HasConfirmationMessageForEntityDeleted();
		}

		[TestMethod]
		public void HasConfirmationMessageForEntityDeletedDoesNotAssertIfConfirmationMessage()
		{
			// arrange
			var controller = new TestGenericController();

			// act
			controller.TestSetConfirmationMessage("test", ConfirmationMessageActionPerformed.EntityDeleted);

			// assert
			controller.HasConfirmationMessageForEntityDeleted();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void HasConfirmationMessageForEntityUpdatedAssertsIfNoConfirmationMessage()
		{
			// arrange
			var controller = new TestGenericController();

			// assert
			controller.HasConfirmationMessageForEntityUpdated();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void HasConfirmationMessageForEntityUpdatedAssertsIfWrongConfirmationMessage()
		{
			// arrange
			var controller = new TestGenericController();

			// act
			controller.TestSetConfirmationMessage("test", ConfirmationMessageActionPerformed.EntityAdded);

			// assert
			controller.HasConfirmationMessageForEntityUpdated();
		}

		[TestMethod]
		public void HasConfirmationMessageForEntityUpdatedDoesNotAssertIfConfirmationMessage()
		{
			// arrange
			var controller = new TestGenericController();

			// act
			controller.TestSetConfirmationMessage("test", ConfirmationMessageActionPerformed.EntityUpdated);

			// assert
			controller.HasConfirmationMessageForEntityUpdated();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void WithCommandHasConfirmationMessageForEntityAddedAssertsIfNoConfirmationMessage()
		{
			// arrange
			var controller = new TestGenericControllerWithCommand();

			// assert
			controller.HasConfirmationMessageForEntityAdded();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void WithCommandHasConfirmationMessageForEntityAddedAssertsIfWrongConfirmationMessage()
		{
			// arrange
			var controller = new TestGenericControllerWithCommand();

			// act
			controller.TestSetConfirmationMessage("test", ConfirmationMessageActionPerformed.EntityDeleted);

			// assert
			controller.HasConfirmationMessageForEntityAdded();
		}

		[TestMethod]
		public void WithCommandHasConfirmationMessageForEntityAddedDoesNotAssertIfConfirmationMessage()
		{
			// arrange
			var controller = new TestGenericControllerWithCommand();

			// act
			controller.TestSetConfirmationMessage("test", ConfirmationMessageActionPerformed.EntityAdded);

			// assert
			controller.HasConfirmationMessageForEntityAdded();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void WithCommandHasConfirmationMessageForEntityDeletedAssertsIfNoConfirmationMessage()
		{
			// arrange
			var controller = new TestGenericControllerWithCommand();

			// assert
			controller.HasConfirmationMessageForEntityDeleted();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void WithCommandHasConfirmationMessageForEntityDeletedAssertsIfWrongConfirmationMessage()
		{
			// arrange
			var controller = new TestGenericControllerWithCommand();

			// act
			controller.TestSetConfirmationMessage("test", ConfirmationMessageActionPerformed.EntityAdded);

			// assert
			controller.HasConfirmationMessageForEntityDeleted();
		}

		[TestMethod]
		public void WithCommandHasConfirmationMessageForEntityDeletedDoesNotAssertIfConfirmationMessage()
		{
			// arrange
			var controller = new TestGenericControllerWithCommand();

			// act
			controller.TestSetConfirmationMessage("test", ConfirmationMessageActionPerformed.EntityDeleted);

			// assert
			controller.HasConfirmationMessageForEntityDeleted();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void WithCommandHasConfirmationMessageForEntityUpdatedAssertsIfNoConfirmationMessage()
		{
			// arrange
			var controller = new TestGenericControllerWithCommand();

			// assert
			controller.HasConfirmationMessageForEntityUpdated();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void WithCommandHasConfirmationMessageForEntityUpdatedAssertsIfWrongConfirmationMessage()
		{
			// arrange
			var controller = new TestGenericControllerWithCommand();

			// act
			controller.TestSetConfirmationMessage("test", ConfirmationMessageActionPerformed.EntityAdded);

			// assert
			controller.HasConfirmationMessageForEntityUpdated();
		}

		[TestMethod]
		public void WithCommandHasConfirmationMessageForEntityUpdatedDoesNotAssertIfConfirmationMessage()
		{
			// arrange
			var controller = new TestGenericControllerWithCommand();

			// act
			controller.TestSetConfirmationMessage("test", ConfirmationMessageActionPerformed.EntityUpdated);

			// assert
			controller.HasConfirmationMessageForEntityUpdated();
		}
	}
}
