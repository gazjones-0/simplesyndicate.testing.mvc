﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSyndicate.Testing.Mvc.Web.Mvc;

namespace SimpleSyndicate.Testing.Mvc.Tests.Web.Mvc
{
	[TestClass]
	public class HtmlHelperFactoryTests
	{
		[TestMethod]
		public void CreateCreates()
		{
			// act
			var htmlHelper = HtmlHelperFactory.Create<object>();

			// assert
			Assert.IsNotNull(htmlHelper);
		}
	}
}
