﻿using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSyndicate.Testing.Mvc.Web.Mvc;

namespace SimpleSyndicate.Testing.Mvc.Tests.Web.Mvc
{
	[TestClass]
	public class RedirectResultExtensionsTests
	{
		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void IsNotNullAssertsIfNull()
		{
			// arrange
			RedirectResult redirectResult = null;

			// act
			redirectResult.IsNotNull();
		}

		[TestMethod]
		public void IsNotNullDoesNotAssertIfNotNull()
		{
			// arrange
			var redirectResult = new RedirectResult("tempuri.org");

			// act
			redirectResult.IsNotNull();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void IsNotPermanentAssertsIfNull()
		{
			// arrange
			RedirectResult redirectResult = null;

			// act
			redirectResult.IsNotPermanent();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void IsNotPermanentAssertsIfPermanenet()
		{
			// arrange
			var redirectResult = new RedirectResult("tempuri.org", true);

			// act
			redirectResult.IsNotPermanent();
		}

		[TestMethod]
		public void IsPermanentDoesNotAssertIfNotPermanent()
		{
			// arrange
			var redirectResult = new RedirectResult("tempuri.org", false);

			// act
			redirectResult.IsNotPermanent();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void IsPermanentAssertsIfNull()
		{
			// arrange
			RedirectResult redirectResult = null;

			// act
			redirectResult.IsPermanent();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void IsPermanentAssertsIfNotPermanenet()
		{
			// arrange
			var redirectResult = new RedirectResult("tempuri.org", false);

			// act
			redirectResult.IsPermanent();
		}

		[TestMethod]
		public void IsPermanentDoesNotAssertIfPermanent()
		{
			// arrange
			var redirectResult = new RedirectResult("tempuri.org", true);

			// act
			redirectResult.IsPermanent();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void UrlIsAssertsIfNull()
		{
			// arrange
			RedirectResult redirectResult = null;

			// act
			redirectResult.UrlIs("test");
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void UrlIsAssertsIfWrongUrl()
		{
			// arrange
			var redirectResult = new RedirectResult("tempuri.org");

			// act
			redirectResult.UrlIs("tempuri2.org");
		}

		[TestMethod]
		public void UrlIsDoesNotAssertIfExpectedUrl()
		{
			// arrange
			var redirectResult = new RedirectResult("tempuri.org");

			// act
			redirectResult.UrlIs("tempuri.org");
		}
	}
}
