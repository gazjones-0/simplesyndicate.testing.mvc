﻿using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSyndicate.Testing.Mvc.Web.Mvc;

namespace SimpleSyndicate.Testing.Mvc.Tests.Web.Mvc
{
	[TestClass]
	public class GlobalFiltersHelpersTests
	{
		[TestMethod]
		public void GenericHasFilterIsFalseIfFilterNotInList()
		{
			// arrange
			var filters = GlobalFilters.Filters;
			filters.Clear();

			// act
			var hasFilter = GlobalFiltersHelpers.HasFilter<HandleErrorAttribute>();

			// assert
			Assert.IsFalse(hasFilter);
		}

		[TestMethod]
		public void GenericHasFilterIsTrueIfFilterInList()
		{
			// arrange
			var filters = GlobalFilters.Filters;
			filters.Clear();
			filters.Add(new HandleErrorAttribute());
			
			// act
			var hasFilter = GlobalFiltersHelpers.HasFilter<HandleErrorAttribute>();

			// assert
			Assert.IsTrue(hasFilter);
		}

		[TestMethod]
		public void InferredHasFilterIsFalseIfFilterNotInList()
		{
			// arrange
			var filters = GlobalFilters.Filters;
			filters.Clear();

			// act
			var hasFilter = GlobalFiltersHelpers.HasFilter(new HandleErrorAttribute());

			// assert
			Assert.IsFalse(hasFilter);
		}

		[TestMethod]
		public void InferredHasFilterIsTrueIfFilterInList()
		{
			// arrange
			var filters = GlobalFilters.Filters;
			filters.Clear();
			filters.Add(new HandleErrorAttribute());

			// act
			var hasFilter = GlobalFiltersHelpers.HasFilter(new HandleErrorAttribute());

			// assert
			Assert.IsTrue(hasFilter);
		}

		[TestMethod]
		public void HasFilterIsFalseIfFilterNotInList()
		{
			// arrange
			var filters = GlobalFilters.Filters;
			filters.Clear();

			// act
			var hasFilter = GlobalFiltersHelpers.HasFilter(typeof(HandleErrorAttribute));

			// assert
			Assert.IsFalse(hasFilter);
		}

		[TestMethod]
		public void HasFilterIsTrueIfFilterInList()
		{
			// arrange
			var filters = GlobalFilters.Filters;
			filters.Clear();
			filters.Add(new HandleErrorAttribute());

			// act
			var hasFilter = GlobalFiltersHelpers.HasFilter(typeof(HandleErrorAttribute));

			// assert
			Assert.IsTrue(hasFilter);
		}
	}
}
