﻿using System;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSyndicate.Mvc.Web.Mvc;
using SimpleSyndicate.Testing.Mvc.Web.Mvc;

namespace SimpleSyndicate.Testing.Mvc.Tests.Web.Mvc
{
	[TestClass]
	public class ViewEnginesHelpersTests
	{
		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void OnlyHandlesCSharpAssertsIfNull()
		{
			// arrange
			RazorViewEngine viewEngine = null;

			// act
			ViewEnginesHelpers.OnlyHandlesCSharp(viewEngine);
		}

		[TestMethod]
		public void OnlyHandlesCSharpReturnsTrueForCSharpRazorViewEngine()
		{
			// arrange
			var viewEngine = new CSharpRazorViewEngine();

			// act
			var onlyHandlesCSharp = ViewEnginesHelpers.OnlyHandlesCSharp(viewEngine);

			// assert
			Assert.AreEqual(true, onlyHandlesCSharp);
		}

		[TestMethod]
		public void OnlyHandlesCSharpReturnsFalseIfAreaMasterLocationFormatsNonCSharp()
		{
			// arrange
			var viewEngine = new CSharpRazorViewEngine();
			var razorViewEngine = new RazorViewEngine();
			viewEngine.AreaMasterLocationFormats = razorViewEngine.AreaMasterLocationFormats;

			// act
			var onlyHandlesCSharp = ViewEnginesHelpers.OnlyHandlesCSharp(viewEngine);

			// assert
			Assert.AreEqual(false, onlyHandlesCSharp);
		}

		[TestMethod]
		public void OnlyHandlesCSharpReturnsFalseIfAreaPartialViewLocationFormatsNonCSharp()
		{
			// arrange
			var viewEngine = new CSharpRazorViewEngine();
			var razorViewEngine = new RazorViewEngine();
			viewEngine.AreaPartialViewLocationFormats = razorViewEngine.AreaPartialViewLocationFormats;

			// act
			var onlyHandlesCSharp = ViewEnginesHelpers.OnlyHandlesCSharp(viewEngine);

			// assert
			Assert.AreEqual(false, onlyHandlesCSharp);
		}

		[TestMethod]
		public void OnlyHandlesCSharpReturnsFalseIfAreaViewLocationFormatsNonCSharp()
		{
			// arrange
			var viewEngine = new CSharpRazorViewEngine();
			var razorViewEngine = new RazorViewEngine();
			viewEngine.AreaViewLocationFormats = razorViewEngine.AreaViewLocationFormats;

			// act
			var onlyHandlesCSharp = ViewEnginesHelpers.OnlyHandlesCSharp(viewEngine);

			// assert
			Assert.AreEqual(false, onlyHandlesCSharp);
		}

		[TestMethod]
		public void OnlyHandlesCSharpReturnsFalseIfFileExtensionsNonCSharp()
		{
			// arrange
			var viewEngine = new CSharpRazorViewEngine();
			var razorViewEngine = new RazorViewEngine();
			viewEngine.FileExtensions = razorViewEngine.FileExtensions;

			// act
			var onlyHandlesCSharp = ViewEnginesHelpers.OnlyHandlesCSharp(viewEngine);

			// assert
			Assert.AreEqual(false, onlyHandlesCSharp);
		}

		[TestMethod]
		public void OnlyHandlesCSharpReturnsFalseIfMasterLocationFormatsNonCSharp()
		{
			// arrange
			var viewEngine = new CSharpRazorViewEngine();
			var razorViewEngine = new RazorViewEngine();
			viewEngine.MasterLocationFormats = razorViewEngine.MasterLocationFormats;

			// act
			var onlyHandlesCSharp = ViewEnginesHelpers.OnlyHandlesCSharp(viewEngine);

			// assert
			Assert.AreEqual(false, onlyHandlesCSharp);
		}

		[TestMethod]
		public void OnlyHandlesCSharpReturnsFalseIfPartialViewLocationFormatsNonCSharp()
		{
			// arrange
			var viewEngine = new CSharpRazorViewEngine();
			var razorViewEngine = new RazorViewEngine();
			viewEngine.PartialViewLocationFormats = razorViewEngine.PartialViewLocationFormats;

			// act
			var onlyHandlesCSharp = ViewEnginesHelpers.OnlyHandlesCSharp(viewEngine);

			// assert
			Assert.AreEqual(false, onlyHandlesCSharp);
		}

		[TestMethod]
		public void OnlyHandlesCSharpReturnsFalseIfViewLocationFormatsNonCSharp()
		{
			// arrange
			var viewEngine = new CSharpRazorViewEngine();
			var razorViewEngine = new RazorViewEngine();
			viewEngine.ViewLocationFormats = razorViewEngine.ViewLocationFormats;

			// act
			var onlyHandlesCSharp = ViewEnginesHelpers.OnlyHandlesCSharp(viewEngine);

			// assert
			Assert.AreEqual(false, onlyHandlesCSharp);
		}
	}
}
