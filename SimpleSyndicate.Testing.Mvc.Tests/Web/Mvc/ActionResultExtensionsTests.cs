﻿using System.Net;
using System.Web.Mvc;
using System.Web.Routing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSyndicate.Testing.Mvc.Web.Mvc;

namespace SimpleSyndicate.Testing.Mvc.Tests.Web.Mvc
{
	[TestClass]
	public class ActionResultExtensionsTests
	{
		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void IsBadRequestAssertsForWrongType()
		{
			// arrange
			var actionResult = new ViewResult();

			// act
			actionResult.IsBadRequest();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void IsBadRequestAssertsForWrongStatusCode()
		{
			// arrange
			var actionResult = new HttpStatusCodeResult((int)HttpStatusCode.Ambiguous);

			// act
			actionResult.IsBadRequest();
		}

		[TestMethod]
		public void IsBadRequestDoesNotAssertForBadRequest()
		{
			// arrange
			var actionResult = new HttpStatusCodeResult((int)HttpStatusCode.BadRequest);

			// act
			actionResult.IsBadRequest();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void IsJsonResultAssertsForWrongType()
		{
			// arrange
			var actionResult = new ViewResult();

			// act
			actionResult.IsJsonResult();
		}

		[TestMethod]
		public void IsJsonResultDoesNotAssertForJsonResult()
		{
			// arrange
			var actionResult = new JsonResult();

			// act
			actionResult.IsJsonResult();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void IsNotFoundAssertsForWrongType()
		{
			// arrange
			var actionResult = new ViewResult();

			// act
			actionResult.IsNotFound();
		}

		[TestMethod]
		public void IsNotFoundDoesNotAssertForNotFound()
		{
			// arrange
			var actionResult = new HttpNotFoundResult();

			// act
			actionResult.IsNotFound();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void IsNotNullAssertsIfNull()
		{
			// arrange
			ActionResult actionResult = null;

			// act
			actionResult.IsNotNull();
		}

		[TestMethod]
		public void IsNotNullDoesNotAssertIfNotNull()
		{
			// arrange
			ActionResult actionResult = new ViewResult();

			// act
			actionResult.IsNotNull();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void IsRedirectResultAssertsForWrongType()
		{
			// arrange
			var actionResult = new JsonResult();

			// act
			actionResult.IsRedirectResult();
		}

		[TestMethod]
		public void IsRedirectResultDoesNotAssertForRedirectResult()
		{
			// arrange
			var actionResult = new RedirectResult("tempuri.org");

			// act
			actionResult.IsRedirectResult();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void IsRedirectToRouteResultAssertsForWrongType()
		{
			// arrange
			var actionResult = new JsonResult();

			// act
			actionResult.IsRedirectToRouteResult();
		}

		[TestMethod]
		public void IsRedirectToRouteResultDoesNotAssertForRedirectToRouteResult()
		{
			// arrange
			var actionResult = new RedirectToRouteResult(new RouteValueDictionary());

			// act
			actionResult.IsRedirectToRouteResult();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void IsRedirectToRouteResultAssertsForWrongAction()
		{
			// arrange
			var routeValueDictionary = new RouteValueDictionary();
			routeValueDictionary.Add("action", "expectedAction");
			var actionResult = new RedirectToRouteResult(routeValueDictionary);

			// act
			actionResult.IsRedirectToRouteResult("notExpectedAction");
		}

		[TestMethod]
		public void IsRedirectToRouteResultDoesNotAssertForExpectedAction()
		{
			// arrange
			var routeValueDictionary = new RouteValueDictionary();
			routeValueDictionary.Add("action", "expectedAction");
			var actionResult = new RedirectToRouteResult(routeValueDictionary);

			// act
			actionResult.IsRedirectToRouteResult("expectedAction");
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void IsRedirectToRouteResultAssertsForWrongAction2()
		{
			// arrange
			var routeValueDictionary = new RouteValueDictionary();
			routeValueDictionary.Add("action", "expectedAction");
			routeValueDictionary.Add("controller", "expectedController");
			var actionResult = new RedirectToRouteResult(routeValueDictionary);

			// act
			actionResult.IsRedirectToRouteResult("notExpectedAction", "notExpectedController");
		}

		[TestMethod]
		public void IsRedirectToRouteResultDoesNotAssertForExpectedAction2()
		{
			// arrange
			var routeValueDictionary = new RouteValueDictionary();
			routeValueDictionary.Add("action", "expectedAction");
			routeValueDictionary.Add("controller", "expectedController");
			var actionResult = new RedirectToRouteResult(routeValueDictionary);

			// act
			actionResult.IsRedirectToRouteResult("expectedAction", "expectedController");
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void IsRedirectToRouteResultAssertsForWrongController()
		{
			// arrange
			var routeValueDictionary = new RouteValueDictionary();
			routeValueDictionary.Add("action", "expectedAction");
			routeValueDictionary.Add("controller", "expectedController");
			var actionResult = new RedirectToRouteResult(routeValueDictionary);

			// act
			actionResult.IsRedirectToRouteResult("expectedAction", "notExpectedController");
		}

		[TestMethod]
		public void IsRedirectToRouteResultDoesNotAssertForExpectedController()
		{
			// arrange
			var routeValueDictionary = new RouteValueDictionary();
			routeValueDictionary.Add("action", "expectedAction");
			routeValueDictionary.Add("controller", "expectedController");
			var actionResult = new RedirectToRouteResult(routeValueDictionary);

			// act
			actionResult.IsRedirectToRouteResult("expectedAction", "expectedController");
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void IsViewResultAssertsForWrongType()
		{
			// arrange
			var actionResult = new JsonResult();

			// act
			actionResult.IsViewResult();
		}

		[TestMethod]
		public void IsViewResultDoesNotAssertForViewResult()
		{
			// arrange
			var actionResult = new ViewResult();

			// act
			actionResult.IsViewResult();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void JsonResultAssertsForWrongType()
		{
			// arrange
			var actionResult = new ViewResult();

			// act
			actionResult.JsonResult();
		}

		[TestMethod]
		public void JsonResultDoesNotAssertForJsonResult()
		{
			// arrange
			var actionResult = new JsonResult();

			// act
			actionResult.JsonResult();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void RedirectResultAssertsForWrongType()
		{
			// arrange
			var actionResult = new JsonResult();

			// act
			actionResult.RedirectResult();
		}

		[TestMethod]
		public void RedirectResultDoesNotAssertForRedirectResult()
		{
			// arrange
			var actionResult = new RedirectResult("tempuri.org");

			// act
			actionResult.RedirectResult();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void RedirectToRouteResultAssertsForWrongType()
		{
			// arrange
			var actionResult = new JsonResult();

			// act
			actionResult.RedirectToRouteResult();
		}

		[TestMethod]
		public void RedirectToRouteResultDoesNotAssertForRedirectToRouteResult()
		{
			// arrange
			var actionResult = new RedirectToRouteResult(new RouteValueDictionary());

			// act
			actionResult.RedirectToRouteResult();
		}

		[TestMethod]
		[ExpectedException(typeof(AssertFailedException))]
		public void ViewResultAssertsForWrongType()
		{
			// arrange
			var actionResult = new JsonResult();

			// act
			actionResult.ViewResult();
		}

		[TestMethod]
		public void ViewResultDoesNotAssertForViewResult()
		{
			// arrange
			var actionResult = new ViewResult();

			// act
			actionResult.ViewResult();
		}
	}
}
