﻿using System;
using System.Web;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSyndicate.Testing.Mvc.Web;

namespace SimpleSyndicate.Testing.Mvc.Tests.Web
{
	[TestClass]
	public class TestHttpContextWrapperTests
	{
		[TestMethod]
		public void CurrentNotificationDoesNotThrowException()
		{
			// arrange
			var httpContextWrapper = new TestHttpContextWrapper(new HttpContext(new HttpRequest(String.Empty, "http://tempuri.org", String.Empty), new HttpResponse(null)));

			// act
			var notification = httpContextWrapper.CurrentNotification;

			// assert
			Assert.AreEqual(RequestNotification.SendResponse, notification);
		}

		[TestMethod]
		public void IsWebSocketRequestDoesNotThrowException()
		{
			// arrange
			var httpContextWrapper = new TestHttpContextWrapper(new HttpContext(new HttpRequest(String.Empty, "http://tempuri.org", String.Empty), new HttpResponse(null)));

			// act
			var isWebSocketRequest = httpContextWrapper.IsWebSocketRequest;

			// assert
			Assert.AreEqual(false, isWebSocketRequest);
		}

		[TestMethod]
		public void WebSocketRequestedProtocolsDoesNotThrowException()
		{
			// arrange
			var httpContextWrapper = new TestHttpContextWrapper(new HttpContext(new HttpRequest(String.Empty, "http://tempuri.org", String.Empty), new HttpResponse(null)));

			// act
			var webSocketRequestedProtocols = httpContextWrapper.WebSocketRequestedProtocols;

			// assert
			Assert.IsNull(webSocketRequestedProtocols);
		}
	}
}
