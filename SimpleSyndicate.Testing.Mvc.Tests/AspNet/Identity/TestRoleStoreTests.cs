﻿using System;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSyndicate.Testing.Mvc.AspNet.Identity;

namespace SimpleSyndicate.Testing.Mvc.Tests.AspNet.Identity
{
	[TestClass]
	public class TestRoleStoreTests
	{
		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void CreateAsyncThrowsArgumentNullException()
		{
			// arrange
			var roleStore = new TestRoleStore();

			// act
			roleStore.CreateAsync(null).Wait();
		}

		[TestMethod]
		public void CreateAsyncCreates()
		{
			// arrange
			var roleStore = new TestRoleStore();
			var role = new IdentityRole();
			role.Id = "testRole";
			role.Name = "Test role";

			// act
			roleStore.CreateAsync(role).Wait();
			var createdRole = roleStore.FindByIdAsync("testRole").Result;

			// assert
			Assert.IsNotNull(createdRole);
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void DeleteAsyncThrowsArgumentNullException()
		{
			// arrange
			var roleStore = new TestRoleStore();

			// act
			roleStore.DeleteAsync(null).Wait();
		}

		[TestMethod]
		public void DeleteAsyncDeletes()
		{
			// arrange
			var roleStore = new TestRoleStore();
			var role = new IdentityRole();
			role.Id = "testRole";
			role.Name = "Test role";

			// act
			roleStore.CreateAsync(role).Wait();
			var createdRole = roleStore.FindByIdAsync("testRole").Result;
			roleStore.DeleteAsync(createdRole).Wait();
			var deletedRole = roleStore.FindByIdAsync("testRole").Result;

			// assert
			Assert.IsNotNull(createdRole);
			Assert.IsNull(deletedRole);
		}

		[TestMethod]
		public void FindByIdAsyncReturnsNullForNoMatch()
		{
			// arrange
			var roleStore = new TestRoleStore();

			// act
			var foundRole = roleStore.FindByIdAsync("testRole").Result;

			// assert
			Assert.IsNull(foundRole);
		}

		[TestMethod]
		public void FindByIdAsyncReturnsRoleForMatch()
		{
			// arrange
			var roleStore = new TestRoleStore();
			var role = new IdentityRole();
			role.Id = "testRole";
			role.Name = "Test role";

			// act
			roleStore.CreateAsync(role).Wait();
			var foundRole = roleStore.FindByIdAsync("testRole").Result;

			// assert
			Assert.IsNotNull(foundRole);
		}

		[TestMethod]
		public void FindByNameAsyncReturnsNullForNoMatch()
		{
			// arrange
			var roleStore = new TestRoleStore();

			// act
			var foundRole = roleStore.FindByNameAsync("Test Role").Result;

			// assert
			Assert.IsNull(foundRole);
		}

		[TestMethod]
		public void FindByNameAsyncReturnsRoleForMatch()
		{
			// arrange
			var roleStore = new TestRoleStore();
			var role = new IdentityRole();
			role.Id = "testRole";
			role.Name = "Test role";

			// act
			roleStore.CreateAsync(role).Wait();
			var foundRole = roleStore.FindByNameAsync("Test Role").Result;

			// assert
			Assert.IsNotNull(foundRole);
		}

		[TestMethod]
		public void FindByNameAsyncReturnsRoleForMatchAndIgnoresCase()
		{
			// arrange
			var roleStore = new TestRoleStore();
			var role = new IdentityRole();
			role.Id = "testRole";
			role.Name = "TEST ROLE";

			// act
			roleStore.CreateAsync(role).Wait();
			var foundRole = roleStore.FindByNameAsync("test role").Result;

			// assert
			Assert.IsNotNull(foundRole);
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void UpdateAsyncThrowsArgumentNullException()
		{
			// arrange
			var roleStore = new TestRoleStore();

			// act
			roleStore.UpdateAsync(null).Wait();
		}

		[TestMethod]
		public void UpdateAsyncUpdates()
		{
			// arrange
			var roleStore = new TestRoleStore();
			var role = new IdentityRole();
			role.Id = "testRole";
			role.Name = "Test role";

			// act
			roleStore.CreateAsync(role).Wait();
			var createdRole = roleStore.FindByIdAsync("testRole").Result;
			createdRole.Name = "Updated role";
			roleStore.UpdateAsync(createdRole).Wait();
			var updatedRole = roleStore.FindByIdAsync("testRole").Result;

			// assert
			Assert.IsNotNull(updatedRole);
			Assert.AreEqual("Updated role", updatedRole.Name);
		}
	}
}
