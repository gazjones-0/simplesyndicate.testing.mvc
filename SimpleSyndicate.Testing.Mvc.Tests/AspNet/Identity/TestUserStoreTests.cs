﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSyndicate.Mvc.Models;
using SimpleSyndicate.Testing.Mvc.AspNet.Identity;

namespace SimpleSyndicate.Testing.Mvc.Tests.AspNet.Identity
{
	[TestClass]
	public class TestUserStoreTests
	{
		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void CreateAsyncThrowsArgumentNullException()
		{
			// arrange
			var userStore = new TestUserStore<ApplicationUser>();

			// act
			userStore.CreateAsync(null).Wait();
		}

		[TestMethod]
		public void CreateAsyncCreates()
		{
			// arrange
			var userStore = new TestUserStore<ApplicationUser>();
			var user = new ApplicationUser();
			user.Id = "testUser";
			user.UserName = "Test user";

			// act
			userStore.CreateAsync(user).Wait();
			var createdUser = userStore.FindByIdAsync("testUser").Result;

			// assert
			Assert.IsNotNull(createdUser);
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void DeleteAsyncThrowsArgumentNullException()
		{
			// arrange
			var userStore = new TestUserStore<ApplicationUser>();

			// act
			userStore.DeleteAsync(null).Wait();
		}

		[TestMethod]
		public void DeleteAsyncDeletes()
		{
			// arrange
			var userStore = new TestUserStore<ApplicationUser>();
			var user = new ApplicationUser();
			user.Id = "testUser";
			user.UserName = "Test user";

			// act
			userStore.CreateAsync(user).Wait();
			var createdUser = userStore.FindByIdAsync("testUser").Result;
			userStore.DeleteAsync(user).Wait();
			var deletedUser = userStore.FindByIdAsync("testUser").Result;

			// assert
			Assert.IsNotNull(createdUser);
			Assert.IsNull(deletedUser);
		}

		[TestMethod]
		public void FindByIdAsyncReturnsNullForNoMatch()
		{
			// arrange
			var userStore = new TestUserStore<ApplicationUser>();

			// act
			var foundUser = userStore.FindByIdAsync("testUser").Result;

			// assert
			Assert.IsNull(foundUser);
		}

		[TestMethod]
		public void FindByIdAsyncReturnsUserForMatch()
		{
			// arrange
			var userStore = new TestUserStore<ApplicationUser>();
			var user = new ApplicationUser();
			user.Id = "testUser";
			user.UserName = "Test user";

			// act
			userStore.CreateAsync(user).Wait();
			var foundUser = userStore.FindByIdAsync("testUser").Result;

			// assert
			Assert.IsNotNull(foundUser);
		}

		[TestMethod]
		public void FindByNameAsyncReturnsNullForNoMatch()
		{
			// arrange
			var userStore = new TestUserStore<ApplicationUser>();

			// act
			var foundUser = userStore.FindByNameAsync("Test User").Result;

			// assert
			Assert.IsNull(foundUser);
		}

		[TestMethod]
		public void FindByNameAsyncReturnsUserForMatch()
		{
			// arrange
			var userStore = new TestUserStore<ApplicationUser>();
			var user = new ApplicationUser();
			user.Id = "testUser";
			user.UserName = "Test user";

			// act
			userStore.CreateAsync(user).Wait();
			var foundUser = userStore.FindByNameAsync("Test User").Result;

			// assert
			Assert.IsNotNull(foundUser);
		}

		[TestMethod]
		public void FindByNameAsyncReturnsUserForMatchAndIgnoresCase()
		{
			// arrange
			var userStore = new TestUserStore<ApplicationUser>();
			var user = new ApplicationUser();
			user.Id = "testUser";
			user.UserName = "TEST USER";

			// act
			userStore.CreateAsync(user).Wait();
			var foundUser = userStore.FindByNameAsync("test user").Result;

			// assert
			Assert.IsNotNull(foundUser);
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void UpdateAsyncThrowsArgumentNullException()
		{
			// arrange
			var userStore = new TestUserStore<ApplicationUser>();

			// act
			userStore.UpdateAsync(null).Wait();
		}

		[TestMethod]
		public void UpdateAsyncUpdates()
		{
			// arrange
			var userStore = new TestUserStore<ApplicationUser>();
			var user = new ApplicationUser();
			user.Id = "testUser";
			user.UserName = "Test user";

			// act
			userStore.CreateAsync(user).Wait();
			var createdUser = userStore.FindByNameAsync("Test User").Result;
			createdUser.UserName = "Updated user";
			userStore.UpdateAsync(createdUser).Wait();
			var updatedUser = userStore.FindByIdAsync("testUser").Result;

			// assert
			Assert.IsNotNull(updatedUser);
			Assert.AreEqual("Updated user", updatedUser.UserName);
		}
	}
}
