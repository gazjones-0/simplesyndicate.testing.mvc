# README #

SimpleSyndicate.Testing.Mvc NuGet package.

Get the package from https://www.nuget.org/packages/SimpleSyndicate.Testing.Mvc

### What is this repository for? ###

* Common unit testing functionality for ASP.Net MVC applications - test role store, test user store, test data helpers, better assertions, fluent assertions for controllers, view results and other MVC items.

### How do I get set up? ###

* See the documentation at http://gazooka_g72.bitbucket.org/SimpleSyndicate

### Reporting issues ###

* Use the tracker at https://bitbucket.org/gazooka_g72/simplesyndicate.testing.mvc/issues?status=new&status=open
